/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server;

import org.onap.usecaseui.server.util.DmaapSubscriber;
import org.onap.usecaseui.server.util.servicemapper.ServiceFactory;
import org.onap.usecaseui.server.util.servicemapper.ServiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@ComponentScan(basePackages = "org.onap.usecaseui.server")
@EnableCaching
public class UsecaseuiServerApplication {
	
    public static DmaapSubscriber dmaapSubscriber;

    public static ServiceLocatorFactoryBean slfbForMyFactory;

    @Autowired
    public void setDatastore(DmaapSubscriber dmaapSubscriber) {
        UsecaseuiServerApplication.dmaapSubscriber = dmaapSubscriber;
    }

    @Autowired
    public void setSlfbForMyFactory(ServiceLocatorFactoryBean slfbForMyFactory) {
        UsecaseuiServerApplication.slfbForMyFactory = slfbForMyFactory;
    }
    
    @Bean
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

    @Bean
    public ServiceLocatorFactoryBean slfbForMyFactory() {
        ServiceLocatorFactoryBean slfb = new ServiceLocatorFactoryBean();
        slfb.setServiceLocatorInterface(ServiceFactory.class);
        return slfb;
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        return new ThreadPoolTaskExecutor();
    }

    public static void main(String[] args) {


//        ServiceFactory ServiceFactory = (ServiceFactory) slfbForMyFactory.getObject();
//        ServiceMapper sm = ServiceFactory.get("SOTN");
//        System.out.println(sm.supports("SOTN"));
        SpringApplication.run(UsecaseuiServerApplication.class, args);
        dmaapSubscriber.run();

    }
}
