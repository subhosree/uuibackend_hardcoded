/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.aspectj.asm.AsmManager;
import org.onap.usecaseui.server.bo.ModelBean;
import org.onap.usecaseui.server.bo.ModelInfoBean;


public class AddModelBean {
    @JsonProperty("edgeNameList")
    private String edgeNameList;

    @JsonProperty("modelInfoList")
    private ModelInfoBean modelInfoBean;


    public AddModelBean(String edgeNameList, ModelInfoBean modelInfoBean) {
        this.edgeNameList = edgeNameList;
        this.modelInfoBean = modelInfoBean;
    }

    public AddModelBean() {
    }

    @JsonProperty("edgeNameList")
    public String getEdgeName() {
        return edgeNameList;
    }
    @JsonProperty("edgeNameList")
    public void setEdgeName(String edgeNameList) {
        this.edgeNameList = edgeNameList;
    }

    public ModelInfoBean getModelInfoBean() {
        return modelInfoBean;
    }

    public void setModelInfoBean(ModelInfoBean modelInfoBean) {
        this.modelInfoBean = modelInfoBean;
    }


   /* @Override
    public String toString() {
        return "AddModelBean{" +
                "edgeName='" + this.edgeNameList + '\'' +
                '}';
    }*/

    @Override
    public String toString() {
        return "AddModelBean{" +
                "edgeNameList='" + edgeNameList + '\'' +
                ", modelInfoBean=" + modelInfoBean +
                '}';
    }
}
