/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Customer {

    @JsonProperty("global-customer-id")
    private String customerId;

    @JsonProperty("subscriber-name")
    private String subscriberName;

    @JsonProperty("subscriber-type")
    private String subscriberType;

    @JsonProperty("resource-version")
    private String resourceVersion;

    public Customer() {
    }

    public Customer(String customerId, String subscriberName, String subscriberType, String resourceVersion) {
        this.customerId = customerId;
        this.subscriberName = subscriberName;
        this.subscriberType = subscriberType;
        this.resourceVersion = resourceVersion;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSubscriberName() {
        return subscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        this.subscriberName = subscriberName;
    }

    public String getSubscriberType() {
        return subscriberType;
    }

    public void setSubscriberType(String subscriberType) {
        this.subscriberType = subscriberType;
    }

    public String getResourceVersion() {
        return resourceVersion;
    }

    public void setResourceVersion(String resourceVersion) {
        this.resourceVersion = resourceVersion;
    }

    @Override
    public String toString() {
        return "Customers{" +
                "customerId='" + customerId + '\'' +
                ", subscriberName='" + subscriberName + '\'' +
                ", subscriberType='" + subscriberType + '\'' +
                ", resourceVersion='" + resourceVersion + '\'' +
                '}';
    }
}
