/**
 * Copyright 2016-2017 ZTE Corporation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.bean;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


public class DataMonitorBean implements Serializable {

    @JsonProperty("sdwan-bandwidth-policy-id")
    private String policyId;

    @JsonProperty("sdwan-bandwidth-policy-name")
    private String policyName;

    @JsonProperty("bandwidth-value")
    private String bandwidth;

    @JsonProperty("bandwidth-change-time")
    private String changeTime;

    @JsonProperty("vpn-id")
    private String vpnId;

    @JsonProperty("resource-version")
    private String resourceVersion;

    public DataMonitorBean() {
    }

    public DataMonitorBean(String vpnId) {
        this.vpnId = vpnId;
    }

    public DataMonitorBean(String policyName, String bandwidth, String changeTime, String vpnId, String resourceVersion) {
        this.policyName = policyName;
        this.bandwidth = bandwidth;
        this.changeTime = changeTime;
        this.vpnId = vpnId;
        this.resourceVersion = resourceVersion;
    }

    @Override
    public String toString() {
        return "DataMonitorBean{" +
                "policyId=" + policyId +
                ", policyName='" + policyName + '\'' +
                ", bandwidth='" + bandwidth + '\'' +
                ", changeTime='" + changeTime + '\'' +
                ", vpnId='" + vpnId + '\'' +
                ", resourceVersion='" + resourceVersion + '\'' +
                '}';
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public String getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(String bandwidth) {
        this.bandwidth = bandwidth;
    }

    public String getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(String changeTime) {
        this.changeTime = changeTime;
    }

    public String getVpnId() {
        return vpnId;
    }

    public void setVpnId(String vpnId) {
        this.vpnId = vpnId;
    }

    public String getResourceVersion() {
        return resourceVersion;
    }

    public void setResourceVersion(String resourceVersion) {
        this.resourceVersion = resourceVersion;
    }
}
