/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Site {

    @JsonProperty("siteId")
    private String siteId;

    @JsonProperty("siteName")
    private String siteName;

    @JsonProperty("isCloudSite")
    private String isCloudSite;

    @JsonProperty("location")
    private String location;

    @JsonProperty("zipCode")
    private String zipCode;

    @JsonProperty("role")
    private String role;

    @JsonProperty("capacity")
    private String capacity;

    @JsonProperty("interface")
    private String siteinterface;

    @JsonProperty("subnet")
    private String subnet;

    @JsonProperty("price")
    private String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getIsCloudSite() {
        return isCloudSite;
    }

    public void setIsCloudSite(String isCloudSite) {
        this.isCloudSite = isCloudSite;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getSiteinterface() {
        return siteinterface;
    }

    public void setSiteinterface(String siteinterface) {
        this.siteinterface = siteinterface;
    }

    public String getSubnet() {
        return subnet;
    }

    public void setSubnet(String subnet) {
        this.subnet = subnet;
    }
}
