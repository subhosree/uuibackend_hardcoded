/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SiteTopologyBean {

    @JsonProperty("siteId")
    private String siteId;

    @JsonProperty("siteName")
    private String siteName;

    @JsonProperty("description")
    private String description;

    @JsonProperty("isCloudSite")
    private String isCloudSite;

    @JsonProperty("location")
    private String location;

    @JsonProperty("zipCode")
    private String zipCode;

    @JsonProperty("role")
    private String role;

    @JsonProperty("capacity")
    private String capacity;

    @JsonProperty("interface")
    private String siteinterface;

    @JsonProperty("subnet")
    private String subnet;

    @JsonProperty("price")
    private String price;

    @JsonProperty("status")
    private String status;

    @JsonProperty("statEnum")
    private String statEnum;

    @JsonProperty("edgeName")
    private String edgeName;

    @JsonProperty("edgeStatus")
    private String edgeStatus;

    @JsonProperty("edgeIP")
    private String edgeIP;

    @JsonProperty("deviceDesc")
    private String deviceDesc;

    @JsonProperty("deviceName")
    private String deviceName;

    @JsonProperty("siteStatus")
    private String siteStatus;

    public String getEdgeIP() {
        return edgeIP;
    }

    public void setEdgeIP(String edgeIP) {
        this.edgeIP = edgeIP;
    }

    public String getDeviceDesc() {
        return deviceDesc;
    }

    public void setDeviceDesc(String deviceDesc) {
        this.deviceDesc = deviceDesc;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getEdgeName() {
        return edgeName;
    }

    public void setEdgeName(String edgeName) {
        this.edgeName = edgeName;
    }

    public String getEdgeStatus() {
        return edgeStatus;
    }

    public void setEdgeStatus(String edgeStatus) {
        this.edgeStatus = edgeStatus;
    }

    public String getStatEnum() {
        return statEnum;
    }

    public void setStatEnum(String statEnum) {
        this.statEnum = statEnum;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getIsCloudSite() {
        return isCloudSite;
    }

    public void setIsCloudSite(String isCloudSite) {
        this.isCloudSite = isCloudSite;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getSiteinterface() {
        return siteinterface;
    }

    public void setSiteinterface(String siteinterface) {
        this.siteinterface = siteinterface;
    }

    public String getSubnet() {
        return subnet;
    }

    public void setSubnet(String subnet) {
        this.subnet = subnet;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSiteStatus() {
        return siteStatus;
    }

    public void setSiteStatus(String siteStatus) {
        this.siteStatus = siteStatus;
    }
    /*   @Override
    public String toString() {
        return "{" +
                "siteId='" + siteId + '\'' +
                ", siteName='" + siteName + '\'' +
                ", isCloudSite='" + isCloudSite + '\'' +
                ", location='" + location + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", role='" + role + '\'' +
                ", capacity='" + capacity + '\'' +
                ", siteinterface='" + siteinterface + '\'' +
                ", subnet='" + subnet + '\'' +
                ", price='" + price + '\'' +
                ", status='" + status + '\'' +
                '}';
    }*/

    @Override
    public String toString() {
        return "{" +
                "\"siteId\":\"" + siteId + '\"' +
                ", \"siteName\":\"" + siteName + '\"' +
                ", \"description\":\"" + description + '\"' +
                ", \"isCloudSite\":\"" + isCloudSite + '\"' +
                ", \"location\":\"" + location + '\"' +
                ", \"zipCode\":\"" + zipCode + '\"' +
                ", \"role\":\"" + role + '\"' +
                ", \"capacity\":\"" + capacity + '\"' +
                ", \"siteinterface\":\"" + siteinterface + '\"' +
                ", \"subnet\":\"" + subnet + '\"' +
                ", \"price\":\"" + price + '\"' +
                ", \"status\":\"" + status + '\"' +
                ", \"statEnum\":\"" + statEnum + '\"' +
                ", \"edgeName\":\"" + edgeName + '\"' +
                ", \"edgeStatus\":\"" + edgeStatus + '\"' +
                ", \"edgeIP\":\"" + edgeIP + '\"' +
                ", \"deviceDesc\":\"" + deviceDesc + '\"' +
                ", \"deviceName\":\"" + deviceName + '\"' +
                ", \"siteStatus\":\"" + siteStatus + '\"' +

                '}';
    }
}