/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean.beans2SO;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "esn",
        "vendor",
        "name",
        "type",
        "version",
        "class",
        "systemIp"
})
public class DeviceList {
    @JsonProperty("Id")
    private String Id;
    @JsonProperty("esn")
    private String esn;
    @JsonProperty("vendor")
    private String vendor;
    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private String type;
    @JsonProperty("version")
    private String version;
    @JsonProperty("class")
    private String _class;
    @JsonProperty("systemIp")
    private String systemIp;

    @JsonProperty("esn")
    public String getEsn() {
        return esn;
    }

    @JsonProperty("esn")
    public void setEsn(String esn) {
        this.esn = esn;
    }

    @JsonProperty("vendor")
    public String getVendor() {
        return vendor;
    }

    @JsonProperty("vendor")
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("class")
    public String getClass_() {
        return _class;
    }

    @JsonProperty("class")
    public void setClass_(String _class) {
        this._class = _class;
    }

    @JsonProperty("systemIp")
    public String getSystemIp() {
        return systemIp;
    }

    @JsonProperty("systemIp")
    public void setSystemIp(String systemIp) {
        this.systemIp = systemIp;
    }

    @JsonProperty("Id")
    public String getId() {
        return Id;
    }
    @JsonProperty("Id")
    public void setId(String id) {
        Id = id;
    }

    @Override
    public String toString() {
        return "{" +
                "\"Id\":\"" + Id + '\"' +
                ", \"esn\":\"" + esn + '\"' +
                ", \"vendor\":\"" + vendor + '\"' +
                ", \"name\":\"" + name + '\"' +
                ", \"type\":\"" + type + '\"' +
                ", \"version\":\"" + version + '\"' +
                ", \"class\":\"" + _class + '\"' +
                ", \"systemIp\":\"" + systemIp + '\"' +
                '}';
    }
}
