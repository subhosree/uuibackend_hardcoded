/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean.beans2SO;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "vpnresourcelist",
        "sitereourcelist"
})
public class RequestInputs {

    @JsonProperty("vpnresourcelist")
    private List<Vpnresourcelist> vpnresourcelist = null;
    @JsonProperty("sitereourcelist")
    private List<Sitereourcelist> sitereourcelist = null;

    @JsonProperty("vpnresourcelist")
    public List<Vpnresourcelist> getVpnresourcelist() {
        return vpnresourcelist;
    }

    @JsonProperty("vpnresourcelist")
    public void setVpnresourcelist(List<Vpnresourcelist> vpnresourcelist) {
        this.vpnresourcelist = vpnresourcelist;
    }

    @JsonProperty("sitereourcelist")
    public List<Sitereourcelist> getSitereourcelist() {
        return sitereourcelist;
    }

    @JsonProperty("sitereourcelist")
    public void setSitereourcelist(List<Sitereourcelist> sitereourcelist) {
        this.sitereourcelist = sitereourcelist;
    }


    @Override
    public String toString() {
        return "{" +
                "\"vpnresourcelist\":" + vpnresourcelist +
                ", \"sitereourcelist\":" + sitereourcelist +
                '}';
    }
}
