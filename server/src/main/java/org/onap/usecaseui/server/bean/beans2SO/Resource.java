/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean.beans2SO;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
/*@JsonPropertyOrder({
        "resourceIndex",
        "name",
        "invariantUUID",
        "UUID",
        "customizationUUID",
        "parameters"
})*/
public class Resource {
    @JsonProperty("resourceIndex")
    private String resourceIndex;
    @JsonProperty("name")
    private String name;
    @JsonProperty("invariantUUID")
    private String invariantUUID;
    @JsonProperty("UUID")
    private String uUID;
    @JsonProperty("customizationUUID")
    private String customizationUUID;
    @JsonIgnore
    private Parameters_ parameters_;

    @JsonProperty("resourceIndex")
    public String getResourceIndex() {
        return resourceIndex;
    }

    @JsonProperty("resourceIndex")
    public void setResourceIndex(String resourceIndex) {
        this.resourceIndex = resourceIndex;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("invariantUUID")
    public String getInvariantUUID() {
        return invariantUUID;
    }

    @JsonProperty("invariantUUID")
    public void setInvariantUUID(String invariantUUID) {
        this.invariantUUID = invariantUUID;
    }

    @JsonProperty("UUID")
    public String getUUID() {
        return uUID;
    }

    @JsonProperty("UUID")
    public void setUUID(String uUID) {
        this.uUID = uUID;
    }

    @JsonProperty("customizationUUID")
    public String getCustomizationUUID() {
        return customizationUUID;
    }

    @JsonProperty("customizationUUID")
    public void setCustomizationUUID(String customizationUUID) {
        this.customizationUUID = customizationUUID;
    }

    public Parameters_ getParameters_() {
        return parameters_;
    }

    public void setParameters_(Parameters_ parameters_) {
        this.parameters_ = parameters_;
    }

    @Override
    public String toString() {
        return "{" +
                "\"resourceIndex\":\"" + resourceIndex + '\"' +
                ", \"name\":\"" + name + '\"' +
                ", \"invariantUUID\":\"" + invariantUUID + '\"' +
                ", \"UUID\":\"" + uUID + '\"' +
                ", \"customizationUUID\":\"" + customizationUUID + '\"' +
                ", \"parameters_\":" + parameters_ +
                '}';
    }
}
