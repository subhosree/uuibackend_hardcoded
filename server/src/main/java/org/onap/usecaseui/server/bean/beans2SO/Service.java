/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean.beans2SO;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
/*@JsonPropertyOrder({
        "name",
        "description",
        "invariantUUID",
        "UUID",
        "globalSubscriberId",
        "serviceType",
        "parameters"
})*/
public class Service {

    @JsonProperty("Id")
    private String Id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("invariantUUID")
    private String invariantUUID;
    @JsonProperty("UUID")
    private String uUID;
    @JsonProperty("globalSubscriberId")
    private String globalSubscriberId;
    @JsonProperty("serviceType")
    private String serviceType;
    @JsonProperty("parameters")
    private Parameters parameters;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("invariantUUID")
    public String getInvariantUUID() {
        return invariantUUID;
    }

    @JsonProperty("invariantUUID")
    public void setInvariantUUID(String invariantUUID) {
        this.invariantUUID = invariantUUID;
    }

    @JsonProperty("UUID")
    public String getUUID() {
        return uUID;
    }

    @JsonProperty("UUID")
    public void setUUID(String uUID) {
        this.uUID = uUID;
    }

    @JsonProperty("globalSubscriberId")
    public String getGlobalSubscriberId() {
        return globalSubscriberId;
    }

    @JsonProperty("globalSubscriberId")
    public void setGlobalSubscriberId(String globalSubscriberId) {
        this.globalSubscriberId = globalSubscriberId;
    }

    @JsonProperty("serviceType")
    public String getServiceType() {
        return serviceType;
    }

    @JsonProperty("serviceType")
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    @JsonProperty("parameters")
    public Parameters getParameters() {
        return parameters;
    }

    @JsonProperty("parameters")
    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }
    @JsonProperty("Id")
    public String getId() {
        return Id;
    }
    @JsonProperty("Id")
    public void setId(String id) {
        Id = id;
    }

    /*   @Override
           public String toString() {
               return "Service{" +
                       "name='" + name + '\'' +
                       ", description='" + description + '\'' +
                       ", serviceInvariantUuid='" + serviceInvariantUuid + '\'' +
                       ", serviceUuid='" + serviceUuid + '\'' +
                       ", globalSubscriberId='" + globalSubscriberId + '\'' +
                       ", serviceType='" + serviceType + '\'' +
                       ", parameters=" + parameters +
                       '}';
           }*/
    @Override
    public String toString() {
        return '{' +
                "\"Id\":\"" + Id + '\"' +
                ", \"name\":\"" + name + '\"' +
                ", \"description\":\"" + description + '\"' +
                ", \"invariantUUID\":\"" + invariantUUID + '\"' +
                ", \"UUID\":\"" + uUID + '\"' +
                ", \"globalSubscriberId\":\"" + globalSubscriberId + '\"' +
                ", \"serviceType\":\"" + serviceType + '\"' +
                ", \"parameters\":" + parameters +
                '}';
    }
}
