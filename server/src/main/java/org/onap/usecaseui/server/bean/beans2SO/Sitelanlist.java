/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean.beans2SO;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
/*@JsonPropertyOrder({
        "role",
        "portType",
        "portSwitch",
        "vlanId",
        "ipAddress",
        "deviceName",
        "portNumer"
})*/

public class Sitelanlist {
    @JsonProperty("role")
    private String role;
    @JsonProperty("portType")
    private String portType;
    @JsonProperty("portSwitch")
    private String portSwitch;
    @JsonProperty("vlanId")
    private String vlanId;
    @JsonProperty("ipAddress")
    private String ipAddress;
    @JsonProperty("deviceName")
    private String deviceName;
    @JsonProperty("portNumer")
    private String portNumer;

    @JsonProperty("role")
    public String getRole() {
        return role;
    }

    @JsonProperty("role")
    public void setRole(String role) {
        this.role = role;
    }

    @JsonProperty("portType")
    public String getPortType() {
        return portType;
    }

    @JsonProperty("portType")
    public void setPortType(String portType) {
        this.portType = portType;
    }

    @JsonProperty("portSwitch")
    public String getPortSwitch() {
        return portSwitch;
    }

    @JsonProperty("portSwitch")
    public void setPortSwitch(String portSwitch) {
        this.portSwitch = portSwitch;
    }

    @JsonProperty("vlanId")
    public String getVlanId() {
        return vlanId;
    }

    @JsonProperty("vlanId")
    public void setVlanId(String vlanId) {
        this.vlanId = vlanId;
    }

    @JsonProperty("ipAddress")
    public String getIpAddress() {
        return ipAddress;
    }

    @JsonProperty("ipAddress")
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @JsonProperty("deviceName")
    public String getDeviceName() {
        return deviceName;
    }

    @JsonProperty("deviceName")
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @JsonProperty("portNumer")
    public String getPortNumer() {
        return portNumer;
    }

    @JsonProperty("portNumer")
    public void setPortNumer(String portNumer) {
        this.portNumer = portNumer;
    }

    @Override
    public String toString() {
        return "{" +
                "\"role\":\"" + role + '\"' +
                ", \"portType\":\"" + portType + '\"' +
                ", \"portSwitch\":\"" + portSwitch + '\"' +
                ", \"vlanId\":\"" + vlanId + '\"' +
                ", \"ipAddress\":\"" + ipAddress + '\"' +
                ", \"deviceName\":\"" + deviceName + '\"' +
                ", \"portNumer\":\"" + portNumer + '\"' +
                '}';
    }
}
