/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean.beans2SO;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
/*@JsonPropertyOrder({
        "site_name",
        "site_emails",
        "site_address",
        "site_description",
        "site_role",
        "site_postcode",
        "site_type",
        "site_latitude",
        "site_controlPoint",
        "site_longitude",
        "sitewanlist",
        "deviceList"
})*/
public class Sitereourcelist {
    @JsonProperty("Id")
    private String Id;
    @JsonProperty("site_name")
    private String siteName;
    @JsonProperty("site_emails")
    private String siteEmails;
    @JsonProperty("site_address")
    private String siteAddress;
    @JsonProperty("site_description")
    private String siteDescription;
    @JsonProperty("site_role")
    private String siteRole;
    @JsonProperty("site_postcode")
    private String sitePostcode;
    @JsonProperty("site_type")
    private String siteType;
    @JsonProperty("site_latitude")
    private String siteLatitude;
    @JsonProperty("site_controlPoint")
    private String siteControlPoint;
    @JsonProperty("site_longitude")
    private String siteLongitude;
    @JsonProperty("sitewanlist")
    private List<Sitewanlist> sitewanlist = null;
    @JsonProperty("deviceList")
    private List<DeviceList> deviceList = null;

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    @JsonProperty("site_emails")
    public String getSiteEmails() {
        return siteEmails;
    }

    @JsonProperty("site_emails")
    public void setSiteEmails(String siteEmails) {
        this.siteEmails = siteEmails;
    }

    @JsonProperty("site_address")
    public String getSiteAddress() {
        return siteAddress;
    }

    @JsonProperty("site_address")
    public void setSiteAddress(String siteAddress) {
        this.siteAddress = siteAddress;
    }

    @JsonProperty("site_description")
    public String getSiteDescription() {
        return siteDescription;
    }

    @JsonProperty("site_description")
    public void setSiteDescription(String siteDescription) {
        this.siteDescription = siteDescription;
    }

    @JsonProperty("site_role")
    public String getSiteRole() {
        return siteRole;
    }

    @JsonProperty("site_role")
    public void setSiteRole(String siteRole) {
        this.siteRole = siteRole;
    }

    @JsonProperty("site_postcode")
    public String getSitePostcode() {
        return sitePostcode;
    }

    @JsonProperty("site_postcode")
    public void setSitePostcode(String sitePostcode) {
        this.sitePostcode = sitePostcode;
    }

    @JsonProperty("site_type")
    public String getSiteType() {
        return siteType;
    }

    @JsonProperty("site_type")
    public void setSiteType(String siteType) {
        this.siteType = siteType;
    }

    @JsonProperty("site_latitude")
    public String getSiteLatitude() {
        return siteLatitude;
    }

    @JsonProperty("site_latitude")
    public void setSiteLatitude(String siteLatitude) {
        this.siteLatitude = siteLatitude;
    }

    @JsonProperty("site_controlPoint")
    public String getSiteControlPoint() {
        return siteControlPoint;
    }

    @JsonProperty("site_controlPoint")
    public void setSiteControlPoint(String siteControlPoint) {
        this.siteControlPoint = siteControlPoint;
    }

    @JsonProperty("site_longitude")
    public String getSiteLongitude() {
        return siteLongitude;
    }

    @JsonProperty("site_longitude")
    public void setSiteLongitude(String siteLongitude) {
        this.siteLongitude = siteLongitude;
    }

    @JsonProperty("sitewanlist")
    public List<Sitewanlist> getSitewanlist() {
        return sitewanlist;
    }

    @JsonProperty("sitewanlist")
    public void setSitewanlist(List<Sitewanlist> sitewanlist) {
        this.sitewanlist = sitewanlist;
    }

    @JsonProperty("deviceList")
    public List<DeviceList> getDeviceList() {
        return deviceList;
    }

    @JsonProperty("deviceList")
    public void setDeviceList(List<DeviceList> deviceList) {
        this.deviceList = deviceList;
    }
    @JsonProperty("Id")
    public String getId() {
        return Id;
    }
    @JsonProperty("Id")
    public void setId(String id) {
        Id = id;
    }

    @Override
    public String toString() {
        return "{" +
                "\"Id\":\"" + Id + '\"' +
                ", \"site_name\":\"" + siteName + '\"' +
                ", \"site_emails\":\"" + siteEmails + '\"' +
                ", \"site_address\":\"" + siteAddress + '\"' +
                ", \"site_description\":\"" + siteDescription + '\"' +
                ", \"site_role\":\"" + siteRole + '\"' +
                ", \"site_type\":\"" + siteType + '\"' +
                ", \"site_latitude\":\"" + siteLatitude + '\"' +
                ", \"site_controlPoint\":\"" + siteControlPoint + '\"' +
                ", \"site_longitude\":\"" + siteLongitude + '\"' +
                ", \"sitewanlist\":" + sitewanlist +
                ", \"deviceList\":" + deviceList +
                '}';
    }
}
