/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean.beans2SO;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
/*@JsonPropertyOrder({
        "providerIpAddress",
        "portType",
        "inputBandwidth",
        "ipAddress",
        "name",
        "transportNetworkName",
        "outputBandwidth",
        "deviceName",
        "portNumber",
        "ipMode",
        "publicIP"
})*/
public class Sitewanlist {
    @JsonProperty("Id")
    private String Id;
    @JsonProperty("providerIpAddress")
    private String providerIpAddress;
    @JsonProperty("portType")
    private String portType;
    @JsonProperty("inputBandwidth")
    private String inputBandwidth;
    @JsonProperty("ipAddress")
    private String ipAddress;
    @JsonProperty("name")
    private String name;
    @JsonProperty("transportNetworkName")
    private String transportNetworkName;
    @JsonProperty("outputBandwidth")
    private String outputBandwidth;
    @JsonProperty("deviceName")
    private String deviceName;
    @JsonProperty("portNumber")
    private String portNumber;
    @JsonProperty("ipMode")
    private String ipMode;
    @JsonProperty("publicIP")
    private String publicIP;

    @JsonProperty("providerIpAddress")
    public String getProviderIpAddress() {
        return providerIpAddress;
    }

    @JsonProperty("providerIpAddress")
    public void setProviderIpAddress(String providerIpAddress) {
        this.providerIpAddress = providerIpAddress;
    }

    @JsonProperty("portType")
    public String getPortType() {
        return portType;
    }

    @JsonProperty("portType")
    public void setPortType(String portType) {
        this.portType = portType;
    }

    @JsonProperty("inputBandwidth")
    public String getInputBandwidth() {
        return inputBandwidth;
    }

    @JsonProperty("inputBandwidth")
    public void setInputBandwidth(String inputBandwidth) {
        this.inputBandwidth = inputBandwidth;
    }

    @JsonProperty("ipAddress")
    public String getIpAddress() {
        return ipAddress;
    }

    @JsonProperty("ipAddress")
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("transportNetworkName")
    public String getTransportNetworkName() {
        return transportNetworkName;
    }

    @JsonProperty("transportNetworkName")
    public void setTransportNetworkName(String transportNetworkName) {
        this.transportNetworkName = transportNetworkName;
    }

    @JsonProperty("outputBandwidth")
    public String getOutputBandwidth() {
        return outputBandwidth;
    }

    @JsonProperty("outputBandwidth")
    public void setOutputBandwidth(String outputBandwidth) {
        this.outputBandwidth = outputBandwidth;
    }

    @JsonProperty("deviceName")
    public String getDeviceName() {
        return deviceName;
    }

    @JsonProperty("deviceName")
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @JsonProperty("portNumber")
    public String getPortNumber() {
        return portNumber;
    }

    @JsonProperty("portNumber")
    public void setPortNumber(String portNumber) {
        this.portNumber = portNumber;
    }

    @JsonProperty("ipMode")
    public String getIpMode() {
        return ipMode;
    }

    @JsonProperty("ipMode")
    public void setIpMode(String ipMode) {
        this.ipMode = ipMode;
    }

    @JsonProperty("publicIP")
    public String getPublicIP() {
        return publicIP;
    }

    @JsonProperty("publicIP")
    public void setPublicIP(String publicIP) {
        this.publicIP = publicIP;
    }
    @JsonProperty("Id")
    public String getId() {
        return Id;
    }
    @JsonProperty("Id")
    public void setId(String id) {
        Id = id;
    }

    @Override
    public String toString() {
        return "{" +
                "\"Id\":\"" + Id + '\"' +
                ", \"providerIpAddress\":\"" + providerIpAddress + '\"' +
                ", \"portType\":\"" + portType + '\"' +
                ", \"inputBandwidth\":\"" + inputBandwidth + '\"' +
                ", \"ipAddress\":\"" + ipAddress + '\"' +
                ", \"name\":\"" + name + '\"' +
                ", \"transportNetworkName\":\"" + transportNetworkName + '\"' +
                ", \"outputBandwidth\":\"" + outputBandwidth + '\"' +
                ", \"deviceName\":\"" + deviceName + '\"' +
                ", \"portNumber\":\"" + portNumber + '\"' +
                ", \"ipMode\":\"" + ipMode + '\"' +
                ", \"publicIP\":\"" + publicIP + '\"' +
                '}';
    }

}
