/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean.beans2SO;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
/*@JsonPropertyOrder({
        "sdwanvpn_topology",
        "sdwanvpn_name",
        "sitelanlist"
})*/
public class Vpnresourcelist {

    private String sdwanvpnTopology;
    @JsonProperty("sdwanvpn_name")
    private String sdwanvpnName;
    @JsonProperty("sitelanlist")
    private List<Sitelanlist> sitelanlist = null;

    @JsonProperty("sdwanvpn_topology")
    public String getSdwanvpnTopology() {
        return sdwanvpnTopology;
    }

    @JsonProperty("sdwanvpn_topology")
    public void setSdwanvpnTopology(String sdwanvpnTopology) {
        this.sdwanvpnTopology = sdwanvpnTopology;
    }

    @JsonProperty("sdwanvpn_name")
    public String getSdwanvpnName() {
        return sdwanvpnName;
    }

    @JsonProperty("sdwanvpn_name")
    public void setSdwanvpnName(String sdwanvpnName) {
        this.sdwanvpnName = sdwanvpnName;
    }

    @JsonProperty("sitelanlist")
    public List<Sitelanlist> getSitelanlist() {
        return sitelanlist;
    }

    @JsonProperty("sitelanlist")
    public void setSitelanlist(List<Sitelanlist> sitelanlist) {
        this.sitelanlist = sitelanlist;
    }

    @Override
    public String toString() {
        return "{" +
                "\"sdwanvpn_topology\":\"" + sdwanvpnTopology + '\"' +
                ", \"sdwanvpn_name\":\"" + sdwanvpnName + '\"' +
                ", \"sitelanlist\":" + sitelanlist +
                '}';
    }
}
