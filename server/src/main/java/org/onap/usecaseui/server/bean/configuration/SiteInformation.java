/**
 * Copyright 2016-2017 ZTE Corporation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.bean.configuration;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class SiteInformation {

    @JsonProperty("subnetMask")
    private String subnetMask;
    @JsonProperty("interface")
    private String siteInterface;


    @JsonProperty("subnetMask")
    public String getSubnetMask() {
        return subnetMask;
    }

    @JsonProperty("subnetMask")
    public void setSubnetMask(String subnetMask) {
        this.subnetMask = subnetMask;
    }

    public String getSiteInterface() {
        return siteInterface;
    }

    public void setSiteInterface(String siteInterface) {
        this.siteInterface = siteInterface;
    }
}
