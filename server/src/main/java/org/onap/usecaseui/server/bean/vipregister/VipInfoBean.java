/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.bean.vipregister;

public class VipInfoBean {
    private String imgUrl;
    private String vipName;
    private String vipRole;
    private String vipModel;

    public String getVipModel() {
        return vipModel;
    }

    public void setVipModel(String vipModel) {
        this.vipModel = vipModel;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public String getVipRole() {
        return vipRole;
    }

    public void setVipRole(String vipRole) {
        this.vipRole = vipRole;
    }

    /*@Override
    public String toString() {
        return "VipInfoBean{" +
                "imgUrl='" + imgUrl + '\'' +
                ", vipName='" + vipName + '\'' +
                ", vipRole='" + vipRole + '\'' +
                '}';
    }*/

    @Override
    public String toString() {
        return "VipInfoBean{" +
                "imgUrl='" + imgUrl + '\'' +
                ", vipName='" + vipName + '\'' +
                ", vipRole='" + vipRole + '\'' +
                ", vipModel='" + vipModel + '\'' +
                '}';
    }

    public VipInfoBean(String imgUrl, String vipName, String vipRole, String vipModel) {
        this.imgUrl = imgUrl;
        this.vipName = vipName;
        this.vipRole = vipRole;
        this.vipModel = vipModel;
    }

    public VipInfoBean() {
    }
}
