/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.bean.vipregister;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Blob;

@Entity
@Table(name="vip_information")
public class VipInfoEntity implements Serializable {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "vipname")
    private String vipName;

    @Column(name = "viprole")
    private String vipRole;

    @Column(name = "vipmodel")
    private String vipModel;

    @Column(name = "image")
    private java.sql.Blob vipImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public String getVipRole() {
        return vipRole;
    }

    public void setVipRole(String vipRole) {
        this.vipRole = vipRole;
    }

    public String getVipModel() {
        return vipModel;
    }

    public void setVipModel(String vipModel) {
        this.vipModel = vipModel;
    }

    public Blob getVipImage() {
        return vipImage;
    }

    public void setVipImage(Blob vipImage) {
        this.vipImage = vipImage;
    }

    public VipInfoEntity() {
    }

    public VipInfoEntity(String id, String vipName, String vipRole, String vipModel, Blob vipImage) {
        this.id = id;
        this.vipName = vipName;
        this.vipRole = vipRole;
        this.vipModel = vipModel;
        this.vipImage = vipImage;
    }
}
