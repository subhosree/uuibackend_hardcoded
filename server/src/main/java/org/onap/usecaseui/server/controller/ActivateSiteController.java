/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.controller;

import org.onap.usecaseui.server.bean.orderservice.SiteBean2SO;
import org.onap.usecaseui.server.service.ActivateSiteService;
import org.onap.usecaseui.server.service.OrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;
import java.util.TreeMap;

@RestController
@CrossOrigin(origins="*")
public class ActivateSiteController {
    @Resource(name="ActivateSiteService")
    public ActivateSiteService activateSiteService;

    @RequestMapping(value = {"/ccvpn/activateSite"}, method = RequestMethod.POST)
    public void activateEdge(@RequestParam(required = false) String siteId, @RequestParam(required = false) String siteName) {
        Map<String, String> result = new TreeMap<>();
       /* Site site = fromRepresentation(edgeInfo, Site.class);
        String siteId = site.getSiteId();
        String siteName = site.getSiteName();*/
        SiteBean2SO siteBean2SO = new SiteBean2SO();
        siteBean2SO.setSiteId(siteId);
        siteBean2SO.setSiteName(siteName);

        activateSiteService.activateEdge(siteBean2SO);

        /*if (json.indexOf("FAILED") != -1) {
            return result;
        }


        return result;*/
    }
}
