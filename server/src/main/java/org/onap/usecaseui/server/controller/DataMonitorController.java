/**
 * Copyright 2016-2017 ZTE Corporation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onap.usecaseui.server.bean.DataMonitorBean;
import org.onap.usecaseui.server.bean.sotn.NetWorkResource;
import org.onap.usecaseui.server.bean.sotn.Pinterface;
import org.onap.usecaseui.server.bean.sotn.Pnf;
import org.onap.usecaseui.server.bo.DataMonitorBo;
import org.onap.usecaseui.server.service.DataMonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.onap.usecaseui.server.bo.DataMonitorBo;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/uui-datamonitor")
public class DataMonitorController {
    @Resource(name="DataMonitorService")
    public DataMonitorService dataMonitorService;

    @Autowired
    ServletContext servletContext;

    @RequestMapping(value = {"/getDataMonitorData"}, params = "vpnId", method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<List<DataMonitorBo>> getDataMonitorData(@RequestParam("vpnId") String vpnId, @RequestParam("duration") String requestLimit ){

        List<DataMonitorBo> result = new ArrayList<>();
        try {
            result = dataMonitorService.getDataFromAAI(vpnId, requestLimit);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        }catch (NullPointerException e)
        {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(result);
        }
        // if(json.indexOf("FAILED")!=-1){
        //    return result;
        // }
        // createJson(json,result);

        //  return result;
    }

    /*
    private void createJson(String json, Map<String,String> result) {
        ObjectMapper mapper = new ObjectMapper();
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh");
        int hour= Integer.parseInt(dateFormat.format(new Date()));

        TypeReference<List<DataMonitorBean>> mapType = new TypeReference<List<DataMonitorBean>>() {};
        try{
            List<DataMonitorBean> response = mapper.readValue(json, mapType);
            List<DataMonitorBean> filteredlist = response.stream().filter(x -> Integer.parseInt(new SimpleDateFormat("HH")
                    .format(new Date(Long.getLong(x.getChangeTime())*1000))) >= hour && Integer.parseInt(new SimpleDateFormat("HH")
                    .format(new Date(Long.getLong(x.getChangeTime())*1000))) <= hour).collect(Collectors.toList());
            Map<String, String> returnresult = filteredlist.stream().collect(
                    Collectors.toMap(DataMonitorBean::getChangeTime, DataMonitorBean::getBandwidth));
            result= new TreeMap<>(returnresult);
            System.out.println(returnresult.toString());
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

    } */
}
