/**
 * Copyright 2016-2017 ZTE Corporation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.controller;

import com.alibaba.dubbo.common.utils.IOUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.json.simple.JSONObject;
import org.onap.usecaseui.server.bean.AddModelBean;
import org.onap.usecaseui.server.bo.ModelBean;
import org.onap.usecaseui.server.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/uui-modelservice")
public class ModelController {
    @Resource(name = "ModelService")
    public ModelService modelService;

    @Autowired
    ServletContext servletContext;

    @RequestMapping(value = {"/modelData"}, method = RequestMethod.GET, produces = "application/json")
    public ModelBean getDataFromDCAE() {
        ModelBean result;
        result = modelService.getDataFromDCAE();

        return result;
    }

    @RequestMapping(value = {"/modelData"}, method = RequestMethod.POST, produces = "application/json")
    public void addDataToDCAE(ServletRequest request) {

        String response = new String();


        ServletInputStream inStream = null;
        BufferedReader reader =null;
        String json=null;
        //request.
        try {
            inStream = request.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inStream));
            for (String line; (line = reader.readLine()) != null; response += line);
            json=org.apache.commons.io.IOUtils.toString(reader);

        }
        catch(Exception e)
        {

        }
        finally {
            if (inStream != null) {
                try {
                    inStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Map<String, String> result = new TreeMap<>();

        AddModelBean modelInfoBean = null;

        modelInfoBean = fromRepresentation(response, AddModelBean.class);

        //  String json = orderService.addSite(siteBean);
        /*if (json.indexOf("FAILED") != -1) {
            return result;
        }*/

       // ModelBean result;
        modelService.addModelData(modelInfoBean);


    }

    public static <T> T fromRepresentation(String json, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        T object = null;
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {

            object = mapper.readValue(json, clazz);
        } catch (Exception e) {
            System.out.println("Parse Exception"+e.getMessage());
            // log.error("Error when parsing JSON of object of type {}", clazz.getSimpleName(), e);
        } // return null in case of exception

        return object;
    }

}