/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.onap.usecaseui.server.bean.MonitorServiceBean;
import org.onap.usecaseui.server.bean.NetworkSpecification;
import org.onap.usecaseui.server.bean.VasConfiguration;
import org.onap.usecaseui.server.bean.orderservice.*;
import org.onap.usecaseui.server.service.OrderService;
import org.onap.usecaseui.server.util.OrderServiceEnum;
import org.onap.usecaseui.server.util.UuiCommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Request;
import java.io.FileNotFoundException;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.onap.usecaseui.server.bean.orderservice.*;
import org.springframework.http.HttpStatus;


@RestController
@CrossOrigin(origins="*")
@RequestMapping("/service")
public class OrderServiceController {
    @Resource(name="OrderService")
    public OrderService orderService;

    @Autowired
    ServletContext servletContext;

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceController.class);

    @RequestMapping(value = {"/orderService"}, consumes = "application/json", method = RequestMethod.POST,produces = "application/json")
    public ResponseEntity<ServiceEstimationBean> estimateService(@RequestBody OrderServiceBean orderServiceBean){
        String siteId = "";
        String siteStatus = "";
        System.out.println("Service Order received : "+orderServiceBean.toString());
      /*  List<VpnInformation> vpnInfo = orderServiceBean.getVpnInformations();
        ListIterator<VpnInformation> litr = vpnInfo.listIterator();*/
   /*     while (litr.hasNext()) {
            VpnInformation vpnInformation = litr.next();
            List<Site> sites = vpnInformation.getSites();
            ListIterator<Site> siteListIterator = sites.listIterator();
            while(siteListIterator.hasNext())
            {
                Site site = siteListIterator.next();
                 siteId = site.getSiteId();
                 siteStatus = "Online";
            }
            orderService.updateSiteStatus(siteId, siteStatus);
        }*/
        try {
            ServiceEstimationBean serviceEstimationBean = orderService.estimateService(orderServiceBean);
            return ResponseEntity.ok()
                    .body(serviceEstimationBean);
        }
        catch(Exception ex){
            logger.error("Exception occured while creating the service:"+ex);
        }
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(null);
    }

 /*   @RequestMapping(value = {"/finalPostOrderedServiceData"}, consumes = "application/json", method = RequestMethod.POST,produces = "application/json")
    public ResponseEntity<ServiceEstimationBean> serviceDataToSO(@RequestBody OrderServiceBean orderServiceBean){

        System.out.println("Service Order received : "+orderServiceBean.toString());

        try {
            ServiceEstimationBean serviceEstimationBean = orderService.estimateService(orderServiceBean);
            orderService.sendDatatoSO(serviceEstimationBean);
            return ResponseEntity.ok()
                    .body(null);
        }
        catch(Exception ex){
            logger.error("Exception occured while creating the service:"+ex);
        }
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(null);
    }
*/

    /*
    public void addServiceData(@RequestParam("sessionid") String sessionid, @RequestBody NetworkSpecification networkSpecification ){

        orderService.createService("serviceCache",sessionid, networkSpecification);
        System.out.println("Added to cache...");
        System.out.println(orderService.getService("serviceCache",sessionid).toString());

    }
    */
    @RequestMapping(value = {"/resourceTopology"}, method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<String> dataFromAAIForTopology() throws Exception {
        String finalOrderServiceInfo = orderService.getFinalOrderServiceInfo();
        return ResponseEntity.ok()
                .body(finalOrderServiceInfo);
        //return orderService.getService("serviceCache",sessionid).toString();
    }
    @RequestMapping(value = {"/summary"}, method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<ServiceEstimationBean> getServiceData(){
        ServiceEstimationBean serviceEstimationBean = orderService.getServiceInformation();
        return ResponseEntity.ok()
                .body(serviceEstimationBean);
        //return orderService.getService("serviceCache",sessionid).toString();
    }

    @RequestMapping(value = {"/orderServicesss"}, params = "sessionid", method = RequestMethod.GET,produces = "application/json")
    public String getServiceData(@RequestParam("sessionid") String sessionid){

        return orderService.getService("serviceCache",sessionid).toString();
    }

    @RequestMapping(value = {"/orderServiceUuid"}, method = RequestMethod.GET,produces = "application/json")
    public String getServiceSession(){

        return UuiCommonUtil.getUUID();

    }

    @RequestMapping(value = {"/configureVpn"}, method = RequestMethod.POST,produces = "application/json")
    public void addOrUpdateVpn(@RequestParam("sessionid") String sessionid, @RequestBody List<VasConfiguration> VasConfigurations ){

        orderService.updateService("serviceCache",sessionid, VasConfigurations, OrderServiceEnum.CONFIGURE_VPN);
        System.out.println("Added VPN Configuration to cache...");
        System.out.println(orderService.getService("serviceCache",sessionid).toString());

    }


    @RequestMapping(value = {"/configureBandwidthPolicy"}, method = RequestMethod.POST,produces = "application/json")
    public void addOrUpdateBandwidthPolicy(@RequestParam("sessionid") String sessionid, @RequestBody List<VasConfiguration> VasConfigurations ){

        orderService.updateService("serviceCache",sessionid, VasConfigurations, OrderServiceEnum.CONFIGURE_BANDWIDTH_POLICY);
        System.out.println("Added Bandwidth Configuration to cache...");
        System.out.println(orderService.getService("serviceCache",sessionid).toString());

    }

    @RequestMapping(value = {"/serviceOrderSummary"}, method = RequestMethod.GET,produces = "application/json")
    public String serviceOrderSummary(@RequestParam("sessionid") String sessionid){
        System.out.println("Complete Service Order information is fetched...");
        System.out.println(orderService.getService("serviceCache",sessionid).toString());
        return orderService.getService("serviceCache",sessionid).toString();


    }


    //added by Sindhuri
    // @RequestMapping(value = {"/topology/sites"}, method = RequestMethod.GET, produces = "application/json")
    public String getAllSitesInfo() throws Exception{
        String json = orderService.getServiceInstances();
        if (json.indexOf("FAILED") != -1) {
            return null;
        }

        return json;
    }

    @RequestMapping(value = {"/site"}, method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Site> addSite(@RequestBody Site siteInfo) {
        Map<String, String> result = new TreeMap<>();
        Site siteBean = null;
        //Site siteBean = fromRepresentation(siteInfo, Site.class);
        siteBean = orderService.addSite(siteInfo);
        return ResponseEntity.ok()
                .body(siteBean);
        /*
        String json = orderService.addSite(siteBean);
        if (json.indexOf("FAILED") != -1) {
            return result;
        }


        return result;
        */
    }

    //added by Sindhuri
    /*@RequestMapping(value = {"/ccvpn/activateSite"}, method = RequestMethod.POST)
    public void activateEdge(@RequestParam(required = false) String siteId, @RequestParam(required = false) String siteName) {
        Map<String, String> result = new TreeMap<>();
       *//* Site site = fromRepresentation(edgeInfo, Site.class);
        String siteId = site.getSiteId();
        String siteName = site.getSiteName();*//*
        SiteBean2SO siteBean2SO = new SiteBean2SO();
        siteBean2SO.setSiteId(siteId);
        siteBean2SO.setSiteName(siteName);

         orderService.activateEdge(siteBean2SO);

        *//*if (json.indexOf("FAILED") != -1) {
            return result;
        }


        return result;*//*
    }*/

    public static <T> T fromRepresentation(String json, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        T object = null;
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            object = mapper.readValue(json, clazz);
        } catch (Exception e) {
            // log.error("Error when parsing JSON of object of type {}", clazz.getSimpleName(), e);
        } // return null in case of exception

        return object;
    }

}

