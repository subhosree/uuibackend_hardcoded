/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.controller;

import org.onap.usecaseui.server.bean.orderservice.OrderServiceBean;
import org.onap.usecaseui.server.bean.orderservice.ServiceEstimationBean;
import org.onap.usecaseui.server.service.OrderService;
import org.onap.usecaseui.server.service.SdWanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/sdwan")
public class SdWanController {

    @Resource(name="SdWanService")
    public SdWanService sdWanService;

    @Resource(name="OrderService")
    public OrderService orderService;

    private static final Logger logger = LoggerFactory.getLogger(SdWanController.class);

    @RequestMapping(value = {"/finalPostOrderedServiceData"}, consumes = "application/json", method = RequestMethod.POST,produces = "application/json")
    public ResponseEntity<ServiceEstimationBean> serviceDataToSO(@RequestBody OrderServiceBean orderServiceBean){

        System.out.println("Service Order received : "+orderServiceBean.toString());

        try {
            ServiceEstimationBean serviceEstimationBean = orderService.estimateService(orderServiceBean);
            sdWanService.sendDatatoSO(serviceEstimationBean);
            return ResponseEntity.ok()
                    .body(null);
        }
        catch(Exception ex){
            logger.error("Exception occured while creating the service:"+ex);
        }
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(null);
    }

    @RequestMapping(value = {"/resourceTopology"}, method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<String> dataFromAAIForResourceTopology() throws Exception {
        String finalOrderServiceInfo = sdWanService.getFinalOrderServiceInfo();
        return ResponseEntity.ok()
                .body(finalOrderServiceInfo);
    }

    @RequestMapping(value = {"/resourceTopology5G"}, method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<String> dataToFEForResourceTopology() throws Exception {
        String dataToUUI = sdWanService.sendDataToUUI5G();
        return ResponseEntity.ok()
                .body(dataToUUI);
    }
}
