
/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.controller;


import com.fasterxml.jackson.core.JsonParseException;
import org.onap.usecaseui.server.bean.SiteTopologyBean;
import org.onap.usecaseui.server.service.OrderService;
import org.onap.usecaseui.server.service.TopologyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;


@RestController
@CrossOrigin(origins="*")
@RequestMapping("/topology")
public class Topology {

    @Resource(name="OrderService")
    public OrderService orderService;
    @Resource(name="TopologyService")
    public TopologyService topologyService;

    @RequestMapping(value = {"/sites"}, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getAllSitesInfo() {
        /*Map<String, String> result = new TreeMap<>();*/
        String result ="";
        try {
            String json = orderService.getServiceInstancesSdwan();
            // List<SiteTopologyBean> siteTopologyBeanList = topologyService.getSiteInformation();
            /*return ResponseEntity.ok()
                    .body(json);*/
            return ResponseEntity.status(HttpStatus.OK).body(json);
        }
        catch(IOException e)
        {
            return ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body("Socket time out : socket closed");
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Site Information details not found");
        }

    }

    @RequestMapping(value = {"/sites"}, method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> deleteSite(@RequestParam("siteId") String siteId,@RequestParam("siteName") String siteName) {
        Map<String, String> result = new TreeMap<>();
        try {
            String siteStatus = "Offline";
            String json = orderService.updateSiteStatus(siteId, siteStatus);
            return ResponseEntity.ok()
                    .body(json);
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Site Information details not found");


        }
    }
}
