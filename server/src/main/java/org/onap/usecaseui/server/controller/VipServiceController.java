/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.controller;


import org.onap.usecaseui.server.bean.vipregister.VipInfoBean;
import org.onap.usecaseui.server.bean.vipregister.VipWrapper;
import org.onap.usecaseui.server.service.impl.VipServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/uui-vipservice")
public class VipServiceController {

    @Autowired
    private VipServiceImpl vipServiceImpl;

    @RequestMapping(value = {"/vipService"}, method = RequestMethod.POST,produces = "application/json")
    public ResponseEntity<String> handleFileUpload(@ModelAttribute VipWrapper wrapper) {
        String vipName = wrapper.getVipName();
        String vipRole = wrapper.getVipRole();
        String vipModel = wrapper.getVipPath();
        MultipartFile file=wrapper.getFile();
        System.out.println("Model Value : "+vipModel);

        try {

            vipServiceImpl.addVip(file, vipName, vipRole, vipModel);


            return ResponseEntity.status(HttpStatus.OK).body("Added VIP Information  " + file.getOriginalFilename() + "!");
        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("FAIL to upload " + file.getOriginalFilename() + "!");
        }
    }

    @RequestMapping(value = {"/vipService"}, method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<List<VipInfoBean>> getAllVipsList(HttpServletResponse response) {
        List<VipInfoBean> files = vipServiceImpl.getAllVipFileList();

        return ResponseEntity.ok().body(files);
    }


    @RequestMapping(value = {"/vipImage/{filename:.+}"}, method = RequestMethod.GET)
    public ResponseEntity<byte[]> getVipImage(@PathVariable String filename) {
        //Resource file = vipServiceImpl.getVipImage(filename);
        byte[] image = vipServiceImpl.getVipImage(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"")
                .body(image);

    }

    /*
    @RequestMapping(value = {"/vipService"}, method = RequestMethod.GET)
    public void getAllVips(HttpServletResponse response) {

        // Set the response type and specify the boundary string
        response.setContentType("multipart/x-mixed-replace;boundary=END");

        try {
            vipServiceImpl.getAllVip(response.getOutputStream());
        } catch (Exception e) {
            System.out.println("Exception : "+e.getMessage());
        }


    }
    */

}
