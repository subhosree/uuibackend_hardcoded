/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.controller.customer;


import org.onap.usecaseui.server.bean.customer.ServiceInstance;
import org.onap.usecaseui.server.bean.customer.ServiceInstances;
import org.onap.usecaseui.server.service.customer.CcvpnCustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.*;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/ccvpncustomer")
public class CcvpnCustomerController {

    @Resource(name="CcvpnCustomerService")
    public CcvpnCustomerService customerService;

    @RequestMapping(value = {"/service-subscriptions/service-subscription/{subscriptiontype}/service-instances"}, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getAllServicesInfo(@PathVariable("subscriptiontype") String subscriptiontype) {

        String result ="";
        String customerId="";
        ServiceInstances serviceInstances = customerService.getServiceInstances(customerId, subscriptiontype);

            return ResponseEntity.status(HttpStatus.OK).body(serviceInstances.toString());

    }

    @RequestMapping(value = {"/services/{serviceinstanceid}"}, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getServiceInformation(@PathVariable("serviceInstanceId") String serviceInstanceId) {

        String result ="";
        String serviceType="service-ccvpn";
        try {
            //result = customerService.getServiceInformation(serviceInstanceId);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        }catch (Exception e)
        {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        }
    }

    @RequestMapping(value = {"/servicesubscriptions"}, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getServiceSubscriptions() {

        String result ="";
        String customerId="";
        try {
            result = customerService.querySubscriptionType(customerId);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        }catch (Exception e)
        {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        }
    }

}

