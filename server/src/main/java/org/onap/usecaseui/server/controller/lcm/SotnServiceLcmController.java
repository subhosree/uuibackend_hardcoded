/**
 * Copyright 2016-2017 ZTE Corporation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.controller.lcm;


import org.onap.usecaseui.server.bean.activateEdge.ServiceInstance;
import org.onap.usecaseui.server.bean.lcm.sotne2eservice.FileWrapper;
import org.onap.usecaseui.server.bean.lcm.sotne2eservice.SotnServiceTemplateInput;
import org.onap.usecaseui.server.service.lcm.ServiceLcmService;
import org.onap.usecaseui.server.service.lcm.SotnServiceTemplateService;
import org.onap.usecaseui.server.service.lcm.domain.so.bean.DeleteOperationRsp;
import org.onap.usecaseui.server.service.lcm.domain.so.bean.ServiceOperation;
import org.onap.usecaseui.server.util.UuiCommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@CrossOrigin(origins="*")
@org.springframework.context.annotation.Configuration
@EnableAspectJAutoProxy
public class SotnServiceLcmController {
    private static final Logger logger = LoggerFactory.getLogger(ServiceLcmController.class);

    @Resource(name = "SotnLcmService")
    private SotnServiceTemplateService sotnServiceTemplateService;
    private DeleteOperationRsp deleteOperationRsp;


    public void setServiceLcmService(SotnServiceTemplateService sotnServiceTemplateService) {
        this.sotnServiceTemplateService = sotnServiceTemplateService;
    }

    @ResponseBody
    @RequestMapping(value = {"/uui-lcm/Sotnservices/cost"}, method = RequestMethod.POST, produces = "application/json")
    //@RequestMapping(value = {"/uui-lcm/Sotnservices/cost"}, method = RequestMethod.POST, produces = "application/json")
    public String getSotnServiceCost(HttpServletRequest request) {

        return sotnServiceTemplateService.ServiceCost(request);
    }

    @ResponseBody
    @RequestMapping(value = {"/uui-lcm/Sotnservices"}, method = RequestMethod.POST, produces = "application/json")
    public ServiceOperation instantiateSotnService(HttpServletRequest request) {

        ServiceOperation serviceOperation = sotnServiceTemplateService.instantiateService(request);

        return serviceOperation;
    }

    @ResponseBody
    @RequestMapping(value = {"/uui-lcm/Sotnservices_unni"}, method = RequestMethod.POST, produces = "application/json")
    public String instantiateService_sotnunni(HttpServletRequest request,@RequestBody HashMap<String, Object> mp) {
        System.out.println("request body  == "+mp);
        //ServiceOperation serviceOperation = sotnServiceTemplateService.instantiateService_sotnunni(mp);

        //ServiceOperation serviceOperation = sotnServiceTemplateService.instantiate_CCVPN_Service(mp);

        String response = "{\n" +
                "\"service\":{\n" +
                "    \"name\":\"CCVPNServiceV2-36\",\n" +
                "    \"description\":\"CCVPNServiceV2\",\n" +
                "    \"serviceInvariantUuid\":\"57c8a933-1364-4f85-b7a9-666d80ecc5b6\",\n" +
                "    \"serviceUuid\":\"0734e398-a427-49f2-9abe-de8eb02542ad\",\n" +
                "    \"globalSubscriberId\": \"{{customer}}\",\n" +
                "    \"serviceType\": \"{{service-subscription}}\",\n" +
                "    \"parameters\":{\n" +
                "      \"locationConstraints\":[],\n" +
                "      \"resources\":[],\n" +
                "      \"requestInputs\":{                \n" +
                "        \"sotnUnderlay\":[\n" +
                "          {\n" +
                "            \"l2vpn\":[\n" +
                "              {\n" +
                "                \"l2vpn_COS\": \"123\",         \n" +
                "                \"l2vpn_dualLink\": \"Yes\",\n" +
                "                \"l2vpn_description\": \"VPN Description\",                 \n" +
                "                \"l2vpn_name\": \"VPN2\",\n" +
                "                \"l2vpn_tenantId\": \"989933\",                  \n" +
                "                \"l2vpn_vpnType\": \"SOTN\",         \n" +
                "                \"l2vpn_cbs\": \"123\",                 \n" +
                "                \"l2vpn_ebs\": \"23\",         \n" +
                "                \"l2vpn_colorAware\": \"true\",         \n" +
                "                \"l2vpn_reroute\": \"Yes\",        \n" +
                "                \"l2vpn_couplingFlag\": \"true\",                 \n" +
                "                \"l2vpn_cir\": \"100\",\n" +
                "                \"l2vpn_startTime\": \"28-02-2020\",\n" +
                "                \"l2vpn_endTime\": \"21-02-2020\",         \n" +
                "                \"l2vpn_eir\": \"1000\",         \n" +
                "                \"l2vpn_SLS\": \"1234\"\n" +
                "              }\n" +
                "            ],\n" +
                "            \"sotnUni\":[\n" +
                "              {\n" +
                "                \"sotnuni_cVLAN\": \"Huawei\",\t\t\t\t\t\t\n" +
                "                \"sotnuni_tpId\": \"Huawei-112233\"\n" +
                "              },\n" +
                "              {\n" +
                "                \"sotnuni_cVLAN\": \"Huawei-1\",\t\t\t\t\t\t\n" +
                "                \"sotnuni_tpId\": \"Huawei1-554433\"\n" +
                "              }\n" +
                "            ]\t\t\t\t\t  \t\t\t\t\t\t                 \n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "\n" +
                "}";

        return response;
    }




    @ResponseBody
    @RequestMapping(value = {"/uui-lcm/Sotnservices/serviceStatus/service-instance/{instanceid}"}, method = RequestMethod.GET, produces = "application/json")
    public String getSotnServicestatus(@PathVariable("instanceid") String instanceid) {

        return sotnServiceTemplateService.getSOTNInstantiationstatus(instanceid);

    }


    @ResponseBody
    @RequestMapping(value = {"/uui-lcm/Sotnservices/servicesubscription/{subscriptionType}/serviceinstance/{instanceid}"}, method = RequestMethod.GET, produces = "application/json")
   // @RequestMapping(value = {"ccvpncustomer/servicesubscriptions/uui-lcm/Sotnservices/servicesubscription/{subscriptionType}/serviceinstance/{instanceid}"}, method = RequestMethod.GET, produces = "application/json")
    public String getSotnService(@PathVariable("subscriptionType") String subscriptionType, @PathVariable("instanceid") String instanceid) {

       // return sotnServiceTemplateService.getService(subscriptionType, instanceid);


        return "{\n" +
                "\"service\":{\n" +
                "    \"name\":\"CCVPNServiceV2-36\",\n" +
                "    \"description\":\"CCVPNServiceV2\",\n" +
                "    \"serviceInvariantUuid\":\"57c8a933-1364-4f85-b7a9-666d80ecc5b6\",\n" +
                "    \"serviceUuid\":\"0734e398-a427-49f2-9abe-de8eb02542ad\",\n" +
                "    \"globalSubscriberId\": \"{{customer}}\",\n" +
                "    \"serviceType\": \"{{service-subscription}}\",\n" +
                "    \"parameters\":{\n" +
                "      \"locationConstraints\":[],\n" +
                "      \"resources\":[],\n" +
                "      \"requestInputs\":{                \n" +
                "        \"sotnUnderlay\":[\n" +
                "          {\n" +
                "            \"l2vpn\":[\n" +
                "              {\n" +
                "                \"l2vpn_COS\": \"123\",         \n" +
                "                \"l2vpn_dualLink\": \"Yes\",\n" +
                "                \"l2vpn_description\": \"VPN Description\",                 \n" +
                "                \"l2vpn_name\": \"VPN2\",\n" +
                "                \"l2vpn_tenantId\": \"989933\",                  \n" +
                "                \"l2vpn_vpnType\": \"SOTN\",         \n" +
                "                \"l2vpn_cbs\": \"123\",                 \n" +
                "                \"l2vpn_ebs\": \"23\",         \n" +
                "                \"l2vpn_colorAware\": \"true\",         \n" +
                "                \"l2vpn_reroute\": \"Yes\",        \n" +
                "                \"l2vpn_couplingFlag\": \"true\",                 \n" +
                "                \"l2vpn_cir\": \"100\",\n" +
                "                \"l2vpn_startTime\": \"28-02-2020\",\n" +
                "                \"l2vpn_endTime\": \"21-02-2020\",         \n" +
                "                \"l2vpn_eir\": \"1000\",         \n" +
                "                \"l2vpn_SLS\": \"1234\"\n" +
                "              }\n" +
                "            ],\n" +
                "            \"sotnUni\":[\n" +
                "              {\n" +
                "                \"sotnuni_cVLAN\": \"Huawei\",\t\t\t\t\t\t\n" +
                "                \"sotnuni_tpId\": \"Huawei-112233\"\n" +
                "              },\n" +
                "              {\n" +
                "                \"sotnuni_cVLAN\": \"Huawei-1\",\t\t\t\t\t\t\n" +
                "                \"sotnuni_tpId\": \"Huawei1-554433\"\n" +
                "              }\n" +
                "            ]\t\t\t\t\t  \t\t\t\t\t\t                 \n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n";

        //return serviceOperation;
    }

    @ResponseBody
    @RequestMapping(value = {"/uui-lcm/Sotnservices/servicesubscription/{subscriptionType}/serviceinstance/{instanceid}"}, method = RequestMethod.DELETE, produces = "application/json")
    public DeleteOperationRsp deleteSotnService(HttpServletRequest request, @PathVariable("subscriptionType") String subscriptionType, @PathVariable("instanceid") String instanceid) {

        logger.debug("Triggered controller");

        DeleteOperationRsp deleteOperationRsp = sotnServiceTemplateService.deleteService(instanceid, subscriptionType, request);

        return deleteOperationRsp;

    }

    @ResponseBody
    @RequestMapping(value = {"/uui-lcm/Sotnservices/bandwidth/service-subscriptions/service-subscription/{service-type}/service-instances/service-instance/{serviceInstanceId}"}, method = RequestMethod.GET, produces = "application/json")
    public FileWrapper getCustomerServiceInformation(@PathVariable("service-type") String servicetype, @PathVariable("serviceInstanceId") String serviceInstanceId) throws Exception {

        FileWrapper fileWrapper = sotnServiceTemplateService.getSOTNBandWidthData( servicetype, serviceInstanceId);

        return fileWrapper;
    }

    @ResponseBody
    @RequestMapping(value = {"/uui-lcm/Sotnservices/topology/service/service-subscriptions/service-subscription/{service-type}/service-instances/service-instance/{serviceInstanceId}"}, method = RequestMethod.GET, produces = "application/json")
    public String getSiteInformationTopology(@PathVariable("service-type") String servicetype, @PathVariable("serviceInstanceId") String serviceInstanceId) throws Exception {
       // return sotnServiceTemplateService.getSOTNSiteInformationTopology( servicetype, serviceInstanceId);

        String response= "{\n" +
                "    \"nodes\": [\n" +
                "      {\n" +
                "        \"id\": \"1\",\n" +
                "        \"shape\": \"circularImage\",\n" +
                "        \"image\": \"./assets/images/tpoint.png\",\n" +
                "        \"label\": \"Termination Point\",\n" +
                "        \"color\": \"Green\",\n" +
                "        \"dataNode\": {}\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": \"2\",\n" +
                "        \"shape\": \"circularImage\",\n" +
                "        \"image\": \"./assets/images/edge.png\",\n" +
                "        \"label\": \"Node\",\n" +
                "        \"color\": \"Green\",\n" +
                "        \"dataNode\": {\n" +
                "          \"ethtSvcName\": \"sotn-021-VS-monitored\",\n" +
                "          \"colorAware\": \"true\",\n" +
                "          \"cbs\": \"100\",\n" +
                "          \"couplingFlag\": \"true\",\n" +
                "          \"ebs\": \"evpl\",\n" +
                "          \"cir\": \"200000\",\n" +
                "          \"eir\": \"0\"\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": \"3\",\n" +
                "        \"shape\": \"circularImage\",\n" +
                "        \"image\": \"./assets/images/logicallink.png\",\n" +
                "        \"label\": \"Logical Link\",\n" +
                "        \"color\": \"Green\",\n" +
                "        \"dataNode\": {\n" +
                "          \"ethtSvcName\": \"sotn-021-VS-monitored\",\n" +
                "          \"colorAware\": \"true\",\n" +
                "          \"cbs\": \"100\",\n" +
                "          \"couplingFlag\": \"true\",\n" +
                "          \"ebs\": \"evpl\",\n" +
                "          \"cir\": \"200000\",\n" +
                "          \"eir\": \"0\"\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": \"4\",\n" +
                "        \"shape\": \"circularImage\",\n" +
                "        \"image\": \"./assets/images/edge.png\",\n" +
                "        \"label\": \"Node\",\n" +
                "        \"color\": \"Green\",\n" +
                "        \"dataNode\": {\n" +
                "          \"zipcode\": \"100095\",\n" +
                "          \"siteName\": \"hubtravel\",\n" +
                "          \"description\": \"desc\",\n" +
                "          \"location\": \"laptop-5\",\n" +
                "          \"cvlan\": \"100\"\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": \"5\",\n" +
                "        \"shape\": \"circularImage\",\n" +
                "        \"image\": \"./assets/images/tpoint.png\",\n" +
                "        \"label\": \"Termination Point\",\n" +
                "        \"color\": \"Green\",\n" +
                "        \"dataNode\": {\n" +
                "          \"accessltpid\": \"155\",\n" +
                "          \"siteName\": \"hubtravel\",\n" +
                "          \"description\": \"desc\",\n" +
                "          \"accessnodeid\": \"10.10.10.10\",\n" +
                "          \"cvlan\": \"100\"\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"edges\": [\n" +
                "      {\n" +
                "        \"from\": \"1\",\n" +
                "        \"to\": \"2\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"from\": \"2\",\n" +
                "        \"to\": \"3\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"from\": \"3\",\n" +
                "        \"to\": \"4\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"from\": \"4\",\n" +
                "        \"to\": \"5\"\n" +
                "      }\n" +
                "    ]\n" +
                "  }";

        return response;
    }

    @ResponseBody
        @RequestMapping(value = {"/uui-lcm/Sotnservices/resourceTopology/service/service-subscriptions/service-subscription/{service-type}/service-instances/service-instance/{serviceInstanceId}"}, method = RequestMethod.GET, produces = "application/json")
    public String getResourceInformationTopology(@PathVariable("service-type") String servicetype, @PathVariable("serviceInstanceId") String serviceInstanceId) throws Exception {
        //return sotnServiceTemplateService.getSOTNResourceInformationTopology( servicetype, serviceInstanceId);



        String response = "{\n" +
                "    \"nodes\": [\n" +
                "      {\n" +
                "        \"id\": \"1\",\n" +
                "        \"shape\": \"circularImage\",\n" +
                "        \"image\": \"./assets/images/edge.png\",\n" +
                "        \"label\": \"Node\",\n" +
                "        \"color\": \"Green\",\n" +
                "        \"dataNode\": {\n" +
                "          \"ethtSvcName\": \"sotn-021-VS-monitored\",\n" +
                "          \"colorAware\": \"true\",\n" +
                "          \"cbs\": \"100\",\n" +
                "          \"couplingFlag\": \"true\",\n" +
                "          \"ebs\": \"evpl\",\n" +
                "          \"cir\": \"200000\",\n" +
                "          \"eir\": \"0\"\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": \"2\",\n" +
                "        \"shape\": \"circularImage\",\n" +
                "        \"image\": \"./assets/images/logicallink.png\",\n" +
                "        \"label\": \"Logical Link\",\n" +
                "        \"color\": \"Green\",\n" +
                "        \"dataNode\": {\n" +
                "          \"ethtSvcName\": \"sotn-021-VS-monitored\",\n" +
                "          \"colorAware\": \"true\",\n" +
                "          \"cbs\": \"100\",\n" +
                "          \"couplingFlag\": \"true\",\n" +
                "          \"ebs\": \"evpl\",\n" +
                "          \"cir\": \"200000\",\n" +
                "          \"eir\": \"0\"\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": \"3\",\n" +
                "        \"shape\": \"circularImage\",\n" +
                "        \"image\": \"./assets/images/edge.png\",\n" +
                "        \"label\": \"Node\",\n" +
                "        \"color\": \"Green\",\n" +
                "        \"dataNode\": {\n" +
                "          \"zipcode\": \"100095\",\n" +
                "          \"siteName\": \"hubtravel\",\n" +
                "          \"description\": \"desc\",\n" +
                "          \"location\": \"laptop-5\",\n" +
                "          \"cvlan\": \"100\"\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"edges\": [\n" +
                "      {\n" +
                "        \"from\": \"1\",\n" +
                "        \"to\": \"2\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"from\": \"2\",\n" +
                "        \"to\": \"3\"\n" +
                "      }\n" +
                "    ]\n" +
                "  }";

        return  response;
    }


}