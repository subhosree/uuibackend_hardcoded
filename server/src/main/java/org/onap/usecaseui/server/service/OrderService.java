/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.service;

import org.onap.usecaseui.server.bean.MonitorServiceBean;
import org.onap.usecaseui.server.bean.orderservice.OrderServiceBean;
import org.onap.usecaseui.server.bean.orderservice.ServiceEstimationBean;
import org.onap.usecaseui.server.bean.orderservice.Site;
import org.onap.usecaseui.server.bean.orderservice.SiteBean;
import org.onap.usecaseui.server.bean.orderservice.SiteBean2SO;
import org.onap.usecaseui.server.util.OrderServiceEnum;

import java.io.FileNotFoundException;

public interface OrderService {

    // Old implementation methods: Begin
    public void createService(String cacheName, String key, Object object);
    public MonitorServiceBean getService(String cacheName, String key);
    public void updateService(String cacheName, String key, Object object, OrderServiceEnum stage);
    public void deleteService(String cacheName, String key, Object object);
    public void evictSingleCacheValue(String cacheName, String cacheKey);
    public void postService(String cacheName, String key, Object object);
    public ServiceEstimationBean getServiceInformation();
    // Old implementation methods: End

    // New implementation methods: Begin

    public ServiceEstimationBean estimateService(OrderServiceBean orderServiceBean);
    public Site addSite(Site siteBean);

    // New implementation methods: End

    String getServiceInstances() throws Exception;

    String addSite(SiteBean siteBean);

    //void activateEdge(SiteBean2SO site);
    String updateSiteStatus(String siteId, String siteStatus);

    // public void sendDatatoSO(ServiceEstimationBean serviceEstimationBean);

    String getFinalOrderServiceInfo() throws Exception;

    String getServiceInstancesSdwan() throws Exception;
}