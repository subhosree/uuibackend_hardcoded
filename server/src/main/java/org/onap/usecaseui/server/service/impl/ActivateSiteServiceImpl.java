/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.onap.aai.domain.yang.Device;
import org.onap.usecaseui.server.bean.Customers;
import org.onap.usecaseui.server.bean.ServiceSubscriptions;
import org.onap.usecaseui.server.bean.activateEdge.ComplexObj;
import org.onap.usecaseui.server.bean.activateEdge.Relationship;
import org.onap.usecaseui.server.bean.activateEdge.SiteResource;
import org.onap.usecaseui.server.bean.configuration.StaticConfiguration;
import org.onap.usecaseui.server.bean.orderservice.SiteBean2SO;
import org.onap.usecaseui.server.constant.Constant;
import org.onap.usecaseui.server.service.ActivateSiteService;
import org.onap.usecaseui.server.service.lcm.domain.aai.AAIService;
import org.onap.usecaseui.server.service.lcm.domain.so.SOService;
import org.onap.usecaseui.server.util.RestfulServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

@Service("ActivateSiteService")
@org.springframework.context.annotation.Configuration
public class ActivateSiteServiceImpl implements ActivateSiteService {

    private static final Logger logger = LoggerFactory.getLogger(ActivateSiteServiceImpl.class);

    private SOService soService;

    private AAIService aaiService;

    public ActivateSiteServiceImpl() {
        this(RestfulServices.create(SOService.class),RestfulServices.create(AAIService.class));
    }

    public ActivateSiteServiceImpl(SOService soService, AAIService aaiService) {
        this.soService = soService;
        this.aaiService = aaiService;
    }
    private String queryCustomerId() {
        String customerId = "";
        String result = "";
        ObjectMapper mapper = new ObjectMapper();

        try {
            logger.info("aai queryCustomerId is starting!");

            Response<ResponseBody> response = this.aaiService.getCustomers().execute();
            logger.info("aai queryCustomerId has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());

                //  Customer customer = mapper.readValue(result, Customer.class);
                Customers customer = mapper.readValue(result, Customers.class);
                customerId = customer.getCustomers().get(0).getCustomerId();

            } else {
                //  logger.info(String.format("Failed to get data from AAI[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
                throw new NullPointerException("Failed to get data from AAI");
            }
        } catch (Exception ex) {

            logger.error("queryCustomerId exception occured:" + ex);
            result = Constant.CONSTANT_FAILED;
        }
        return customerId;
    }

    private String querySubscriptionType(String customerId) {
        String subscriptionType = "";
        String result = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            logger.info("aai querySubscriptionType is starting!");

            Response<ResponseBody> response = this.aaiService.getServiceSubscription(customerId).execute();
            logger.info("aai querySubscriptionType has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());
                ServiceSubscriptions subscriptions = mapper.readValue(result, ServiceSubscriptions.class);
                subscriptionType = subscriptions.getServiceSubscriptions().get(0).getServiceType();

            } else {
                // logger.info(String.format("Failed to get data from AAI[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
                throw new NullPointerException("Failed to get data from AAI");
            }

        } catch (Exception ex) {

            logger.error("getServiceSubscription exception occured:" + ex);
            result = Constant.CONSTANT_FAILED;
        }
        return subscriptionType;
    }

    @Override
    public void activateEdge(SiteBean2SO site) {
        String result = "";
        Random rand = new Random();
        int num = rand.nextInt(1000);
        String name = "dummy-"+ num;
        String serviceInstanceName = "";
        String siteName = site.getSiteName();
        String siteId = site.getSiteId();
        ObjectMapper mapper = new ObjectMapper();
      /*  try {
            Response<ResponseBody> rsp = this.aaiService.getSiteResourceInfo(siteId).execute();
            if(rsp.isSuccessful())
            {
                result = new String(rsp.body().bytes());

                SiteResource siteResource = mapper.readValue(result, SiteResource.class);
                List<Relationship> rList = siteResource.getRelationshipList().getRelationship();
                ListIterator<Relationship> itr = rList.listIterator();
                while (itr.hasNext()) {
                    Relationship relationship = itr.next();
                    if (relationship.getRelatedTo().equalsIgnoreCase("service-instance")) {
                        serviceInstanceName = relationship.getRelatedToProperty().get(0).getPropertyValue();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        serviceInstanceName = "499hkg9933RRR";

        String path ="/home/root1/Desktop/SystemConfiguration.json";
        StaticConfiguration staticConfig = ReadStaticConfiguration(path);

        String customerId = staticConfig.getSystemInformation().getCustomerName();
        String serviceType = staticConfig.getSystemInformation().getServiceType();

        String str = "{\n" +
                "    \"service\":{\n" +
                "        \"name\":\"" + name + "\",\n" +
                "        \"description\":\"SOTNVPNInfra\",\n" +
                "        \"serviceInvariantUuid\":\"69161960-515b-4bf3-91f1-313b813f5e1d\",\n" +
                "        \"serviceUuid\":\"0bad8c92-7d22-49f0-b092-b64e6ca564a7\",\n" +
                "        \"globalSubscriberId\":\"" + customerId + "\",\n" +
                "        \"serviceType\":\"" + serviceType + "\",\n" +
                "        \"parameters\":{\n" +
                "            \"locationConstraints\":[\n" +
                "\n" +
                "            ],\n" +
                "            \"resources\":[\n" +
                "                {\n" +
                "                    \"resourceName\":\"simple_nf\",\n" +
                "                    \"resourceInvariantUuid\":\"1a140869-32f9-4e43-b618-9515f54c87cb\",\n" +
                "                    \"resourceUuid\":\"c4eb06cc-f0f5-4a8a-b986-c5daf8af3790\",\n" +
                "                    \"resourceCustomizationUuid\":\"08d3e5be-296c-478e-8d9a-f1825a6e74c8\",\n" +
                "                    \"parameters\":{\n" +
                "                        \"locationConstraints\":[\n" +
                "\n" +
                "                        ],\n" +
                "                        \"resources\":[\n" +
                "\n" +
                "                        ],\n" +
                "                        \"requestInputs\":{\n" +
                "                        \t\"name\" : \"vpn1\",\n" +
                "\t\t\t\t\t\t\t\"description\": \"some_desc\",\n" +
                "\t\t\t\t\t\t\t\"vpnType\": \"ethernet\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                }\n" +
                "            ],\n" +
                "            \"requestInputs\":{\n" +
                "                \"siteName\": \"" + serviceInstanceName + "\",\n" +
                "                \"siteId\": \"" + serviceInstanceName + "\"\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}";
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), str);
        try {
            logger.info("SO activateEdge is starting!");

            Response<ResponseBody> response = this.soService.activateEdge(requestBody).execute();

            logger.info("SO activateEdge has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());
            } else {
                logger.info(String.format("Can not activateEdge[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
            }
        } catch (IOException e) {

            logger.error("activateEdge exception occured:" + e);
            result = Constant.CONSTANT_FAILED;
        }
        //return result;
    }

    public StaticConfiguration ReadStaticConfiguration(String path)
    {
        JSONParser parser = new JSONParser();

        String jsonString = null;
        ObjectMapper mapper = new ObjectMapper();

        try {

            Object object = parser.parse(new FileReader(path));
            //System.out.println(object.toString());
            StaticConfiguration staticConfiguration = mapper.readValue(object.toString(), new TypeReference<StaticConfiguration>() {
            });

            return staticConfiguration;
        }catch (ParseException | IOException ex)
        {
            logger.error("Reading static information from configuration file (StaticConfiguration.json) failed:"+ex.getMessage());
            return null;
        }
    }
}

