/**
 * Copyright 2016-2017 ZTE Corporation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.json.simple.parser.JSONParser;

import okhttp3.ResponseBody;
import okio.BufferedSource;
import org.apache.http.client.methods.RequestBuilder;
import org.json.simple.parser.ParseException;
import org.onap.usecaseui.server.bean.DataMonitorBean;
import org.onap.usecaseui.server.bean.activateEdge.ServiceInstance;
import org.onap.usecaseui.server.bean.lcm.sotne2eservice.Model;
import org.onap.usecaseui.server.bean.lcm.sotne2eservice.ModelConfig;
import org.onap.usecaseui.server.bo.DataMonitorBo;
import org.onap.usecaseui.server.constant.Constant;
import org.onap.usecaseui.server.service.DataMonitorService;
import org.onap.usecaseui.server.service.lcm.domain.aai.AAIService;
import org.onap.usecaseui.server.util.RestfulServices;
import org.onap.usecaseui.server.util.configfile.ConfigFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service("DataMonitorService")
@org.springframework.context.annotation.Configuration
@EnableAspectJAutoProxy
public class DataMonitorServiceImpl implements DataMonitorService {

    private static final Logger logger = LoggerFactory.getLogger(DataMonitorServiceImpl.class);

    private AAIService aaiService;

    private int limit = 20;

    public DataMonitorServiceImpl() { this(RestfulServices.create(AAIService.class)); }

    public DataMonitorServiceImpl(AAIService aaiService) {  this.aaiService = aaiService; }



    public ServiceInstance getServiceInstancesInfo(String customerId, String serviceType, String serviceInstanceId) throws Exception {
        logger.info("Fire getServiceInstances : Begin");
        ObjectMapper mapper = new ObjectMapper();

        Response<ResponseBody> response = this.aaiService.getServiceInstancesForEdge(customerId, serviceType, serviceInstanceId).execute();
        if (response.isSuccessful()) {
            logger.info("Fire getServiceInstances : End");
            String result = new String(response.body().bytes());
            ServiceInstance serviceInstance = mapper.readValue(result, ServiceInstance.class);
            return serviceInstance;
            //System.out.println("Response received : "+response.body().bytes());
        } else {
            logger.info("Fire getServiceInstances : Failed");

        }
        return null;
    }


    @Override
    public List<DataMonitorBo> getDataFromAAI(String vpnId, String requestLimit) throws NullPointerException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

        String currentTime = sdf.format(new Date());

        long different=0;
        List<DataMonitorBo> convertedList = null;
        List<DataMonitorBo> returnlist = null;
        Map<String, String> returnresult = new TreeMap<>();

        limit=Integer.parseInt(requestLimit);
        //limit=readLimit();

        //List<DataMonitorBean> aaidata=readJsonSampleDataFile();
        List<DataMonitorBean> aaidata = null;
        try {
                aaidata = getDataFromAAI();
        }
        catch (NullPointerException e){
                throw e;
        }
        if(null==aaidata)
                return returnlist;

        //System.out.println("Data from AAI : "+aaidata.toString());

        //------------------------------------------------------------------------------------------------------
        convertedList = createJson(aaidata);
        if(convertedList.isEmpty())
            convertedList=createJsonforEntireData(aaidata);

        convertedList = sortBasedOnDate(convertedList);
        List<DateTime> timeList = getLastXSeconds(limit);


        List<DataMonitorBo> mergedsortedconvertedList = mergeAllTimeAaiData(timeList, new ArrayList<DataMonitorBo>(), limit);
        mergedsortedconvertedList.addAll(convertedList);
        returnresult = mergedsortedconvertedList.stream().collect(
                Collectors.toMap(DataMonitorBo::getChangeTime, DataMonitorBo::getBandwidth,(p1, p2) -> p2));

        mergedsortedconvertedList = returnresult.entrySet()
                .stream()
                .map(m->new DataMonitorBo(m.getKey(), m.getValue()))
                .collect(Collectors.toList());
        mergedsortedconvertedList = sortBasedOnDate(mergedsortedconvertedList);


        returnlist=correctBandwidth(mergedsortedconvertedList,convertedList);


        return returnlist;

    }

    public void removeDuplicate(List<DataMonitorBo> mergedsortedconvertedList)
    {

        Collection<DataMonitorBo> distinctElements = mergedsortedconvertedList.stream()
                .<Map<String, DataMonitorBo>> collect(HashMap::new,(m,e)->m.put(e.getChangeTime(), e), Map::putAll)
                .values();

        //System.out.println("Unique data:"+distinctElements.toString());

    }

    public List<DataMonitorBean> getDataFromAAI() throws NullPointerException
    {
        String result="";
        ObjectMapper mapper = new ObjectMapper();
        List<DataMonitorBean> aaiDataList=null;
        try {
            logger.info("aai getDataMonitorInfo is starting!");

            Response<ResponseBody> response = this.aaiService.getDataMonitorInfo().execute();
            logger.info("aai getDataMonitorInfo has finished!");
            if (response.isSuccessful()) {
                result=new String(response.body().bytes());
                result=result.substring(21,result.length()-1);
                aaiDataList = mapper.readValue(result, new TypeReference<List<DataMonitorBean>>() {
                });
            } else {
                logger.info(String.format("Failed to get data from AAI[code=%s, message=%s]", response.code(), response.message()));
                result=Constant.CONSTANT_FAILED;
                throw new NullPointerException("Failed to get data from AAI");
            }

        } catch (IOException ex) {

            logger.error("getDataMonitorInfo exception occured:"+ex);
            result=Constant.CONSTANT_FAILED;
        }
        return aaiDataList;
    }

    public List<DataMonitorBean> readJsonSampleDataFile()
    {
        JSONParser parser = new JSONParser();
        String jsonPath="/home/root1/Desktop/jsondemo.json";
        String jsonString = null;
        ObjectMapper mapper = new ObjectMapper();

        try {

            Object object = parser.parse(new FileReader(jsonPath));
            //System.out.println(object.toString());
            List<DataMonitorBean> aaiDataList = mapper.readValue(object.toString(), new TypeReference<List<DataMonitorBean>>() {
            });

            return aaiDataList;
        }catch (ParseException | IOException ex)
        {
            logger.error("getDataMonitorInfo exception occured:"+ex);
            return null;
        }
    }

    public  List<DataMonitorBo> createJson(List<DataMonitorBean> json) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String hour = dateFormat.format(new Date());


        SimpleDateFormat sdformatter = new SimpleDateFormat("yyyyMMdd");
        sdformatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String hour1=sdformatter.format(new Date());




        //System.out.println("Current Hour : "+hour);

        try {

            List<DataMonitorBean> filteredList = json.stream().filter(x -> x.getChangeTime().substring(0,8).matches(hour+".*") ).collect(Collectors.toList());


            List<DataMonitorBo> dataMonitorBoList =
                    filteredList.stream()
                            .map(changeList)
                            .collect(Collectors.toList());
            //System.out.println(dataMonitorBoList.toString());
            return dataMonitorBoList;

        } catch (Exception e) {
            logger.error("Exception occured while converting data format :"+e);
            System.out.println("getDataMonitorInfo exception occured:"+e);
        }
        return null;
    }

    Function<DataMonitorBean, DataMonitorBo> changeList
            = new Function<DataMonitorBean, DataMonitorBo>() {

        public DataMonitorBo apply(DataMonitorBean dataMonitorBean) {
            DataMonitorBo dataMonitorBo = new DataMonitorBo();
            try { dataMonitorBo.setChangeTime(new SimpleDateFormat("dd-MM-yy HH:mm:ss").format(new SimpleDateFormat("yyyyMMdd-HH:mm:ss:SSS").parse(dataMonitorBean.getChangeTime()))); }
            catch (Exception e) {
                logger.error("Exception occured while converting data format :"+e);
                //System.out.println("Exception : "+e.getMessage());
            }
            dataMonitorBo.setBandwidth(dataMonitorBean.getBandwidth());
            return dataMonitorBo;
        }
    };
    public List<DataMonitorBo> sortBasedOnDate(List<DataMonitorBo> datalist) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss");
        Collections.sort(datalist, (s1, s2) -> LocalDateTime.parse(s1.getChangeTime(), formatter).
                compareTo(LocalDateTime.parse(s2.getChangeTime(), formatter)));
        return datalist;

    }

    public List<DateTime> getLastXSeconds(int limit)
    {
        DateTime currentDate = new DateTime(new Date());
        SimpleDateFormat sdformatter = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
        org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yy HH:mm:ss");
        sdformatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String gmtTime=sdformatter.format(currentDate.toDate());
        DateTime lastDate=((org.joda.time.format.DateTimeFormatter) formatter).parseDateTime(gmtTime);

        List<DateTime> allTime= null;

        try {

            allTime = IntStream.iterate(0, i -> i + 1)
                    .limit(limit)
                    .mapToObj(i -> lastDate.minusSeconds(i))
                    .collect(Collectors.toList());


        }catch (Exception e) {

        }
        //System.out.println("Last Time : "+lastDate);
        //System.out.println("All Time : "+allTime.toString());

        return allTime;

    }

    public List<DataMonitorBo> mergeAllTimeAaiData(List<DateTime> allTime, List<DataMonitorBo> convertedList, int limit) {
        List<DataMonitorBo> subList = null;
        if(limit < convertedList.size())
            subList = convertedList.subList(convertedList.size()-limit, convertedList.size());
        subList = convertedList;


        subList.addAll(allTime.stream()
                .map(convertList)
                .collect(Collectors.toList()));
        return subList;

    }
    Function<DateTime, DataMonitorBo> convertList
            = new Function<DateTime, DataMonitorBo>() {

        public DataMonitorBo apply(DateTime datetime) {
            DataMonitorBo tempdataMonitorBo = new DataMonitorBo();
            org.joda.time.format.DateTimeFormatter fmt = DateTimeFormat.forPattern("dd-MM-yy HH:mm:ss");
            try { tempdataMonitorBo.setChangeTime(fmt.print(datetime)); }
            catch (Exception e) {
                logger.error("Exception occured while converting data format :"+e);
                //System.out.println("Exception : "+e.getMessage());
            }
            tempdataMonitorBo.setBandwidth("-1");
            return tempdataMonitorBo;
        }
    };

    public List<DataMonitorBo> correctBandwidth(List<DataMonitorBo> mergedsortedconvertedList , List<DataMonitorBo> convertedList)
    {
        String lastValidBandwidth="";
        int mergedlistsize=mergedsortedconvertedList.size();
        for (int i=(mergedlistsize-limit+1);i>-1;i--) {
            if(!mergedsortedconvertedList.get(i).getBandwidth().equals("-1")) {
                lastValidBandwidth = mergedsortedconvertedList.get(i).getBandwidth();
                break;
            }
        }

        if(limit < mergedsortedconvertedList.size())
            mergedsortedconvertedList = mergedsortedconvertedList.subList(mergedlistsize-limit, mergedlistsize);



        String lastbandwidth = mergedsortedconvertedList.get(0).getBandwidth();
        String lastdate = mergedsortedconvertedList.get(0).getChangeTime();

        if("-1".equals(lastbandwidth)) {
            DataMonitorBo previousBandwidth = new DataMonitorBo();
            previousBandwidth.setBandwidth(lastValidBandwidth);
            previousBandwidth.setChangeTime(lastdate);
            mergedsortedconvertedList.set(0, previousBandwidth);
        }

        //(int index = aaiDataSize; index > -1 && (new SimpleDateFormat("dd-MM-yy HH:mm:ss").parse(lastAaiDate).before(new SimpleDateFormat("dd-MM-yy HH:mm:ss").parse(lastdate))); index--) {

        for (int index=1; index < mergedsortedconvertedList.size(); index++) {
            if("-1".equals(mergedsortedconvertedList.get(index).getBandwidth())) {
                DataMonitorBo tempBo=new DataMonitorBo();
                tempBo.setChangeTime(mergedsortedconvertedList.get(index).getChangeTime());
                tempBo.setBandwidth(mergedsortedconvertedList.get(index-1).getBandwidth());
                mergedsortedconvertedList.set(index,tempBo);
            }

        }
        return mergedsortedconvertedList;
    }


    public int readLimit()
    {
        Properties prop = new Properties();
        InputStream input = null;
        int limit=20;

        try {

            input = new FileInputStream("/home/root1/Desktop/usecaseui.properties");

            // load a properties file
            prop.load(input);

            // get the property value and assign to limit
            limit = Integer.parseInt(prop.getProperty("CountOfSamples"));
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return limit;
    }


    public  List<DataMonitorBo> createJsonforEntireData(List<DataMonitorBean> json) {

        try {

            List<DataMonitorBo> dataMonitorBoList =
                    json.stream()
                            .map(changeList)
                            .collect(Collectors.toList());

            //System.out.println(dataMonitorBoList.toString());
            return dataMonitorBoList;

        } catch (Exception e) {
            logger.error("Exception occured while converting data format :"+e);
            //System.out.println("getDataMonitorInfo exception occured:"+e);
        }
        return null;
    }
}


