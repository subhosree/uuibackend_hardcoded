/**
 * Copyright 2016-2017 ZTE Corporation.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.impl;

import com.alibaba.dubbo.common.utils.IOUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.json.simple.parser.JSONParser;
import org.onap.usecaseui.server.bean.AddModelBean;
import org.onap.usecaseui.server.bo.ModelBean;
import org.onap.usecaseui.server.constant.Constant;
import org.onap.usecaseui.server.service.ModelService;
import org.onap.usecaseui.server.service.lcm.domain.aai.AAIService;
import org.onap.usecaseui.server.service.lcm.domain.sdnc.SDNCService;
import org.onap.usecaseui.server.service.lcm.domain.sdnc.bean.*;
import org.onap.usecaseui.server.util.RestfulServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.parser.ParseException;
import retrofit2.Response;

@Service("ModelService")
@org.springframework.context.annotation.Configuration
@EnableAspectJAutoProxy
public class ModelServiceImpl implements ModelService {

    private static final Logger logger = LoggerFactory.getLogger(ModelServiceImpl.class);

    private SDNCService sdncService;

    public ModelServiceImpl() {
        this(RestfulServices.create("http://119.3.84.205:30202/restconf/operations/GENERIC-RESOURCE-API:vnf-topology-operation/",SDNCService.class));
    }

    public ModelServiceImpl(SDNCService sdncService) {
        this.sdncService = sdncService;
    }

    @Override
    public ModelBean getDataFromDCAE() {
        ModelBean data = readJsonSampleDataFile();
        return data;
    }

    @Override
    public void addModelData(AddModelBean addModelBean) {
        String result;
        System.out.println("Received...."+ addModelBean.toString());
        List<Param> paramsList = new ArrayList<>();
        Param param1=new Param("modelName",addModelBean.getModelInfoBean().getModelName());
        Param param2=new Param("edgeName",addModelBean.getEdgeName());
        Param param3=new Param("modelUrl",addModelBean.getModelInfoBean().getUrl());
        paramsList.add(param1);
        paramsList.add(param2);
        paramsList.add(param3);
        VnfInputParameters vnfInputParameters = new VnfInputParameters(paramsList);
        Input input = new Input(new SdncRequestHeader("24c2f2bf-677f-43d9-90e2-4f7e3829f694", "update"),
                new RequestInformation("UpdateEdgeModelInstance"), new ServiceInformation("498dad9933HHH"), new VnfInformation(), new VnfRequestInput(vnfInputParameters));
        SdncBean sdncBean = new SdncBean(input);
        String modelName = addModelBean.getModelInfoBean().getModelName();
        String edgeName = addModelBean.getEdgeName();
        String strUrl = addModelBean.getModelInfoBean().getUrl();
        String str="{\n" +
                "   \"input\":{\n" +
                "      \"sdnc-request-header\":{\n" +
                "         \"svc-request-id\":\"24c2f2bf-677f-43d9-90e2-4f7e3829f694\",\n" +
                "         \"svc-action\":\"update\"\n" +
                "      },\n" +
                "      \"request-information\":{\n" +
                "         \"request-action\":\"UpdateEdgeModelInstance\"\n" +
                "      },\n" +
                "      \"service-information\":{\n" +
                "         \"service-instance-id\":\"498dad9933HHH\"\n" +
                "      },\n" +
                "      \"vnf-information\":{\n" +
                "\n" +
                "      },\n" +
                "      \"vnf-request-input\":{\n" +
                "         \"vnf-input-parameters\":{\n" +
                "            \"param\":[\n" +
                "               {\n" +
                "                  \"name\":\"modelUrl\",\n" +
                "                  \"value\":\""+strUrl+"\"\n" +
                //"                  \"value\":\"ftp://root:root@pureftpd:21/model/shashi.pkl\"\n" +
                "               },\n" +
                "               {\n" +
                "                  \"name\":\"modelname\",\n" +
                "                  \"value\":\""+modelName+"\"\n" +
                "               },\n" +
                "               {\n" +
                "                  \"name\":\"edgeName\",\n" +
                "                  \"value\":\""+edgeName+"\"\n" +
                "               }\n" +
                "            ]\n" +
                "         }\n" +
                "      }\n" +
                "   }\n" +
                "}\n";
        RequestBody requestBody =  RequestBody.create(MediaType.parse("application/json"), str);
        System.out.println("Sending..........."+sdncBean.toString());
        try {
            Response<ResponseBody> response =sdncService.addModelData(requestBody).execute();
            if (response.isSuccessful()) {
                result=new String(response.body().bytes());
            } else {
                logger.info(String.format("Add Model Information failed : [code=%s, message=%s]", response.code(), response.message()));
                result=Constant.CONSTANT_FAILED;;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public ModelBean readJsonSampleDataFile() {
        JSONParser parser = new JSONParser();
        //String jsonPath = System.getenv("JSON_FILE_PATH");
        String jsonPath = "/home/root1/Desktop/jsondemo.json";
        ObjectMapper mapper = new ObjectMapper();

        try {

            Object object = parser.parse(new FileReader(jsonPath));
            ModelBean dcaeData = mapper.readValue(object.toString(), new TypeReference<ModelBean>() {
            });

            return dcaeData;
        } catch (ParseException | IOException ex) {
            logger.error("getDataMonitorInfo exception occured:" + ex);
            return null;
        }
    }


}

