/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.onap.usecaseui.server.bean.*;
import org.onap.usecaseui.server.bean.activateEdge.*;
import org.onap.usecaseui.server.bean.VasConfiguration;
import org.onap.usecaseui.server.bean.activateEdge.GenericVnf;
import org.onap.usecaseui.server.bean.activateEdge.Relationship;
import org.onap.usecaseui.server.bean.activateEdge.RelationshipList;
import org.onap.usecaseui.server.bean.activateEdge.SiteResource;
import org.onap.usecaseui.server.bean.beans2SO.*;
import org.onap.usecaseui.server.bean.beans2SO.Parameters;
import org.onap.usecaseui.server.bean.beans2SO.RequestInputs;
import org.onap.usecaseui.server.bean.beans2SO.Resource;
import org.onap.usecaseui.server.bean.configuration.StaticConfiguration;
import org.onap.usecaseui.server.bean.orderservice.*;
import org.onap.usecaseui.server.bean.orderservice.Site;
import org.onap.usecaseui.server.constant.Constant;
import org.onap.usecaseui.server.service.OrderService;
import org.onap.usecaseui.server.service.lcm.domain.aai.AAIService;
import org.onap.usecaseui.server.service.lcm.domain.sdc.bean.SDCServiceTemplate;
import org.onap.usecaseui.server.service.lcm.domain.sdc.bean.Vnf;
import org.onap.usecaseui.server.service.lcm.domain.so.SOService;
import org.onap.usecaseui.server.service.serviceorderbusinesslogic.ServiceOrderBusiness;
import org.onap.usecaseui.server.util.OrderServiceEnum;
import org.onap.usecaseui.server.util.RestfulServices;
import org.onap.usecaseui.server.util.StatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static org.onap.usecaseui.server.util.RestfulServices.create;

@Service("OrderService")
@org.springframework.context.annotation.Configuration
//@EnableAspectJAutoProxy
//@CacheConfig(cacheNames = "customer")
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    private AAIService aaiService;

   /* public OrderServiceImpl() {
        this(RestfulServices.create(AAIService.class));
    }

    public OrderServiceImpl(AAIService aaiService) {
        this.aaiService = aaiService;
    }*/

    private SOService soService;

    public OrderServiceImpl() {
        this(RestfulServices.create(SOService.class), RestfulServices.create(AAIService.class));
    }

    public OrderServiceImpl(SOService soService, AAIService aaiService) {
        this.soService = soService;
        this.aaiService = aaiService;
    }


    @Autowired
    private CacheManager cacheManager;


    @Autowired
    private ServiceOrderBusiness serviceOrderBusiness;


    @Override
    public ServiceEstimationBean estimateService(OrderServiceBean orderServiceBean) {

        //Need to move getcostinformation to spring startup.
        CostInformation costInformation = getCostInformation();
        ServiceEstimationBean serviceEstimationBean = new ServiceEstimationBean();
        copyBean(orderServiceBean, serviceEstimationBean);

        System.out.println("Estimation bean :" + serviceEstimationBean.toString());
        getVpnCost(serviceEstimationBean, costInformation);
        System.out.println("Cost information :" + costInformation.toString());
        try {

            persistserviceInfomation(serviceEstimationBean);

        } catch (Exception ex) {

            logger.error("Exception occured while creating the service:" + ex);

        }
        return serviceEstimationBean;
    }

    /*public void sendDatatoSO(ServiceEstimationBean serviceEstimationBean) {

        SDCServiceTemplate serviceTemplate = getServiceFromSDC();
        List<Vnf> vnfList = getResources();

        Parameters parameters = assignDataToParameters(serviceEstimationBean, vnfList);

        org.onap.usecaseui.server.bean.beans2SO.Service service = new org.onap.usecaseui.server.bean.beans2SO.Service();
        MainService obj = new MainService();
        service.setName(serviceTemplate.getName());
        service.setDescription("SiteService");
        service.setInvariantUUID(serviceTemplate.getInvariantUUID());
        service.setUUID(serviceTemplate.getUuid());
        service.setGlobalSubscriberId("subscriberId");
        service.setServiceType("service-ccvpn");
        service.setParameters(parameters);
        obj.setService(service);
        String res = obj.toString();
        storeOrderServiceInformation(res);
        String result = "";

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), res);
        try {
            logger.info("SO activateEdge is starting!");

            Response<ResponseBody> response = this.soService.orderService(requestBody).execute();

            logger.info("SO orderService has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());
            } else {
                logger.info(String.format("Can not orderService[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
            }
        } catch (IOException e) {

            logger.error("activateEdge exception occured:" + e);
            result = Constant.CONSTANT_FAILED;
        }

    }*/

    private void storeOrderServiceInformation(String orderServiceInfo) {
        String jsonPath = "/home/root1/Desktop/FinalOrderServiceInfo.json";

        File file = new File(jsonPath);
        ObjectMapper mapper = new ObjectMapper();
        try {
            // Serialize Java object info JSON file.
            mapper.writeValue(file, orderServiceInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getFinalOrderServiceInfo() {

        String thisLine = null;
        FileReader file = null;
        StringBuffer buffer = new StringBuffer();
        try {
            // open input stream test.txt for reading purpose.
            file = new FileReader("/home/root1/Desktop/Rsp2UUI.json");
            BufferedReader br = new BufferedReader(file);

            while ((thisLine = br.readLine()) != null) {
                buffer.append(thisLine);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }

    private Parameters assignDataToParameters(ServiceEstimationBean serviceEstimationBean, List<Vnf> vnfList) {
        Parameters parameters = new Parameters();
        List<Resource> resourceList = assignDataToResources(serviceEstimationBean, vnfList);
        org.onap.usecaseui.server.bean.beans2SO.RequestInputs requestInputs = assignDataToRequestInputs(serviceEstimationBean, vnfList);
        parameters.setResources(resourceList);
        parameters.setRequestInputs(requestInputs);
        return parameters;
    }

    private RequestInputs assignDataToRequestInputs(ServiceEstimationBean serviceEstimationBean, List<Vnf> vnfList) {
        RequestInputs requestInputs_ = new RequestInputs();
        List<Vpnresourcelist> sdwanvpnresourceLists = assignDataToVpnReourceLists(serviceEstimationBean);
        List<Sitereourcelist> sdwansiteresourceLists = assignDataToSiteResourceLists(serviceEstimationBean);
        requestInputs_.setVpnresourcelist(sdwanvpnresourceLists);
        requestInputs_.setSitereourcelist(sdwansiteresourceLists);
        return requestInputs_;
    }

    private List<Vpnresourcelist> assignDataToVpnReourceLists(ServiceEstimationBean serviceEstimationBean) {
        List<Vpnresourcelist> list = new ArrayList<>();
        Vpnresourcelist sdwanvpnresourceList = new Vpnresourcelist();
        sdwanvpnresourceList.setSdwanvpnTopology("hub_spoke");
        sdwanvpnresourceList.setSdwanvpnName("defaultvpn");

        List<Sitelanlist> sdwansitelanLists = new ArrayList<>();
        Sitelanlist sdwansitelanList = new Sitelanlist();
        sdwansitelanList.setRole("Hub");
        sdwansitelanList.setPortType("GE");
        sdwansitelanList.setPortSwitch("layer3-port");
        sdwansitelanList.setVlanId("55");
        sdwansitelanList.setIpAddress("192.168.10.1");
        sdwansitelanList.setDeviceName("vCPE");
        sdwansitelanList.setPortNumer("0/0/1");
        sdwansitelanLists.add(sdwansitelanList);

        sdwanvpnresourceList.setSitelanlist(sdwansitelanLists);

        list.add(sdwanvpnresourceList);

        return list;
    }

    private List<Sitereourcelist> assignDataToSiteResourceLists(ServiceEstimationBean serviceEstimationBean) {
        List<Sitereourcelist> lists = new ArrayList<>();

        ListIterator<VpnInformation> listIterator = serviceEstimationBean.getVpnInformations().listIterator();
        while (listIterator.hasNext()) {
            VpnInformation vpnInformation = listIterator.next();
            List<Site> siteList = vpnInformation.getSites();
            ListIterator<Site> listIterator1 = siteList.listIterator();
            while (listIterator1.hasNext()) {
                Site site = listIterator1.next();
                Sitereourcelist sdwansiteresourceList = new Sitereourcelist();
                sdwansiteresourceList.setSiteEmails("email");
                sdwansiteresourceList.setSiteName(site.getSiteName());
                sdwansiteresourceList.setSiteAddress(site.getAddress());
                sdwansiteresourceList.setSiteDescription(site.getDescription());
                sdwansiteresourceList.setSiteRole(site.getRole());
                sdwansiteresourceList.setSitePostcode(site.getZipCode());
                sdwansiteresourceList.setSiteType(site.getType());

                List<Sitewanlist> sdwansitewanList = new ArrayList<>();
                List<String> stringList = serviceEstimationBean.getWlanAccess();
                for (String item : stringList) {
                    Sitewanlist sdwansitewanList1 = new Sitewanlist();
                    sdwansitewanList1.setProviderIpAddress("");
                    sdwansitewanList1.setPortType("GE");
                    sdwansitewanList1.setInputBandwidth(serviceEstimationBean.getBandWidth());
                    sdwansitewanList1.setIpAddress("");
                    sdwansitewanList1.setName("name");
                    sdwansitewanList1.setTransportNetworkName(item);
                    sdwansitewanList1.setOutputBandwidth(serviceEstimationBean.getBandWidth());
                    sdwansitewanList1.setDeviceName("vCPE");
                    sdwansitewanList1.setPortNumber("0/0/0");
                    sdwansitewanList1.setIpMode("DHCP");
                    sdwansitewanList1.setPublicIP("119.3.7.113");

                    sdwansitewanList.add(sdwansitewanList1);
                }
                sdwansiteresourceList.setSitewanlist(sdwansitewanList);

                List<DeviceList> sdwandeviceLists = new ArrayList<>();
                DeviceList sdwandeviceList = new DeviceList();
                sdwandeviceList.setEsn("esn");
                sdwandeviceList.setVersion("Huawei");
                sdwandeviceList.setName("vCPE");
                sdwandeviceList.setType("AR1000V");
                sdwandeviceList.setVersion("1.0");
                sdwandeviceList.setClass_("VNF");
                sdwandeviceList.setSystemIp("20.20.20.1");

                sdwandeviceLists.add(sdwandeviceList);

                sdwansiteresourceList.setDeviceList(sdwandeviceLists);

                lists.add(sdwansiteresourceList);
            }

        }
        return lists;
    }

    private List<Resource> assignDataToResources(ServiceEstimationBean serviceEstimationBean, List<Vnf> vnfList) {
        List<Resource> resourceList = new ArrayList<>();
        ListIterator<Vnf> listIterator = vnfList.listIterator();
        while (listIterator.hasNext()) {
            Vnf vnf = listIterator.next();
            Resource resource = new Resource();
            resource.setResourceIndex("1");
            resource.setName(vnf.getName());
            resource.setInvariantUUID(vnf.getInvariantUUID());
            resource.setUUID(vnf.getUuid());
            resource.setCustomizationUUID("66590e07-0777-415c-af44-36347cf3ddd3");
            Parameters parameters = new Parameters();
            //  resource.setParameters(parameters);
            resourceList.add(resource);

        }

        return resourceList;
    }

    private List<Vnf> getResources() {
        /*DefaultPackageDistributionService distributionService = new DefaultPackageDistributionService();
        List<Vnf> vnfList = distributionService.sdcVfPackageInfo();*/
        JSONParser parser = new JSONParser();
        String jsonPath = "/home/root1/Desktop/VnfList.json";
        ObjectMapper mapper = new ObjectMapper();

        try {

            Object object = parser.parse(new FileReader(jsonPath));
            VnfInformation vnfInformation = mapper.readValue(object.toString(), new TypeReference<VnfInformation>() {
            });

            return vnfInformation.getVnf();
        } catch (ParseException | IOException ex) {
            logger.error("getResources vnfInformation exception occured:" + ex);
            return null;
        }

    }

    private SDCServiceTemplate getServiceFromSDC() {
       /* DefaultServiceTemplateService defaultServiceTemplateService = new DefaultServiceTemplateService();
        List<SDCServiceTemplate> sdcServiceTemplateList = defaultServiceTemplateService.listDistributedServiceTemplate();
        ListIterator<SDCServiceTemplate> iterator = sdcServiceTemplateList.listIterator();
        while(iterator.hasNext())
        {
            SDCServiceTemplate serviceTemplate = iterator.next();
            if(serviceTemplate.getName() == "sdwan")
            {
                return serviceTemplate;
            }
        }
        */

        JSONParser parser = new JSONParser();
        String jsonPath = "/home/root1/Desktop/ServiceInformation.json";
        ObjectMapper mapper = new ObjectMapper();

        try {

            Object object = parser.parse(new FileReader(jsonPath));
            ServiceInformation serviceInformation = mapper.readValue(object.toString(), new TypeReference<ServiceInformation>() {
            });

            return serviceInformation.getServiceInfo();
        } catch (ParseException | IOException ex) {
            logger.error("getResources vnfInformation exception occured:" + ex);
            return null;
        }
    }


    public void copyBean(OrderServiceBean orderServiceBean, ServiceEstimationBean serviceEstimationBean) {

        serviceEstimationBean.setWlanAccess(orderServiceBean.getWlanAccess());
        serviceEstimationBean.setBandWidth(orderServiceBean.getBandWidth());
        serviceEstimationBean.setVpnInformations(orderServiceBean.getVpnInformations());

    }


    public CostInformation getCostInformation() {
        JSONParser parser = new JSONParser();
        String jsonPath = "/home/root1/Desktop/costconfig.json";
        String jsonString = null;
        ObjectMapper mapper = new ObjectMapper();

        try {

            Object object = parser.parse(new FileReader(jsonPath));
            //System.out.println(object.toString());
            CostInformation costInformation = mapper.readValue(object.toString(), new TypeReference<CostInformation>() {
            });

            return costInformation;
        } catch (ParseException | IOException ex) {
            logger.error("getDataMonitorInfo exception occured:" + ex);
            return null;
        }
    }

    public void getSiteCost(ServiceEstimationBean serviceEstimationBean) {
        List<VpnInformation> vpnInfo = serviceEstimationBean.getVpnInformations();
        vpnInfo.forEach(vpn -> {
            vpn.getSites()
                    .forEach(site ->
                    {
                        Integer icost = 0, vpncost = 0;
                        String cost = vpn.getCost();
                        if (cost != null)
                            icost = Integer.parseInt(vpn.getCost());
                        vpn.setCost(String.valueOf(icost + Integer.parseInt(site.getPrice())));
                        String svpncost = serviceEstimationBean.getVpnCost();
                        if (svpncost != null)
                            vpncost = Integer.parseInt(serviceEstimationBean.getVpnCost());
                        serviceEstimationBean.setVpnCost(String.valueOf(vpncost + Integer.parseInt(vpn.getCost())));
                    });
        });
    }

    public void getCameraCost(ServiceEstimationBean serviceEstimationBean, String cameraCost) {
        List<VpnInformation> vpnInfo = serviceEstimationBean.getVpnInformations();
        vpnInfo.forEach(vpn -> {
            int icost = 0, icamcost = 0;
            String cost = vpn.getCost();
            if (cost != null)
                icost = Integer.parseInt(vpn.getCost());
            vpn.setCost(String.valueOf(icost + (Integer.parseInt(cameraCost) * Integer.parseInt(vpn.getCameras())) * vpn.getSites().size()));
            String vpncamcost = serviceEstimationBean.getCameraCost();
            if (vpncamcost != null)
                icamcost = Integer.parseInt(serviceEstimationBean.getCameraCost());
            serviceEstimationBean.setCameraCost(String.valueOf(icamcost + (Integer.parseInt(vpn.getCost()))));
        });

    }

    public void getServiceCost(ServiceEstimationBean serviceEstimationBean, CostInformation costInformation) {
        List<BandwidthCost> bandwidthCost = null;
        Integer iVpnCost = Integer.parseInt(serviceEstimationBean.getVpnCost());
        bandwidthCost = costInformation.getBandwidthCost();
        String serviceBandwith = serviceEstimationBean.getBandWidth();
        Integer iBandwidthCost = Integer.parseInt(bandwidthCost.stream()
                .filter(bandcost -> bandcost.getBandWidth().equalsIgnoreCase(serviceBandwith))
                .map(bandcost -> bandcost.getBandwidthcost())
                .collect(Collectors.toList())
                .get(0));


        Integer iAccessCost = 0;
        List<String> wlanAccess = serviceEstimationBean.getWlanAccess();
        List<String> accessCost = costInformation.getAccessCost()
                .stream()
                .filter(accesscost -> wlanAccess.contains(accesscost.getType()))
                .map(AccessCost::getAccesscost)
                .collect(Collectors.toList());

        for (String cost : accessCost) {
            iAccessCost = iAccessCost + Integer.parseInt(cost);
        }
        serviceEstimationBean.setBandwidthCost(String.valueOf(iAccessCost));
        serviceEstimationBean.setServiceCost(String.valueOf(iVpnCost + iBandwidthCost + iAccessCost));
    }

    public void getVpnCost(ServiceEstimationBean serviceEstimationBean, CostInformation costInformation) {
        getCameraCost(serviceEstimationBean, costInformation.getVpnCamerCost().get(0).getCameraCost());
        getSiteCost(serviceEstimationBean);
        getServiceCost(serviceEstimationBean, costInformation);
    }

    public Site addSite(Site sitebean) {
        String path = "/home/root1/Desktop/SystemConfiguration.json";
        StaticConfiguration staticConfig = ReadStaticConfiguration(path);
        sitebean.setSiteinterface(staticConfig.getSiteInformation().getSiteInterface());
        sitebean.setSubnet(staticConfig.getSiteInformation().getSubnetMask());
        CostInformation costInformation = getCostInformation();
        List<SiteCost> siteCost = costInformation.getSiteCost();
        String price = siteCost.stream().filter(cost -> cost.getSiteType().equalsIgnoreCase(sitebean.getIsCloudSite())).map(SiteCost::getSiteCost).collect(Collectors.toList()).get(0);
        sitebean.setPrice(price);
        return sitebean;
    }

    public StaticConfiguration ReadStaticConfiguration(String path) {
        JSONParser parser = new JSONParser();

        String jsonString = null;
        ObjectMapper mapper = new ObjectMapper();

        try {

            Object object = parser.parse(new FileReader(path));
            //System.out.println(object.toString());
            StaticConfiguration staticConfiguration = mapper.readValue(object.toString(), new TypeReference<StaticConfiguration>() {
            });

            return staticConfiguration;
        } catch (ParseException | IOException ex) {
            logger.error("Reading static information from configuration file (StaticConfiguration.json) failed:" + ex.getMessage());
            return null;
        }
    }


    @Override
    public String getServiceInstances() throws Exception {
        String result = "";
        ObjectMapper mapper = new ObjectMapper();
        List<SiteTopologyBean> beanList = new ArrayList<>();
        CostInformation costInformation = getCostInformation();
        List<SiteCost> siteCost = costInformation.getSiteCost();

        String path = "/home/root1/Desktop/SystemConfiguration.json";
        StaticConfiguration staticConfig = ReadStaticConfiguration(path);

        String customerId = staticConfig.getSystemInformation().getCustomerName();
        String serviceType = staticConfig.getSystemInformation().getServiceType();
        String serviceInstanceId = staticConfig.getSystemInformation().getServiceInstanceId();


        List<Relationship> perfectrelationship = new ArrayList<>();

        ServiceInstance serviceInstance = null;

        //----------------------------- GET SERVICE INSTANCE INFORMATION FROM AAI : BEGIN ---------------------------------------
        try {
            serviceInstance = getServiceInstancesForEdge(customerId, serviceType, serviceInstanceId);
            if (serviceInstance == null)
                return null;
        } catch (Exception e) {
            logger.info("Query Service Instance information failed. No service information found for customer "
                    + customerId + " and Service Type " + serviceType);
            return null;
        }
        //----------------------------- GET SERVICE INSTANCE INFORMATION FROM AAI : END -----------------------------------------

        //----------------------------- Get the detailed information of Generic VNF from the service instance and filter the site resource : BEGIN ---------------------------------------

        RelationshipList genericvnfrelationshipList = serviceInstance.getRelationshipList();
        if (genericvnfrelationshipList == null) {
            logger.info("No VNF associated with the service instance : " + serviceInstanceId);
            return null;
        }
        List<Relationship> genericRelationship = genericvnfrelationshipList.getRelationship();
        List<String> resourceurl = new ArrayList<>();
        for (Relationship tempRelation : genericRelationship) {
            resourceurl.add(tempRelation.getRelatedLink());
        }

        String vnfID = new String(resourceurl.get(0).substring(resourceurl.get(0).lastIndexOf("/") + 1));

        org.onap.usecaseui.server.bean.orderservice.GenericVnf genericVnf = getGenericVnfInfo(vnfID);

        RelationshipList newrelationshipList = genericVnf.getRelationshipList();

        perfectrelationship = newrelationshipList.getRelationship().stream().filter(x -> x.getRelatedTo()
                .equalsIgnoreCase("site-resource")
                || x.getRelatedTo().equalsIgnoreCase("device")
                || x.getRelatedTo().equalsIgnoreCase("edge-camera")).collect(Collectors.toList());


        //----------------------------- Get the detailed information of Generic VNF from the service instance and filter the site resource : END ---------------------------------------


        //----------------------------- Get the VNF information from the service instance and filter the site resource : END ---------------------------------------


        for (Relationship relationship : perfectrelationship) {
            SiteTopologyBean bean = new SiteTopologyBean();
            bean.setEdgeStatus("Offline");
            //Relationship relationship = itr.next();
            if (relationship.getRelatedTo().equalsIgnoreCase("site-resource")) {
                // String relatedLink = relationship.getRelatedLink();
                String siteResourceId = relationship.getRelationshipData().get(0).getRelationshipValue();
                Response<ResponseBody> rsp = this.aaiService.getSiteResourceInfo(siteResourceId).execute();
                if (rsp.isSuccessful()) {
                    result = new String(rsp.body().bytes());

                    SiteResource siteResource = mapper.readValue(result, SiteResource.class);
                    String siteResourceName = siteResource.getSiteResourceName();
                    bean.setSiteStatus(siteResource.getOperationalStatus());
                    String role = siteResource.getRole();
                    bean.setRole(role);
                    String desc = siteResource.getDescription();
                    bean.setDescription(desc);
                    bean.setSiteName(siteResourceName);
                    bean.setSiteId(siteResourceId);
                    List<Relationship> rList = siteResource.getRelationshipList().getRelationship();
                    ListIterator<Relationship> itr1 = rList.listIterator();
                    while (itr1.hasNext()) {
                        Relationship relationship1 = itr1.next();
                        if (relationship1.getRelatedTo().equalsIgnoreCase("complex")) {
                            String complexId = relationship1.getRelationshipData().get(0).getRelationshipValue();
                            Response<ResponseBody> compResp = this.aaiService.getComplexObject(complexId).execute();
                            if (compResp.isSuccessful()) {
                                result = new String(compResp.body().bytes());
                                ComplexObj complexObj = mapper.readValue(result, ComplexObj.class);
                                String zipCode = complexObj.getPostalCode();
                                bean.setZipCode(zipCode);
                                String location = complexObj.getCity();
                                bean.setLocation(location);
                            }
                        }
                    }
                }
            }

            if (relationship.getRelatedTo().equalsIgnoreCase("device")) {
                String deviceId = relationship.getRelationshipData().get(0).getRelationshipValue();
                Map<String, String> resultMaps = getDeviceStatus(deviceId);
                String deviceDesc = resultMaps.get("deviceDesc");
                String deviceName = resultMaps.get("deviceName");
                bean.setDeviceName(deviceName);
                bean.setStatus(resultMaps.get("status"));
                bean.setDeviceDesc(deviceDesc);
            }

            if (relationship.getRelatedTo().equalsIgnoreCase("edge-camera")) {
                String edgeCamId = relationship.getRelationshipData().get(0).getRelationshipValue();
                Response<ResponseBody> edgeRsp = this.aaiService.getEdgeInfo(edgeCamId).execute();
                if (edgeRsp.isSuccessful()) {
                    result = new String(edgeRsp.body().bytes());
                    EdgeCamera edgeInfo = mapper.readValue(result, EdgeCamera.class);
                    String edgeName = edgeInfo.getEdgeCameraName();
                    String edgeIP = edgeInfo.getIpAddress();
                    bean.setEdgeName(edgeName);
                    bean.setEdgeIP(edgeIP);
                    bean.setEdgeStatus("Online");
                }
            }
            if (bean.getStatus() != null && bean.getEdgeStatus() != null) {
                Enum value = generateEnumValue(bean.getStatus(), bean.getEdgeStatus());
                String enumVal = value.name();
                bean.setStatEnum(enumVal);
            }

            if (bean.getRole() != null) {
                if (bean.getRole().toLowerCase().contains("hub")) {
                    String price = siteCost.stream().filter(cost -> cost.getSiteType().equalsIgnoreCase("1"))
                            .map(SiteCost::getSiteCost).collect(Collectors.toList()).get(0);
                    bean.setPrice(price);

                } else {
                    String price = siteCost.stream().filter(cost -> cost.getSiteType().equalsIgnoreCase("0"))
                            .map(SiteCost::getSiteCost).collect(Collectors.toList()).get(0);
                    bean.setPrice(price);
                }
                bean.setSiteinterface(staticConfig.getSiteInformation().getSiteInterface());
                bean.setSubnet(staticConfig.getSiteInformation().getSubnetMask());
                beanList.add(bean);
            }
        }
        return beanList.toString();
    }


    //for sdwan of vodafone demo
    @Override
    public String getServiceInstancesSdwan() throws Exception {
        String result = "";
        String vnfId;
        ObjectMapper mapper = new ObjectMapper();
        List<SiteTopologyBean> beanList = new ArrayList<>();
        CostInformation costInformation = getCostInformation();
        List<SiteCost> siteCost = costInformation.getSiteCost();

        String path = "/home/root1/Desktop/SystemConfiguration.json";
        StaticConfiguration staticConfig = ReadStaticConfiguration(path);

        String customerId = staticConfig.getSystemInformation().getCustomerName();
        String serviceType = staticConfig.getSystemInformation().getServiceType();
        String serviceInstanceId = staticConfig.getSystemInformation().getServiceInstanceId();


        List<Relationship> perfectrelationship = new ArrayList<>();
        List<GenericVnf> genericVnfList = new ArrayList<>();

        ServiceInstance serviceInstance = null;

        //----------------------------- GET SERVICE INSTANCE INFORMATION FROM AAI : BEGIN ---------------------------------------
        try {
            serviceInstance = getServiceInstancesForEdge(customerId, serviceType, serviceInstanceId);
            if (serviceInstance == null)
                return null;
        } catch (Exception e) {
            logger.info("Query Service Instance information failed. No service information found for customer "
                    + customerId + " and Service Type " + serviceType);
            return null;
        }


        List<Relationship> relationshipList = serviceInstance.getRelationshipList().getRelationship().stream().filter(x -> x.getRelatedTo()
                .equalsIgnoreCase("generic-vnf")).collect(Collectors.toList());

        for(Relationship rel : relationshipList)
        {
            String value = rel.getRelatedToProperty().get(0).getPropertyValue();
            if(value.equalsIgnoreCase("SDWAN-site"))
            {
                vnfId = rel.getRelationshipData().get(0).getRelationshipValue();
                GenericVnf tempgenericVnf = getGenericVnfInfo5G(vnfId);
                genericVnfList.add(tempgenericVnf);
            }
        }

        for(GenericVnf genericVnf : genericVnfList)
        {
            RelationshipList genericVnfRelationshipList = genericVnf.getRelationshipList();

            SiteTopologyBean bean = new SiteTopologyBean();

            perfectrelationship = genericVnfRelationshipList.getRelationship().stream().filter(x -> x.getRelatedTo()
                    .equalsIgnoreCase("site-resource")
                    || x.getRelatedTo().equalsIgnoreCase("device")
                    || x.getRelatedTo().equalsIgnoreCase("edge-camera")).collect(Collectors.toList());


            for (Relationship relationship : perfectrelationship) {
                bean.setEdgeStatus("Offline");

                if (relationship.getRelatedTo().equalsIgnoreCase("site-resource")) {
                    String siteResourceId = relationship.getRelationshipData().get(0).getRelationshipValue();
                    Response<ResponseBody> rsp = this.aaiService.getSiteResourceInfo(siteResourceId).execute();
                    if (rsp.isSuccessful()) {
                        result = new String(rsp.body().bytes());

                        SiteResource siteResource = mapper.readValue(result, SiteResource.class);
                        String siteResourceName = siteResource.getSiteResourceName();
                        bean.setSiteStatus(siteResource.getOperationalStatus());
                        String role = siteResource.getRole();
                        bean.setRole(role);
                        String desc = siteResource.getDescription();
                        bean.setDescription(desc);
                        bean.setSiteName(siteResourceName);
                        bean.setSiteId(siteResourceId);
                        List<Relationship> rList = siteResource.getRelationshipList().getRelationship();
                        ListIterator<Relationship> itr1 = rList.listIterator();
                        while (itr1.hasNext()) {
                            Relationship relationship1 = itr1.next();
                            if (relationship1.getRelatedTo().equalsIgnoreCase("complex")) {
                                String complexId = relationship1.getRelationshipData().get(0).getRelationshipValue();
                                Response<ResponseBody> compResp = this.aaiService.getComplexObject(complexId).execute();
                                if (compResp.isSuccessful()) {
                                    result = new String(compResp.body().bytes());
                                    ComplexObj complexObj = mapper.readValue(result, ComplexObj.class);
                                    String zipCode = complexObj.getPostalCode();
                                    bean.setZipCode(zipCode);
                                    String location = complexObj.getCity();
                                    bean.setLocation(location);
                                }
                            }
                        }
                    }
                }

                if (relationship.getRelatedTo().equalsIgnoreCase("device")) {
                    String deviceId = relationship.getRelationshipData().get(0).getRelationshipValue();
                    Map<String, String> resultMaps = getDeviceStatus(deviceId);
                    String deviceDesc = resultMaps.get("deviceDesc");
                    String deviceName = resultMaps.get("deviceName");
                    bean.setDeviceName(deviceName);
                    bean.setStatus(resultMaps.get("status"));
                    bean.setDeviceDesc(deviceDesc);
                }

                if (relationship.getRelatedTo().equalsIgnoreCase("edge-camera")) {
                    String edgeCamId = relationship.getRelationshipData().get(0).getRelationshipValue();
                    Response<ResponseBody> edgeRsp = this.aaiService.getEdgeInfo(edgeCamId).execute();
                    if (edgeRsp.isSuccessful()) {
                        result = new String(edgeRsp.body().bytes());
                        EdgeCamera edgeInfo = mapper.readValue(result, EdgeCamera.class);
                        String edgeName = edgeInfo.getEdgeCameraName();
                        String edgeIP = edgeInfo.getIpAddress();
                        bean.setEdgeName(edgeName);
                        bean.setEdgeIP(edgeIP);
                        bean.setEdgeStatus("Online");
                    }
                }
                if (bean.getStatus() != null && bean.getEdgeStatus() != null) {
                    Enum value = generateEnumValue(bean.getStatus(), bean.getEdgeStatus());
                    String enumVal = value.name();
                    bean.setStatEnum(enumVal);
                }

                if (bean.getRole() != null) {
                    if (bean.getRole().toLowerCase().contains("hub")) {
                        String price = siteCost.stream().filter(cost -> cost.getSiteType().equalsIgnoreCase("1"))
                                .map(SiteCost::getSiteCost).collect(Collectors.toList()).get(0);
                        bean.setPrice(price);

                    } else {
                        String price = siteCost.stream().filter(cost -> cost.getSiteType().equalsIgnoreCase("0"))
                                .map(SiteCost::getSiteCost).collect(Collectors.toList()).get(0);
                        bean.setPrice(price);
                    }
                    bean.setSiteinterface(staticConfig.getSiteInformation().getSiteInterface());
                    bean.setSubnet(staticConfig.getSiteInformation().getSubnetMask());
                }

            }
            beanList.add(bean);
        }

        return beanList.toString();
    }

    public String getCustomQuery(String json, String format, String depth) {
        logger.info("Fire custom query Begin for " + json);
        MediaType mediaType
                = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(mediaType, json);
        try {
            Response<ResponseBody> response = this.aaiService.getCustomQuery(requestBody, format, depth).execute();
            if (response.isSuccessful()) {
                logger.info("Fire custom query : End");
                String result = new String(response.body().bytes());
                return result;
            } else {
                logger.info("Fire custom query : Failed");

            }
        } catch (Exception e) {
            logger.info("Fire custom query : Failed");
            return null;
        }
        return null;
    }


    public ServiceInstance getServiceInstancesForEdge(String customerId, String serviceType, String serviceInstanceId) throws Exception {
        logger.info("Fire getServiceInstancesForEdge : Begin");
        ObjectMapper mapper = new ObjectMapper();

        Response<ResponseBody> response = this.aaiService.getServiceInstancesFor5G(customerId, serviceType, serviceInstanceId).execute();
        if (response.isSuccessful()) {
            logger.info("Fire getServiceInstancesForEdge : End");
            String result = new String(response.body().bytes());
            ServiceInstance serviceInstance = mapper.readValue(result, ServiceInstance.class);
            return serviceInstance;
            //System.out.println("Response received : "+response.body().bytes());
        } else {
            logger.info("Fire getServiceInstancesForEdge : Failed");

        }

        return null;
    }



    public GenericVnf getGenericVnfInfo5G(String vnfID) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        logger.info("Fire getGenericVnfInfo5G : Begin for vnfID" + vnfID);
        Response<ResponseBody> deviceResponse = this.aaiService.getGenericVnfInfo5G(vnfID).execute();
        if (deviceResponse.isSuccessful()) {
            String result = new String(deviceResponse.body().bytes());
            logger.info("Response received getGenericVnfInfo5G");
            GenericVnf genericVnf = mapper.readValue(result, GenericVnf.class);
            return genericVnf;

        } else {
            logger.info("Fire getGenericVnfInfo5G : Failed");
        }
        return null;
    }

    public List<SiteResource> getSiteResource(String siteResourceId) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        logger.info("Fire getSiteResourceInfo : Begin");
        Response<ResponseBody> rsp = this.aaiService.getSiteResourceInfo(siteResourceId).execute();
        if (rsp.isSuccessful()) {
            logger.info("Fire getSiteResourceInfo : End");
            String result = new String(rsp.body().bytes());
            result = result.substring(17, result.length() - 1);
            List<SiteResource> siteResourceList = mapper.readValue(result, new TypeReference<List<SiteResource>>() {
            });
            ;
            return siteResourceList;
        } else {
            logger.info("Fire getSiteResourceInfo : Failed");
        }
        return null;
    }


    public List<ComplexObj> getComplexObjectsfromAAI() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        logger.info("Fire getComplexObject : Begin");
        Response<ResponseBody> compResp = this.aaiService.getAllComplexObject().execute();
        if (compResp.isSuccessful()) {
            String result = new String(compResp.body().bytes());
            result = result.substring(11, result.length() - 1);
            logger.info("Received response for getComplexObject.");
            List<ComplexObj> complexObjList = mapper.readValue(result, new TypeReference<List<ComplexObj>>() {
            });
            return complexObjList;
        } else {
            logger.info("Fire getComplexObject : Failed");
        }
        return null;
    }

    public List<EdgeCamera> getAllEdgeInfoFromAAI() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        logger.info("Fire getEdgeInfo : Begin");
        Response<ResponseBody> edgeRsp = this.aaiService.getAllEdgeInfo().execute();
        if (edgeRsp.isSuccessful()) {
            logger.info("Response received getEdgeInfo");
            String result = new String(edgeRsp.body().bytes());
            result = result.substring(15, result.length() - 1);
            List<EdgeCamera> edgeInfo = mapper.readValue(result, new TypeReference<List<EdgeCamera>>() {
            });
            return edgeInfo;
        } else {
            logger.info("Fire getEdgeInfo : Failed");
        }
        return null;
        //logger.info("Fire getEdgeInfo : End");
    }

    public List<DeviceInfo> getAllDeviceInfoFromAAI() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        logger.info("Fire getDeviceInfo : Begin");
        Response<ResponseBody> deviceResponse = this.aaiService.getAllDeviceInfo().execute();
        if (deviceResponse.isSuccessful()) {
            String result = new String(deviceResponse.body().bytes());
            logger.info("Response received getDeviceInfo");
            result = result.substring(10, result.length() - 1);
            List<DeviceInfo> deviceInfo = mapper.readValue(result, new TypeReference<List<DeviceInfo>>() {
            });
            return deviceInfo;

        } else {
            logger.info("Fire getDeviceInfo : Failed");
        }
        return null;
        //logger.info("Fire getDeviceInfo : End");
    }


    public org.onap.usecaseui.server.bean.orderservice.GenericVnf getGenericVnfInfo(String vnfID) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        logger.info("Fire GetGenericVnfInfo : Begin for vnfID" + vnfID);
        Response<ResponseBody> deviceResponse = this.aaiService.getGenericVnfInformation(vnfID).execute();
        if (deviceResponse.isSuccessful()) {
            String result = new String(deviceResponse.body().bytes());
            logger.info("Response received GetGenericVnfInfo");
            //result = result.substring(10, result.length() - 1);
            org.onap.usecaseui.server.bean.orderservice.GenericVnf genericVnf = mapper.readValue(result, new TypeReference<org.onap.usecaseui.server.bean.orderservice.GenericVnf>() {
            });
            return genericVnf;

        } else {
            logger.info("Fire GetGenericVnfInfo : Failed");
        }
        return null;
    }


    private Enum generateEnumValue(String deviceStatus, String edgeStatus) {
        StatusEnum value = null;
        if (edgeStatus.equalsIgnoreCase("Online") && deviceStatus.equalsIgnoreCase("Online")) {
            value = StatusEnum.GREEN_GREEN;
        } else if (edgeStatus.equalsIgnoreCase("Offline") && deviceStatus.equalsIgnoreCase("Online")) {
            value = StatusEnum.GREEN_GREY;
        } else {
            value = StatusEnum.GREY_GREY;
        }
        return value;
    }


    private String queryCustomerId() {
        String customerId = "";
        String result = "";
        ObjectMapper mapper = new ObjectMapper();

        try {
            logger.info("aai queryCustomerId is starting!");

            Response<ResponseBody> response = this.aaiService.getCustomers().execute();
            logger.info("aai queryCustomerId has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());

                //  Customer customer = mapper.readValue(result, Customer.class);
                Customers customer = mapper.readValue(result, Customers.class);
                // customerId = customer.getCustomers().get(1).getCustomerId();
                customerId = "ONSDEMOBJHKCustomer";

            } else {
                //  logger.info(String.format("Failed to get data from AAI[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
                throw new NullPointerException("Failed to get data from AAI");
            }
        } catch (Exception ex) {

            logger.error("queryCustomerId exception occured:" + ex);
            result = Constant.CONSTANT_FAILED;
        }
        return customerId;
    }

    private String querySubscriptionType(String customerId) {
        String subscriptionType = "";
        String result = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            logger.info("aai querySubscriptionType is starting!");

            Response<ResponseBody> response = this.aaiService.getServiceSubscription(customerId).execute();
            logger.info("aai querySubscriptionType has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());
                ServiceSubscriptions subscriptions = mapper.readValue(result, ServiceSubscriptions.class);
                subscriptionType = subscriptions.getServiceSubscriptions().get(0).getServiceType();

            } else {
                // logger.info(String.format("Failed to get data from AAI[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
                throw new NullPointerException("Failed to get data from AAI");
            }

        } catch (Exception ex) {

            logger.error("getServiceSubscription exception occured:" + ex);
            result = Constant.CONSTANT_FAILED;
        }
        return subscriptionType;
    }

    private Map<String, String> getDeviceStatus(String deviceId) {
        String result = "";
        String status = "";
        Map<String, String> resultMap = new HashMap<>();
        String deviceDesc = "";
        String deviceName = "";
        ObjectMapper mapper = new ObjectMapper();

        try {
            logger.info("aai getDeviceStatus is starting!");

            Response<ResponseBody> response = this.aaiService.getDeviceInfo(deviceId).execute();


            logger.info("aai getDeviceStatus has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());
                DeviceInfo deviceInfo = mapper.readValue(result, new TypeReference<DeviceInfo>() {
                });
                status = deviceInfo.getOperationalStatus();
                deviceDesc = deviceInfo.getDescription();
                deviceName = deviceInfo.getDeviceName();
                if (status.isEmpty()) {
                    status = "activate";
                }
                resultMap.put("status", status);
                resultMap.put("deviceDesc", deviceDesc);
                resultMap.put("deviceName", deviceName);
            } else {
                logger.info(String.format("Can not get getDeviceStatus[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
            }
        } catch (IOException e) {

            logger.error("getDeviceStatus exception occured:" + e);
            result = Constant.CONSTANT_FAILED;
        }
        return resultMap;
    }

    @Override
    public String addSite(SiteBean siteBean) {
        String result = "";
        try {
            logger.info("aai addSite is starting!");

            Response<ResponseBody> response = this.aaiService.addSite(siteBean).execute();

            logger.info("aai addSite has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());
            } else {
                logger.info(String.format("Can not get getDataMonitorInfo[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
            }
        } catch (IOException e) {

            logger.error("addSite exception occured:" + e);
            result = Constant.CONSTANT_FAILED;
        }
        return result;
    }

    @Override
    public String updateSiteStatus(String siteId, String siteStatus) {
        String result = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            logger.info("aai querySubscriptionType is starting!");

            Response<ResponseBody> response = this.aaiService.getSiteResourceInfo(siteId).execute();
            logger.info("aai querySubscriptionType has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());
                SiteResource siteResource = mapper.readValue(result, SiteResource.class);
                siteResource.setOperationalStatus(siteStatus);
                String resourceVersion = siteResource.getResourceVersion();

                MediaType mediaType
                        = MediaType.parse("application/json; charset=utf-8");
                String payload = siteResource.toString();
                RequestBody requestBody = RequestBody.create(mediaType, payload);

                Response<ResponseBody> response1 = aaiService.updateSiteStatus(requestBody, siteId, resourceVersion).execute();
                logger.info("aai createHostUrl has finished");
                if (response1.isSuccessful()) {
                    result = Constant.CONSTANT_SUCCESS;
                } else {
                    result = Constant.CONSTANT_FAILED;
                    logger.error(String.format("Can not createHostUrl[code=%s, message=%s]", response.code(), response.message()));
                }
                return result;

            } else {
                // logger.info(String.format("Failed to get data from AAI[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
                throw new NullPointerException("Failed to get data from AAI");
            }

        } catch (Exception ex) {

            logger.error("getServiceSubscription exception occured:" + ex);
            result = Constant.CONSTANT_FAILED;
        }
        return null;
        //    HttpServletRequest request = new ServletServerHttpRequest();
    }

    public ServiceEstimationBean getServiceInformation() {
        JSONParser parser = new JSONParser();
        String jsonPath = "/home/root1/Desktop/OrderedServiceInformation.json";
        String jsonString = null;
        ObjectMapper mapper = new ObjectMapper();

        try {

            Object object = parser.parse(new FileReader(jsonPath));
            //System.out.println(object.toString());
            ServiceEstimationBean serviceEstimationBean = mapper.readValue(object.toString(), new TypeReference<ServiceEstimationBean>() {
            });

            return serviceEstimationBean;
        } catch (ParseException | IOException ex) {
            logger.error("No saved service exist in the system." + ex);
            return null;
        }
    }

    private void persistserviceInfomation(ServiceEstimationBean serviceEstimationBean) throws Exception {
        String jsonPath = "/home/root1/Desktop/OrderedServiceInformation.json";

        /*
        FileWriter fileWriter = new FileWriter(jsonPath);
        // Writting the jsonObject into sample.json
        fileWriter.write(jsonObject.toJSONString());
        fileWriter.close();
        */
        File file = new File(jsonPath);
        ObjectMapper mapper = new ObjectMapper();
        try {
            // Serialize Java object info JSON file.
            mapper.writeValue(file, serviceEstimationBean);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    /*@Override
    public void activateEdge(SiteBean2SO site) {
        String result="";
        String siteName = site.getSiteName();
        String siteId = site.getSiteId();
        String customerId = queryCustomerId();
        String serviceType = querySubscriptionType(customerId);
        String str = "{\n" +
                "    \"service\":{\n" +
                "        \"name\":\""+siteName+"\",\n" +
                "        \"description\":\"SOTNVPNInfra\",\n" +
                "        \"serviceInvariantUuid\":\"69161960-515b-4bf3-91f1-313b813f5e1d\",\n" +
                "        \"serviceUuid\":\"0bad8c92-7d22-49f0-b092-b64e6ca564a7\",\n" +
                "        \"globalSubscriberId\":\""+customerId+"\",\n" +
                "        \"serviceType\":\""+serviceType+"\",\n" +
                "        \"parameters\":{\n" +
                "            \"locationConstraints\":[\n" +
                "\n" +
                "            ],\n" +
                "            \"resources\":[\n" +
                "                {\n" +
                "                    \"resourceName\":\"simple_nf\",\n" +
                "                    \"resourceInvariantUuid\":\"1a140869-32f9-4e43-b618-9515f54c87cb\",\n" +
                "                    \"resourceUuid\":\"c4eb06cc-f0f5-4a8a-b986-c5daf8af3790\",\n" +
                "                    \"resourceCustomizationUuid\":\"08d3e5be-296c-478e-8d9a-f1825a6e74c8\",\n" +
                "                    \"parameters\":{\n" +
                "                        \"locationConstraints\":[\n" +
                "\n" +
                "                        ],\n" +
                "                        \"resources\":[\n" +
                "\n" +
                "                        ],\n" +
                "                        \"requestInputs\":{\n" +
                "                        \t\"name\" : \"vpn1\",\n" +
                "\t\t\t\t\t\t\t\"description\": \"some_desc\",\n" +
                "\t\t\t\t\t\t\t\"vpnType\": \"ethernet\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                }\n" +
                "            ],\n" +
                "            \"requestInputs\":{\n" +
                "                \"siteName\": \""+siteName+"\",\n" +
                "                \"siteId\": \""+siteId+"\"\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}";
        RequestBody requestBody =  RequestBody.create(MediaType.parse("application/json"), str);
        try {
            logger.info("SO activateEdge is starting!");

            Response<ResponseBody> response = this.soService.activateEdge(requestBody).execute();

            logger.info("SO activateEdge has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());
            } else {
                logger.info(String.format("Can not activateEdge[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
            }
        } catch (IOException e) {

            logger.error("activateEdge exception occured:" + e);
            result = Constant.CONSTANT_FAILED;
        }
        //return result;
    }
*/

    public void createService(String cacheName, String key, Object object) {
        cacheManager.getCache(cacheName).put(key, object);
    }

    public MonitorServiceBean getService(String cacheName, String key) {
        MonitorServiceBean value = null;
        if (cacheManager.getCache(cacheName).get(key) != null) {
            value = (MonitorServiceBean) cacheManager.getCache(cacheName).get(key).get();
        }
        return value;
    }

    @Override
    public void updateService(String cacheName, String key, Object object, OrderServiceEnum stage) {
        MonitorServiceBean value = getService(cacheName, key);
        if (null != value) {
            switch (stage) {
                case CONFIGURE_SITE:
                    break;
                case CONFIGURE_VAS:
                    List vasList = new ArrayList<>();
                    vasList.add((VasConfiguration) object);
                    value.setVasConfigurations(vasList);
                    cacheManager.getCache(cacheName).put(key, value);
                    break;
                case CONFIGURE_BANDWIDTH_POLICY:
                    break;
                case CONFIGURE_VPN:
                    break;
                case ORDER_SUMMARY:
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void postService(String cacheName, String key, Object object) {

    }

    @Override
    public void deleteService(String cacheName, String key, Object object) {

    }

    public void evictSingleCacheValue(String cacheName, String cacheKey) {
        cacheManager.getCache(cacheName).evict(cacheKey);
    }



       /*
            siteResourceList = getSiteResource();
            //--------------------------------------------------------------------
            complexObjList = getComplexObjectsfromAAI();
            //--------------------------------------------------------------------
            edgeInfo = getAllEdgeInfoFromAAI();
            //--------------------------------------------------------------------
            deviceInfo = getAllDeviceInfoFromAAI();
            //--------------------------------------------------------------------

            //serviceInstanceList = serviceInstances.getServiceInstances();
            //perfectserviceInstanceList = serviceInstanceList.stream().filter(x -> !x.getServiceInstanceName().contains("dummy")
            //&& x.getRelationshipList()!=null).collect(Collectors.toList());
            //relationshipList = perfectserviceInstanceList.stream().map(ServiceInstance::getRelationshipList).collect(Collectors.toList());
            //relationship = relationshipList.stream().map(RelationshipList::getRelationship).collect(Collectors.toList());




            siteResourceMap = siteResourceList.stream().collect(
                    Collectors.toMap(SiteResource::getSiteResourceId, Function.identity()));
            deviceInfoMap = deviceInfo.stream().collect(
                    Collectors.toMap(DeviceInfo::getDeviceId, Function.identity()));
            edgeInfoMap = edgeInfo.stream().collect(
                    Collectors.toMap(EdgeCamera::getEdgeCameraId, Function.identity()));
            complexObjMap = complexObjList.stream().collect(
                    Collectors.toMap(ComplexObj::getPhysicalLocationId, Function.identity()));

            */


}