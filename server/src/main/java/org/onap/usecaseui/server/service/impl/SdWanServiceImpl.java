/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.onap.usecaseui.server.bean.activateEdge.AllotedResource;
import org.onap.usecaseui.server.bean.activateEdge.Relationship;
import org.onap.usecaseui.server.bean.activateEdge.ServiceInstance;
import org.onap.usecaseui.server.bean.activateEdge.SiteResource;
import org.onap.usecaseui.server.bean.beans2SO.*;
import org.onap.usecaseui.server.bean.orderservice.*;
import org.onap.usecaseui.server.constant.Constant;
import org.onap.usecaseui.server.service.SdWanService;
import org.onap.usecaseui.server.service.lcm.domain.aai.AAIService;
import org.onap.usecaseui.server.service.lcm.domain.so.SOService;
import org.onap.usecaseui.server.util.RestfulServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import retrofit2.Response;
import org.onap.usecaseui.server.bean.activateEdge.GenericVnf;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service("SdWanService")
@org.springframework.context.annotation.Configuration
public class SdWanServiceImpl implements SdWanService {

    private static final Logger logger = LoggerFactory.getLogger(SdWanServiceImpl.class);

    private AAIService aaiService;

    private SOService soService;

    public SdWanServiceImpl() {
        this(RestfulServices.create(SOService.class), RestfulServices.create(AAIService.class));
    }

    public SdWanServiceImpl(SOService soService, AAIService aaiService) {
        this.soService = soService;
        this.aaiService = aaiService;
    }

    public void sendDatatoSO(ServiceEstimationBean serviceEstimationBean) {

        JSONParser parser = new JSONParser();
        String jsonPath = "/home/root1/Desktop/Configuration.json";
        String jsonString = null;
        ObjectMapper mapper = new ObjectMapper();
        List<Sitereourcelist> lists = new ArrayList<>();
        String res = "";

        ListIterator<VpnInformation> listIterator = serviceEstimationBean.getVpnInformations().listIterator();
        org.onap.usecaseui.server.bean.beans2SO.MainService service = null;

        try {

            Object object = parser.parse(new FileReader(jsonPath));
            service = mapper.readValue(object.toString(), new TypeReference<org.onap.usecaseui.server.bean.beans2SO.MainService>() {
            });

            List<Sitereourcelist> siteresourcelists = service.getService().getParameters().getRequestInputs().getSitereourcelist();
           /* for (Sitereourcelist siteresource : siteresourcelists) {
                while (listIterator.hasNext()) {
                    VpnInformation vpnInformation = listIterator.next();
                    List<Site> siteList = vpnInformation.getSites();
                    ListIterator<Site> listIterator1 = siteList.listIterator();
                    if (listIterator1.hasNext()) {
                        Site site = listIterator1.next();

                        siteresource.setSiteName(site.getSiteName());
                        siteresource.setSiteDescription(site.getDescription());
                        siteresource.setSiteRole(site.getRole());
                        siteresource.setSiteType(site.getType());
                        List<Sitewanlist> sitewanlists = siteresource.getSitewanlist();
                        List<String> stringList = serviceEstimationBean.getWlanAccess();
                        ListIterator<String> stringListIterator = stringList.listIterator();
                        ListIterator<Sitewanlist> listIterator2 = sitewanlists.listIterator();
                        while (listIterator2.hasNext()) {
                            Sitewanlist sitewanlist = listIterator2.next();
                            sitewanlist.setInputBandwidth(serviceEstimationBean.getBandWidth());
                            sitewanlist.setOutputBandwidth(serviceEstimationBean.getBandWidth());
                            sitewanlist.setTransportNetworkName(stringListIterator.next());
                        }
                    }
                }

            }*/
            while (listIterator.hasNext()) {
                VpnInformation vpnInformation = listIterator.next();
                List<Site> siteList = vpnInformation.getSites();
                ListIterator<Site> listIterator1 = siteList.listIterator();
                for (Sitereourcelist siteresource : siteresourcelists) {
                    if (listIterator1.hasNext()) {
                        Site site = listIterator1.next();

                        siteresource.setSiteName(site.getSiteName());
                        siteresource.setSiteDescription(site.getDescription());
                        siteresource.setSiteRole(site.getRole());
                        siteresource.setSiteType(site.getType());
                        List<Sitewanlist> sitewanlists = siteresource.getSitewanlist();
                        List<String> stringList = serviceEstimationBean.getWlanAccess();
                        ListIterator<String> stringListIterator = stringList.listIterator();
                        ListIterator<Sitewanlist> listIterator2 = sitewanlists.listIterator();
                        while (listIterator2.hasNext()) {
                            Sitewanlist sitewanlist = listIterator2.next();
                            sitewanlist.setInputBandwidth(serviceEstimationBean.getBandWidth());
                            sitewanlist.setOutputBandwidth(serviceEstimationBean.getBandWidth());
                            sitewanlist.setTransportNetworkName(stringListIterator.next());
                        }
                    }

                }

            }


            res = service.toString();

        } catch (ParseException | IOException ex) {
            logger.error("getDataMonitorInfo exception occured:" + ex);
        }

        storeOrderServiceInformation(service);
        String result = "";

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), res);
        try {
            logger.info("SO activateEdge is starting!");

            Response<ResponseBody> response = this.soService.orderService(requestBody).execute();

            logger.info("SO orderService has finished!");
            if (response.isSuccessful()) {
                result = new String(response.body().bytes());
            } else {
                logger.info(String.format("Can not orderService[code=%s, message=%s]", response.code(), response.message()));
                result = Constant.CONSTANT_FAILED;
            }
        } catch (IOException e) {

            logger.error("activateEdge exception occured:" + e);
            result = Constant.CONSTANT_FAILED;
        }

    }

    private void storeOrderServiceInformation(org.onap.usecaseui.server.bean.beans2SO.MainService orderServiceInfo) {
        String jsonPath = "/home/root1/Desktop/FinalOrderServiceInfo.json";

        File file = new File(jsonPath);
        ObjectMapper mapper = new ObjectMapper();
        try {
            // Serialize Java object info JSON file.
            mapper.writeValue(file, orderServiceInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getFinalOrderServiceInfo() throws Exception {
        JSONParser parser = new JSONParser();
        String jsonPath = "/home/root1/Desktop/FinalOrderServiceInfo.json";
        String jsonString = null;
        ObjectMapper mapper = new ObjectMapper();
        String color = "";
        Map<String, String> map = null;


        TopologyBean2FE bean2FE = new TopologyBean2FE();

        //Query AAI for status
        map = queryAAIForSiteInfo();

        //Form the bean for UUI FE
        try {
            List<Node> nodeList = new ArrayList<>();
            List<Edge> edgeList = new ArrayList<>();
            Object object = parser.parse(new FileReader(jsonPath));
            org.onap.usecaseui.server.bean.beans2SO.MainService service = mapper.readValue(object.toString(), new TypeReference<org.onap.usecaseui.server.bean.beans2SO.MainService>() {
            });
            List<Sitereourcelist> siteresourcelists = service.getService().getParameters().getRequestInputs().getSitereourcelist();
            for (Sitereourcelist siteresource : siteresourcelists) {
                String siteNameInAAI = siteresource.getSiteName();
                if (map.containsKey(siteNameInAAI)) {
                    color = map.get(siteNameInAAI);
                } else {
                    color = "grey";
                }

                //if siteName exists in AAI db, then proceed
                Node node = new Node();
                node.setColor(color);
                node.setId(siteresource.getId());
                node.setLabel(siteresource.getSiteName());
                node.setShape("circularImage");
                node.setImage("./assets/treeTopology/Site.jpg");
                nodeList.add(node);
                Edge edge = new Edge();
                edge.setFrom("S");
                edge.setTo(node.getId());
                edgeList.add(edge);


                List<Sitewanlist> sitewanlist = siteresource.getSitewanlist();
                List<DeviceList> deviceLists = siteresource.getDeviceList();

                for (Sitewanlist sitewanlist1 : sitewanlist) {
                    Node wanNode = new Node();
                    wanNode.setColor("red");
                    wanNode.setId(sitewanlist1.getId());
                    wanNode.setLabel(sitewanlist1.getName());
                    wanNode.setShape("circularImage");
                    wanNode.setImage("./assets/treeTopology/Site.jpg");
                    nodeList.add(wanNode);

                    Edge wanEdge = new Edge();
                    wanEdge.setFrom(node.getId());
                    wanEdge.setTo(wanNode.getId());
                    edgeList.add(wanEdge);
                }

                for (DeviceList deviceList : deviceLists) {
                    Node deviceNode = new Node();
                    deviceNode.setColor("green");
                    deviceNode.setId(deviceList.getId());
                    deviceNode.setLabel(deviceList.getName());
                    deviceNode.setShape("circularImage");
                    deviceNode.setImage("./assets/treeTopology/device1.png");
                    nodeList.add(deviceNode);

                    Edge deviceEdge = new Edge();
                    deviceEdge.setFrom(node.getId());
                    deviceEdge.setTo(deviceNode.getId());
                    edgeList.add(deviceEdge);
                }

                bean2FE.setNodes(nodeList);
                bean2FE.setEdges(edgeList);
            }


        } catch (ParseException | IOException ex) {
            logger.error("getFinalOrderServiceInfo exception occured:" + ex);
            return null;
        }

        return bean2FE.toString();
    }

    public Map<String, String> queryAAIForSiteInfo() throws Exception {
        String customerId = "CustomerId-1";
        String serviceType = "service-ccvpn";
        String serviceInstanceId = "106";
        String res;
        ServiceInstance serviceInstance = null;
        Map<String, String> map = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();

        try {
            serviceInstance = getServiceInstancesForEdge(customerId, serviceType, serviceInstanceId);
            if (serviceInstance == null)
                return null;
        } catch (Exception e) {
            logger.info("Query Service Instance information failed. No service information found for customer "
                    + customerId + " and Service Type " + serviceType);
            return null;
        }
        String siteColor = "";
        List<Relationship> relationshipList = serviceInstance.getRelationshipList().getRelationship().stream().filter(x -> x.getRelatedTo()
                .equalsIgnoreCase("site-resource")).collect(Collectors.toList());
        for (Relationship relationship : relationshipList) {
            String siteResourceId = relationship.getRelationshipData().get(0).getRelationshipValue();
            Response<ResponseBody> rsp = this.aaiService.getSiteResourceInfo(siteResourceId).execute();
            if (rsp.isSuccessful()) {
                res = new String(rsp.body().bytes());

                SiteResource siteResource = mapper.readValue(res, SiteResource.class);
                String siteStatus = siteResource.getOperationalStatus();
                siteColor = findColor(siteStatus);
                String siteName = siteResource.getSiteResourceName();
                map.put(siteName, siteColor);
            }
        }
        return map;
    }

    private String findColor(String siteStatus) {
        if (siteStatus.equalsIgnoreCase("Online")) {
            return "green";
        } else if (siteStatus.equalsIgnoreCase("Offline")) {
            return "red";
        } else
            return "grey";

    }

    public ServiceInstance getServiceInstancesForEdge(String customerId, String serviceType, String serviceInstanceId) throws Exception {
        logger.info("Fire getServiceInstancesForEdge : Begin");
        ObjectMapper mapper = new ObjectMapper();

        Response<ResponseBody> response = this.aaiService.getServiceInstancesForEdge(customerId, serviceType, serviceInstanceId).execute();
        if (response.isSuccessful()) {
            logger.info("Fire getServiceInstancesForEdge : End");
            String result = new String(response.body().bytes());
            ServiceInstance serviceInstance = mapper.readValue(result, ServiceInstance.class);
            return serviceInstance;
            //System.out.println("Response received : "+response.body().bytes());
        } else {
            logger.info("Fire getServiceInstancesForEdge : Failed");

        }

        return null;
    }


    // For Shanghai MWC 5G
    public String sendDataToUUI() {
        JSONParser parser = new JSONParser();
        String jsonPath = "/home/root1/Desktop/Rsp2UUI.json";
        String jsonPath1 = "/home/root1/Desktop/5GInfo.json";
        ObjectMapper mapper = new ObjectMapper();
        org.onap.usecaseui.server.bean.orderservice.Node node = null;
        org.onap.usecaseui.server.bean.orderservice.TopologyBean2FE topologyBean2FE = null;
        File file = new File(jsonPath1);

        try {
            Object object = parser.parse(new FileReader(jsonPath));
            topologyBean2FE = mapper.readValue(object.toString(), new TypeReference<org.onap.usecaseui.server.bean.orderservice.TopologyBean2FE>() {
            });

        } catch (ParseException | IOException ex) {
            logger.error("getFinalOrderServiceInfo exception occured:" + ex);

        }

        if (file.length() != 0) {
            try {
                Object object = parser.parse(new FileReader(jsonPath1));
                node = mapper.readValue(object.toString(), new TypeReference<org.onap.usecaseui.server.bean.orderservice.Node>() {
                });
                String id = node.getId();
                List<Node> nodeList = topologyBean2FE.getNodes();
                nodeList.add(node);

                topologyBean2FE.setNodes(nodeList);

                List<Edge> edgeList = topologyBean2FE.getEdges();
                Edge edge1 = new Edge();
                edge1.setFrom(id);
                edge1.setTo("R1C3");
                Edge edge2 = new Edge();
                edge2.setFrom("R2");
                edge2.setTo(id);

                edgeList.add(edge1);
                edgeList.add(edge2);

                topologyBean2FE.setEdges(edgeList);

            } catch (ParseException | IOException ex) {
                logger.error("getFinalOrderServiceInfo exception occured:" + ex);
            }
        }
        return topologyBean2FE.toString();
    }

    public String sendDataToUUI5G()
    {
        Map<String, String> map = null;
        JSONParser parser = new JSONParser();
        String jsonPath = "/home/root1/Desktop/Rsp2UUI.json";
        ObjectMapper mapper = new ObjectMapper();
        org.onap.usecaseui.server.bean.orderservice.Node node = new Node();
        org.onap.usecaseui.server.bean.orderservice.TopologyBean2FE topologyBean2FE = null;

        try {
            Object object = parser.parse(new FileReader(jsonPath));
            topologyBean2FE = mapper.readValue(object.toString(), new TypeReference<org.onap.usecaseui.server.bean.orderservice.TopologyBean2FE>() {
            });

        } catch (ParseException | IOException ex) {
            logger.error("getFinalOrderServiceInfo exception occured:" + ex);

        }

      /*  try {
           map = queryAAIFor5G();
           if(!map.isEmpty()) {
               node.setColor("green");
               node.setId("R1C2");
               node.setShape("circularImage");
               node.setImage("./assets/treeTopology/vpn-icon.jpg");
               node.setLabel(map.get("name"));
               DataNode dataNode = new DataNode();
               dataNode.setName(map.get("name"));
               dataNode.setDescription(map.get("desc"));
               dataNode.setType(map.get("type"));
               node.setDataNode(dataNode);
           }

        }catch (Exception e){
            logger.info(e.getMessage());

        }

        List<Node> nodeList = topologyBean2FE.getNodes();

        if(node.getDataNode()!=null) {
            nodeList.add(node);
        }
        topologyBean2FE.setNodes(nodeList);

        List<Edge> edgeList = topologyBean2FE.getEdges();
        Edge edge1 = new Edge();
        edge1.setFrom("R1C2");
        edge1.setTo("R1C3");
        Edge edge2 = new Edge();
        edge2.setFrom("R2");
        edge2.setTo("R1C2");

        edgeList.add(edge1);
        edgeList.add(edge2);

        topologyBean2FE.setEdges(edgeList);*/
        return topologyBean2FE.toString();
    }

    private Map<String,String> queryAAIFor5G() throws Exception{
        String customerId = "dublinCustomer12";
        String serviceType = "service-ccvpn";
        String serviceInstanceId = "dublin-IS12";
        String vnfId;
        String allotedResourceId = null;
        ServiceInstance serviceInstance = null;
        Map<String, String> map = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        GenericVnf genericVnf = null;

        try {
            serviceInstance = getServiceInstancesFor5G(customerId, serviceType, serviceInstanceId);
            if (serviceInstance == null){}
            // return null;
        } catch (Exception e) {
            logger.info("Query Service Instance information failed. No service information found for customer "
                    + customerId + " and Service Type " + serviceType);
            //return null;
        }
        List<Relationship> relationshipList = serviceInstance.getRelationshipList().getRelationship().stream().filter(x -> x.getRelatedTo()
                .equalsIgnoreCase("generic-vnf")).collect(Collectors.toList());
        for (Relationship relationship : relationshipList) {
            vnfId = relationship.getRelationshipData().get(0).getRelationshipValue();
            genericVnf = getGenericVnfInfo5G(vnfId);
        }
        List<Relationship> relationshipList1 = genericVnf.getRelationshipList().getRelationship().stream().filter(x -> x.getRelatedTo()
                .equalsIgnoreCase("allotted-resource")).collect(Collectors.toList());
        if(!relationshipList1.isEmpty()) {
            Relationship relationship = relationshipList1.get(0);
            allotedResourceId = relationship.getRelationshipData().get(3).getRelationshipValue();

            AllotedResource allotedResource = getAllotedResource5G(allotedResourceId);
            String allotedResourceName = allotedResource.getAllottedResourceName();
            String description = allotedResource.getDescription();
            String type = allotedResource.getType();

            map.put("name", allotedResourceName);
            map.put("desc", description);
            map.put("type", type);
        }
        return map;

    }

    public AllotedResource getAllotedResource5G(String allotedResourceId) throws IOException {
        String customerId = "dublinCustomer12";
        String serviceType = "service-ccvpn";
        String serviceInstanceId = "dublin-IS12";
        logger.info("Fire getServiceInstancesForEdge : Begin");
        ObjectMapper mapper = new ObjectMapper();

        Response<ResponseBody> response = this.aaiService.getAllotedResourceFor5G(customerId, serviceType, serviceInstanceId,allotedResourceId).execute();
        if (response.isSuccessful()) {
            logger.info("Fire getServiceInstancesForEdge : End");
            String result = new String(response.body().bytes());
            AllotedResource allotedResource = mapper.readValue(result, AllotedResource.class);
            return allotedResource;
            //System.out.println("Response received : "+response.body().bytes());
        } else {
            logger.info("Fire getServiceInstancesForEdge : Failed");

        }

        return null;
    }

    public ServiceInstance getServiceInstancesFor5G(String customerId, String serviceType, String serviceInstanceId) throws Exception {
        logger.info("Fire getServiceInstancesForEdge : Begin");
        ObjectMapper mapper = new ObjectMapper();

        Response<ResponseBody> response = this.aaiService.getServiceInstancesFor5G(customerId, serviceType, serviceInstanceId).execute();
        if (response.isSuccessful()) {
            logger.info("Fire getServiceInstancesForEdge : End");
            String result = new String(response.body().bytes());
            ServiceInstance serviceInstance = mapper.readValue(result, ServiceInstance.class);
            return serviceInstance;
            //System.out.println("Response received : "+response.body().bytes());
        } else {
            logger.info("Fire getServiceInstancesForEdge : Failed");

        }

        return null;
    }

    public GenericVnf getGenericVnfInfo5G(String vnfID) throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();
        logger.info("Fire GetGenericVnfInfo : Begin for vnfID"+vnfID);
        Response<ResponseBody> deviceResponse = this.aaiService.getGenericVnfInfo5G(vnfID).execute();
        if (deviceResponse.isSuccessful()) {
            String result = new String(deviceResponse.body().bytes());
            logger.info("Response received GetGenericVnfInfo");
            //result = result.substring(10, result.length() - 1);
            GenericVnf genericVnf = mapper.readValue(result, GenericVnf.class);
            return genericVnf;

        } else {
            logger.info("Fire GetGenericVnfInfo : Failed");
        }
        return null;
    }

}
