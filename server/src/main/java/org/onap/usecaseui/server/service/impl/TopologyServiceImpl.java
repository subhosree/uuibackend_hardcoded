/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.onap.usecaseui.server.bean.SiteTopologyBean;
import org.onap.usecaseui.server.bean.orderservice.CostInformation;
import org.onap.usecaseui.server.service.TopologyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

@Service("TopologyService")
public class TopologyServiceImpl implements TopologyService {

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);
    @Override
    public List<SiteTopologyBean> getSiteInformation() throws Exception {
        return readJsonSiteInfo();
    }

    public List<SiteTopologyBean> readJsonSiteInfo()
    {
        JSONParser parser = new JSONParser();
        String jsonPath="/home/root1/Desktop/TopologyInfo.json";
        String jsonString = null;
        ObjectMapper mapper = new ObjectMapper();

        try {

            Object object = parser.parse(new FileReader(jsonPath));
            //System.out.println(object.toString());
            List<SiteTopologyBean> SiteTopologyBeans = mapper.readValue(object.toString(), new TypeReference<List<SiteTopologyBean>>() {
            });

            return SiteTopologyBeans;
        }catch (ParseException | IOException ex)
        {
            logger.error("getDataMonitorInfo exception occured:"+ex);
            return null;
        }
    }
}
