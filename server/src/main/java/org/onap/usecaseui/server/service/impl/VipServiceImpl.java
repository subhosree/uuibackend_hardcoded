/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.impl;

import org.onap.usecaseui.server.bean.vipregister.VipInfoBean;
import org.onap.usecaseui.server.service.VipService;
import org.onap.usecaseui.server.service.serviceorderbusinesslogic.VipServiceBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

@Component
public class VipServiceImpl implements VipService {

    @Autowired
    private VipServiceBusiness vipServiceBusiness;



    @Override
    public void addVip(MultipartFile file, String vipName, String vipRole, String vipModel) throws Exception{

        //vipServiceBusiness.saveFile(file,vipName,vipRole);
        vipServiceBusiness.saveVipToDB(file,vipName,vipRole,vipModel);


    }



    public List<VipInfoBean>  getAllVipFileList()
    {
        return vipServiceBusiness.getAllVipInfoFromDb();
        //return vipServiceBusiness.getAllVipfileList();

    }


    public byte[] getVipImage(String fileName)
    {
        byte[] image= vipServiceBusiness.getVipImageFromDb(fileName);
        //return vipServiceBusiness.loadFile(fileName);
        return image;
    }

    /*
    @Override
    public void getAllVip(ServletOutputStream out) throws Exception{

        File[] allVips=vipServiceBusiness.getAllVipfiles();
        out.println();
        out.println("--END");
        for (File file : allVips) {

            // Get the file
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);

            } catch (FileNotFoundException fnfe) {
                // If the file does not exists, continue with the next file
                System.out.println("Couldfind file " + file.getAbsolutePath());
                continue;
            }

            BufferedInputStream fif = new BufferedInputStream(fis);

            // Print the content type
            out.println("Content-type: text/rtf");
            out.println("Content-Disposition: attachment; filename=" + file.getName());
            out.println();

            System.out.println("Sending " + file.getName());

            // Write the contents of the file
            int data = 0;
            while ((data = fif.read()) != -1) {
                out.write(data);
            }
            fif.close();

            // Print the boundary string
            out.println();
            out.println("--END");
            out.flush();
            System.out.println("Finisheding file " + file.getName());
        }

        // Print the ending boundary string
        out.println("--END--");
        out.flush();
        out.close();

    }
    */
}
