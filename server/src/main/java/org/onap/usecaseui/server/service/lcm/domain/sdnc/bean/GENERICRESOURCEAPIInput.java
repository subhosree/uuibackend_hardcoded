/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:request-information",
        "GENERIC-RESOURCE-API:sdnc-request-header",
        "GENERIC-RESOURCE-API:service-information",
        "GENERIC-RESOURCE-API:vnf-information",
        "GENERIC-RESOURCE-API:vnf-request-input"
})
public class GENERICRESOURCEAPIInput {
    @JsonProperty("GENERIC-RESOURCE-API:request-information")
    private GENERICRESOURCEAPIRequestInformation gENERICRESOURCEAPIRequestInformation;
    @JsonProperty("GENERIC-RESOURCE-API:sdnc-request-header")
    private GENERICRESOURCEAPISdncRequestHeader gENERICRESOURCEAPISdncRequestHeader;
    @JsonProperty("GENERIC-RESOURCE-API:service-information")
    private GENERICRESOURCEAPIServiceInformation gENERICRESOURCEAPIServiceInformation;
    @JsonProperty("GENERIC-RESOURCE-API:vnf-information")
    private GENERICRESOURCEAPIVnfInformation gENERICRESOURCEAPIVnfInformation;
    @JsonProperty("GENERIC-RESOURCE-API:vnf-request-input")
    private GENERICRESOURCEAPIVnfRequestInput gENERICRESOURCEAPIVnfRequestInput;

    @JsonProperty("GENERIC-RESOURCE-API:request-information")
    public GENERICRESOURCEAPIRequestInformation getGENERICRESOURCEAPIRequestInformation() {
        return gENERICRESOURCEAPIRequestInformation;
    }

    @JsonProperty("GENERIC-RESOURCE-API:request-information")
    public void setGENERICRESOURCEAPIRequestInformation(GENERICRESOURCEAPIRequestInformation gENERICRESOURCEAPIRequestInformation) {
        this.gENERICRESOURCEAPIRequestInformation = gENERICRESOURCEAPIRequestInformation;
    }

    @JsonProperty("GENERIC-RESOURCE-API:sdnc-request-header")
    public GENERICRESOURCEAPISdncRequestHeader getGENERICRESOURCEAPISdncRequestHeader() {
        return gENERICRESOURCEAPISdncRequestHeader;
    }

    @JsonProperty("GENERIC-RESOURCE-API:sdnc-request-header")
    public void setGENERICRESOURCEAPISdncRequestHeader(GENERICRESOURCEAPISdncRequestHeader gENERICRESOURCEAPISdncRequestHeader) {
        this.gENERICRESOURCEAPISdncRequestHeader = gENERICRESOURCEAPISdncRequestHeader;
    }

    @JsonProperty("GENERIC-RESOURCE-API:service-information")
    public GENERICRESOURCEAPIServiceInformation getGENERICRESOURCEAPIServiceInformation() {
        return gENERICRESOURCEAPIServiceInformation;
    }

    @JsonProperty("GENERIC-RESOURCE-API:service-information")
    public void setGENERICRESOURCEAPIServiceInformation(GENERICRESOURCEAPIServiceInformation gENERICRESOURCEAPIServiceInformation) {
        this.gENERICRESOURCEAPIServiceInformation = gENERICRESOURCEAPIServiceInformation;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-information")
    public GENERICRESOURCEAPIVnfInformation getGENERICRESOURCEAPIVnfInformation() {
        return gENERICRESOURCEAPIVnfInformation;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-information")
    public void setGENERICRESOURCEAPIVnfInformation(GENERICRESOURCEAPIVnfInformation gENERICRESOURCEAPIVnfInformation) {
        this.gENERICRESOURCEAPIVnfInformation = gENERICRESOURCEAPIVnfInformation;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-request-input")
    public GENERICRESOURCEAPIVnfRequestInput getGENERICRESOURCEAPIVnfRequestInput() {
        return gENERICRESOURCEAPIVnfRequestInput;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-request-input")
    public void setGENERICRESOURCEAPIVnfRequestInput(GENERICRESOURCEAPIVnfRequestInput gENERICRESOURCEAPIVnfRequestInput) {
        this.gENERICRESOURCEAPIVnfRequestInput = gENERICRESOURCEAPIVnfRequestInput;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPIRequestInformation", gENERICRESOURCEAPIRequestInformation).append("gENERICRESOURCEAPISdncRequestHeader", gENERICRESOURCEAPISdncRequestHeader).append("gENERICRESOURCEAPIServiceInformation", gENERICRESOURCEAPIServiceInformation).append("gENERICRESOURCEAPIVnfInformation", gENERICRESOURCEAPIVnfInformation).append("gENERICRESOURCEAPIVnfRequestInput", gENERICRESOURCEAPIVnfRequestInput).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPIServiceInformation).append(gENERICRESOURCEAPIRequestInformation).append(gENERICRESOURCEAPISdncRequestHeader).append(gENERICRESOURCEAPIVnfRequestInput).append(gENERICRESOURCEAPIVnfInformation).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPIInput) == false) {
            return false;
        }
        GENERICRESOURCEAPIInput rhs = ((GENERICRESOURCEAPIInput) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPIServiceInformation, rhs.gENERICRESOURCEAPIServiceInformation).append(gENERICRESOURCEAPIRequestInformation, rhs.gENERICRESOURCEAPIRequestInformation).append(gENERICRESOURCEAPISdncRequestHeader, rhs.gENERICRESOURCEAPISdncRequestHeader).append(gENERICRESOURCEAPIVnfRequestInput, rhs.gENERICRESOURCEAPIVnfRequestInput).append(gENERICRESOURCEAPIVnfInformation, rhs.gENERICRESOURCEAPIVnfInformation).isEquals();
    }
}
