/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:model-customization-uuid",
        "GENERIC-RESOURCE-API:model-invariant-uuid",
        "GENERIC-RESOURCE-API:model-uuid",
        "GENERIC-RESOURCE-API:model-version",
        "GENERIC-RESOURCE-API:model-name"
})
public class GENERICRESOURCEAPIOnapModelInformation {

    @JsonProperty("GENERIC-RESOURCE-API:model-customization-uuid")
    private String gENERICRESOURCEAPIModelCustomizationUuid;
    @JsonProperty("GENERIC-RESOURCE-API:model-invariant-uuid")
    private String gENERICRESOURCEAPIModelInvariantUuid;
    @JsonProperty("GENERIC-RESOURCE-API:model-uuid")
    private String gENERICRESOURCEAPIModelUuid;
    @JsonProperty("GENERIC-RESOURCE-API:model-version")
    private String gENERICRESOURCEAPIModelVersion;
    @JsonProperty("GENERIC-RESOURCE-API:model-name")
    private String gENERICRESOURCEAPIModelName;

    @JsonProperty("GENERIC-RESOURCE-API:model-customization-uuid")
    public String getGENERICRESOURCEAPIModelCustomizationUuid() {
        return gENERICRESOURCEAPIModelCustomizationUuid;
    }

    @JsonProperty("GENERIC-RESOURCE-API:model-customization-uuid")
    public void setGENERICRESOURCEAPIModelCustomizationUuid(String gENERICRESOURCEAPIModelCustomizationUuid) {
        this.gENERICRESOURCEAPIModelCustomizationUuid = gENERICRESOURCEAPIModelCustomizationUuid;
    }

    @JsonProperty("GENERIC-RESOURCE-API:model-invariant-uuid")
    public String getGENERICRESOURCEAPIModelInvariantUuid() {
        return gENERICRESOURCEAPIModelInvariantUuid;
    }

    @JsonProperty("GENERIC-RESOURCE-API:model-invariant-uuid")
    public void setGENERICRESOURCEAPIModelInvariantUuid(String gENERICRESOURCEAPIModelInvariantUuid) {
        this.gENERICRESOURCEAPIModelInvariantUuid = gENERICRESOURCEAPIModelInvariantUuid;
    }

    @JsonProperty("GENERIC-RESOURCE-API:model-uuid")
    public String getGENERICRESOURCEAPIModelUuid() {
        return gENERICRESOURCEAPIModelUuid;
    }

    @JsonProperty("GENERIC-RESOURCE-API:model-uuid")
    public void setGENERICRESOURCEAPIModelUuid(String gENERICRESOURCEAPIModelUuid) {
        this.gENERICRESOURCEAPIModelUuid = gENERICRESOURCEAPIModelUuid;
    }

    @JsonProperty("GENERIC-RESOURCE-API:model-version")
    public String getGENERICRESOURCEAPIModelVersion() {
        return gENERICRESOURCEAPIModelVersion;
    }

    @JsonProperty("GENERIC-RESOURCE-API:model-version")
    public void setGENERICRESOURCEAPIModelVersion(String gENERICRESOURCEAPIModelVersion) {
        this.gENERICRESOURCEAPIModelVersion = gENERICRESOURCEAPIModelVersion;
    }

    @JsonProperty("GENERIC-RESOURCE-API:model-name")
    public String getGENERICRESOURCEAPIModelName() {
        return gENERICRESOURCEAPIModelName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:model-name")
    public void setGENERICRESOURCEAPIModelName(String gENERICRESOURCEAPIModelName) {
        this.gENERICRESOURCEAPIModelName = gENERICRESOURCEAPIModelName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPIModelCustomizationUuid", gENERICRESOURCEAPIModelCustomizationUuid).append("gENERICRESOURCEAPIModelInvariantUuid", gENERICRESOURCEAPIModelInvariantUuid).append("gENERICRESOURCEAPIModelUuid", gENERICRESOURCEAPIModelUuid).append("gENERICRESOURCEAPIModelVersion", gENERICRESOURCEAPIModelVersion).append("gENERICRESOURCEAPIModelName", gENERICRESOURCEAPIModelName).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPIModelUuid).append(gENERICRESOURCEAPIModelInvariantUuid).append(gENERICRESOURCEAPIModelVersion).append(gENERICRESOURCEAPIModelName).append(gENERICRESOURCEAPIModelCustomizationUuid).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPIOnapModelInformation) == false) {
            return false;
        }
        GENERICRESOURCEAPIOnapModelInformation rhs = ((GENERICRESOURCEAPIOnapModelInformation) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPIModelUuid, rhs.gENERICRESOURCEAPIModelUuid).append(gENERICRESOURCEAPIModelInvariantUuid, rhs.gENERICRESOURCEAPIModelInvariantUuid).append(gENERICRESOURCEAPIModelVersion, rhs.gENERICRESOURCEAPIModelVersion).append(gENERICRESOURCEAPIModelName, rhs.gENERICRESOURCEAPIModelName).append(gENERICRESOURCEAPIModelCustomizationUuid, rhs.gENERICRESOURCEAPIModelCustomizationUuid).isEquals();
    }

}
