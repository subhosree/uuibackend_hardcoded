/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:name",
        "GENERIC-RESOURCE-API:value"
})
public class GENERICRESOURCEAPIParam {

    @JsonProperty("GENERIC-RESOURCE-API:name")
    private String gENERICRESOURCEAPIName;
    @JsonProperty("GENERIC-RESOURCE-API:value")
    private String gENERICRESOURCEAPIValue;

    @JsonProperty("GENERIC-RESOURCE-API:name")
    public String getGENERICRESOURCEAPIName() {
        return gENERICRESOURCEAPIName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:name")
    public void setGENERICRESOURCEAPIName(String gENERICRESOURCEAPIName) {
        this.gENERICRESOURCEAPIName = gENERICRESOURCEAPIName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:value")
    public String getGENERICRESOURCEAPIValue() {
        return gENERICRESOURCEAPIValue;
    }

    @JsonProperty("GENERIC-RESOURCE-API:value")
    public void setGENERICRESOURCEAPIValue(String gENERICRESOURCEAPIValue) {
        this.gENERICRESOURCEAPIValue = gENERICRESOURCEAPIValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPIName", gENERICRESOURCEAPIName).append("gENERICRESOURCEAPIValue", gENERICRESOURCEAPIValue).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPIValue).append(gENERICRESOURCEAPIName).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPIParam) == false) {
            return false;
        }
        GENERICRESOURCEAPIParam rhs = ((GENERICRESOURCEAPIParam) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPIValue, rhs.gENERICRESOURCEAPIValue).append(gENERICRESOURCEAPIName, rhs.gENERICRESOURCEAPIName).isEquals();
    }

}