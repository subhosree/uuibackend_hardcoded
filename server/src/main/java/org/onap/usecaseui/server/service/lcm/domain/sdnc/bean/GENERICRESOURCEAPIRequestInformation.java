/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:request-id",
        "GENERIC-RESOURCE-API:request-action",
        "GENERIC-RESOURCE-API:source",
        "GENERIC-RESOURCE-API:notification-url",
        "GENERIC-RESOURCE-API:order-number",
        "GENERIC-RESOURCE-API:order-version"
})
public class GENERICRESOURCEAPIRequestInformation {

    @JsonProperty("GENERIC-RESOURCE-API:request-id")
    private String gENERICRESOURCEAPIRequestId;
    @JsonProperty("GENERIC-RESOURCE-API:request-action")
    private String gENERICRESOURCEAPIRequestAction;
    @JsonProperty("GENERIC-RESOURCE-API:source")
    private String gENERICRESOURCEAPISource;
    @JsonProperty("GENERIC-RESOURCE-API:notification-url")
    private String gENERICRESOURCEAPINotificationUrl;
    @JsonProperty("GENERIC-RESOURCE-API:order-number")
    private String gENERICRESOURCEAPIOrderNumber;
    @JsonProperty("GENERIC-RESOURCE-API:order-version")
    private String gENERICRESOURCEAPIOrderVersion;

    @JsonProperty("GENERIC-RESOURCE-API:request-id")
    public String getGENERICRESOURCEAPIRequestId() {
        return gENERICRESOURCEAPIRequestId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:request-id")
    public void setGENERICRESOURCEAPIRequestId(String gENERICRESOURCEAPIRequestId) {
        this.gENERICRESOURCEAPIRequestId = gENERICRESOURCEAPIRequestId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:request-action")
    public String getGENERICRESOURCEAPIRequestAction() {
        return gENERICRESOURCEAPIRequestAction;
    }

    @JsonProperty("GENERIC-RESOURCE-API:request-action")
    public void setGENERICRESOURCEAPIRequestAction(String gENERICRESOURCEAPIRequestAction) {
        this.gENERICRESOURCEAPIRequestAction = gENERICRESOURCEAPIRequestAction;
    }

    @JsonProperty("GENERIC-RESOURCE-API:source")
    public String getGENERICRESOURCEAPISource() {
        return gENERICRESOURCEAPISource;
    }

    @JsonProperty("GENERIC-RESOURCE-API:source")
    public void setGENERICRESOURCEAPISource(String gENERICRESOURCEAPISource) {
        this.gENERICRESOURCEAPISource = gENERICRESOURCEAPISource;
    }

    @JsonProperty("GENERIC-RESOURCE-API:notification-url")
    public String getGENERICRESOURCEAPINotificationUrl() {
        return gENERICRESOURCEAPINotificationUrl;
    }

    @JsonProperty("GENERIC-RESOURCE-API:notification-url")
    public void setGENERICRESOURCEAPINotificationUrl(String gENERICRESOURCEAPINotificationUrl) {
        this.gENERICRESOURCEAPINotificationUrl = gENERICRESOURCEAPINotificationUrl;
    }

    @JsonProperty("GENERIC-RESOURCE-API:order-number")
    public String getGENERICRESOURCEAPIOrderNumber() {
        return gENERICRESOURCEAPIOrderNumber;
    }

    @JsonProperty("GENERIC-RESOURCE-API:order-number")
    public void setGENERICRESOURCEAPIOrderNumber(String gENERICRESOURCEAPIOrderNumber) {
        this.gENERICRESOURCEAPIOrderNumber = gENERICRESOURCEAPIOrderNumber;
    }

    @JsonProperty("GENERIC-RESOURCE-API:order-version")
    public String getGENERICRESOURCEAPIOrderVersion() {
        return gENERICRESOURCEAPIOrderVersion;
    }

    @JsonProperty("GENERIC-RESOURCE-API:order-version")
    public void setGENERICRESOURCEAPIOrderVersion(String gENERICRESOURCEAPIOrderVersion) {
        this.gENERICRESOURCEAPIOrderVersion = gENERICRESOURCEAPIOrderVersion;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPIRequestId", gENERICRESOURCEAPIRequestId).append("gENERICRESOURCEAPIRequestAction", gENERICRESOURCEAPIRequestAction).append("gENERICRESOURCEAPISource", gENERICRESOURCEAPISource).append("gENERICRESOURCEAPINotificationUrl", gENERICRESOURCEAPINotificationUrl).append("gENERICRESOURCEAPIOrderNumber", gENERICRESOURCEAPIOrderNumber).append("gENERICRESOURCEAPIOrderVersion", gENERICRESOURCEAPIOrderVersion).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPIRequestAction).append(gENERICRESOURCEAPIOrderVersion).append(gENERICRESOURCEAPIOrderNumber).append(gENERICRESOURCEAPISource).append(gENERICRESOURCEAPINotificationUrl).append(gENERICRESOURCEAPIRequestId).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPIRequestInformation) == false) {
            return false;
        }
        GENERICRESOURCEAPIRequestInformation rhs = ((GENERICRESOURCEAPIRequestInformation) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPIRequestAction, rhs.gENERICRESOURCEAPIRequestAction).append(gENERICRESOURCEAPIOrderVersion, rhs.gENERICRESOURCEAPIOrderVersion).append(gENERICRESOURCEAPIOrderNumber, rhs.gENERICRESOURCEAPIOrderNumber).append(gENERICRESOURCEAPISource, rhs.gENERICRESOURCEAPISource).append(gENERICRESOURCEAPINotificationUrl, rhs.gENERICRESOURCEAPINotificationUrl).append(gENERICRESOURCEAPIRequestId, rhs.gENERICRESOURCEAPIRequestId).isEquals();
    }

}
