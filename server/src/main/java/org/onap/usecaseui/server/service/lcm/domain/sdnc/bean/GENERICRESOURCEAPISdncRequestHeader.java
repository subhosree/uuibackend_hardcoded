/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:svc-request-id",
        "GENERIC-RESOURCE-API:svc-action",
        "GENERIC-RESOURCE-API:svc-notification-url"
})
public class GENERICRESOURCEAPISdncRequestHeader {

    @JsonProperty("GENERIC-RESOURCE-API:svc-request-id")
    private String gENERICRESOURCEAPISvcRequestId;
    @JsonProperty("GENERIC-RESOURCE-API:svc-action")
    private String gENERICRESOURCEAPISvcAction;
    @JsonProperty("GENERIC-RESOURCE-API:svc-notification-url")
    private String gENERICRESOURCEAPISvcNotificationUrl;

    @JsonProperty("GENERIC-RESOURCE-API:svc-request-id")
    public String getGENERICRESOURCEAPISvcRequestId() {
        return gENERICRESOURCEAPISvcRequestId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:svc-request-id")
    public void setGENERICRESOURCEAPISvcRequestId(String gENERICRESOURCEAPISvcRequestId) {
        this.gENERICRESOURCEAPISvcRequestId = gENERICRESOURCEAPISvcRequestId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:svc-action")
    public String getGENERICRESOURCEAPISvcAction() {
        return gENERICRESOURCEAPISvcAction;
    }

    @JsonProperty("GENERIC-RESOURCE-API:svc-action")
    public void setGENERICRESOURCEAPISvcAction(String gENERICRESOURCEAPISvcAction) {
        this.gENERICRESOURCEAPISvcAction = gENERICRESOURCEAPISvcAction;
    }

    @JsonProperty("GENERIC-RESOURCE-API:svc-notification-url")
    public String getGENERICRESOURCEAPISvcNotificationUrl() {
        return gENERICRESOURCEAPISvcNotificationUrl;
    }

    @JsonProperty("GENERIC-RESOURCE-API:svc-notification-url")
    public void setGENERICRESOURCEAPISvcNotificationUrl(String gENERICRESOURCEAPISvcNotificationUrl) {
        this.gENERICRESOURCEAPISvcNotificationUrl = gENERICRESOURCEAPISvcNotificationUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPISvcRequestId", gENERICRESOURCEAPISvcRequestId).append("gENERICRESOURCEAPISvcAction", gENERICRESOURCEAPISvcAction).append("gENERICRESOURCEAPISvcNotificationUrl", gENERICRESOURCEAPISvcNotificationUrl).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPISvcRequestId).append(gENERICRESOURCEAPISvcNotificationUrl).append(gENERICRESOURCEAPISvcAction).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPISdncRequestHeader) == false) {
            return false;
        }
        GENERICRESOURCEAPISdncRequestHeader rhs = ((GENERICRESOURCEAPISdncRequestHeader) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPISvcRequestId, rhs.gENERICRESOURCEAPISvcRequestId).append(gENERICRESOURCEAPISvcNotificationUrl, rhs.gENERICRESOURCEAPISvcNotificationUrl).append(gENERICRESOURCEAPISvcAction, rhs.gENERICRESOURCEAPISvcAction).isEquals();
    }

}