/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:service-id",
        "GENERIC-RESOURCE-API:subscription-service-type",
        "GENERIC-RESOURCE-API:service-instance-id",
        "GENERIC-RESOURCE-API:global-customer-id",
        "GENERIC-RESOURCE-API:subscriber-name",
        "GENERIC-RESOURCE-API:onap-model-information"
})
public class GENERICRESOURCEAPIServiceInformation {

    @JsonProperty("GENERIC-RESOURCE-API:service-id")
    private String gENERICRESOURCEAPIServiceId;
    @JsonProperty("GENERIC-RESOURCE-API:subscription-service-type")
    private String gENERICRESOURCEAPISubscriptionServiceType;
    @JsonProperty("GENERIC-RESOURCE-API:service-instance-id")
    private String gENERICRESOURCEAPIServiceInstanceId;
    @JsonProperty("GENERIC-RESOURCE-API:global-customer-id")
    private String gENERICRESOURCEAPIGlobalCustomerId;
    @JsonProperty("GENERIC-RESOURCE-API:subscriber-name")
    private String gENERICRESOURCEAPISubscriberName;
    @JsonProperty("GENERIC-RESOURCE-API:onap-model-information")
    private GENERICRESOURCEAPIOnapModelInformation gENERICRESOURCEAPIOnapModelInformation;

    @JsonProperty("GENERIC-RESOURCE-API:service-id")
    public String getGENERICRESOURCEAPIServiceId() {
        return gENERICRESOURCEAPIServiceId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:service-id")
    public void setGENERICRESOURCEAPIServiceId(String gENERICRESOURCEAPIServiceId) {
        this.gENERICRESOURCEAPIServiceId = gENERICRESOURCEAPIServiceId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subscription-service-type")
    public String getGENERICRESOURCEAPISubscriptionServiceType() {
        return gENERICRESOURCEAPISubscriptionServiceType;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subscription-service-type")
    public void setGENERICRESOURCEAPISubscriptionServiceType(String gENERICRESOURCEAPISubscriptionServiceType) {
        this.gENERICRESOURCEAPISubscriptionServiceType = gENERICRESOURCEAPISubscriptionServiceType;
    }

    @JsonProperty("GENERIC-RESOURCE-API:service-instance-id")
    public String getGENERICRESOURCEAPIServiceInstanceId() {
        return gENERICRESOURCEAPIServiceInstanceId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:service-instance-id")
    public void setGENERICRESOURCEAPIServiceInstanceId(String gENERICRESOURCEAPIServiceInstanceId) {
        this.gENERICRESOURCEAPIServiceInstanceId = gENERICRESOURCEAPIServiceInstanceId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:global-customer-id")
    public String getGENERICRESOURCEAPIGlobalCustomerId() {
        return gENERICRESOURCEAPIGlobalCustomerId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:global-customer-id")
    public void setGENERICRESOURCEAPIGlobalCustomerId(String gENERICRESOURCEAPIGlobalCustomerId) {
        this.gENERICRESOURCEAPIGlobalCustomerId = gENERICRESOURCEAPIGlobalCustomerId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subscriber-name")
    public String getGENERICRESOURCEAPISubscriberName() {
        return gENERICRESOURCEAPISubscriberName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subscriber-name")
    public void setGENERICRESOURCEAPISubscriberName(String gENERICRESOURCEAPISubscriberName) {
        this.gENERICRESOURCEAPISubscriberName = gENERICRESOURCEAPISubscriberName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:onap-model-information")
    public GENERICRESOURCEAPIOnapModelInformation getGENERICRESOURCEAPIOnapModelInformation() {
        return gENERICRESOURCEAPIOnapModelInformation;
    }

    @JsonProperty("GENERIC-RESOURCE-API:onap-model-information")
    public void setGENERICRESOURCEAPIOnapModelInformation(GENERICRESOURCEAPIOnapModelInformation gENERICRESOURCEAPIOnapModelInformation) {
        this.gENERICRESOURCEAPIOnapModelInformation = gENERICRESOURCEAPIOnapModelInformation;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPIServiceId", gENERICRESOURCEAPIServiceId).append("gENERICRESOURCEAPISubscriptionServiceType", gENERICRESOURCEAPISubscriptionServiceType).append("gENERICRESOURCEAPIServiceInstanceId", gENERICRESOURCEAPIServiceInstanceId).append("gENERICRESOURCEAPIGlobalCustomerId", gENERICRESOURCEAPIGlobalCustomerId).append("gENERICRESOURCEAPISubscriberName", gENERICRESOURCEAPISubscriberName).append("gENERICRESOURCEAPIOnapModelInformation", gENERICRESOURCEAPIOnapModelInformation).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPIServiceId).append(gENERICRESOURCEAPIOnapModelInformation).append(gENERICRESOURCEAPIServiceInstanceId).append(gENERICRESOURCEAPISubscriberName).append(gENERICRESOURCEAPIGlobalCustomerId).append(gENERICRESOURCEAPISubscriptionServiceType).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPIServiceInformation) == false) {
            return false;
        }
        GENERICRESOURCEAPIServiceInformation rhs = ((GENERICRESOURCEAPIServiceInformation) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPIServiceId, rhs.gENERICRESOURCEAPIServiceId).append(gENERICRESOURCEAPIOnapModelInformation, rhs.gENERICRESOURCEAPIOnapModelInformation).append(gENERICRESOURCEAPIServiceInstanceId, rhs.gENERICRESOURCEAPIServiceInstanceId).append(gENERICRESOURCEAPISubscriberName, rhs.gENERICRESOURCEAPISubscriberName).append(gENERICRESOURCEAPIGlobalCustomerId, rhs.gENERICRESOURCEAPIGlobalCustomerId).append(gENERICRESOURCEAPISubscriptionServiceType, rhs.gENERICRESOURCEAPISubscriptionServiceType).isEquals();
    }

}