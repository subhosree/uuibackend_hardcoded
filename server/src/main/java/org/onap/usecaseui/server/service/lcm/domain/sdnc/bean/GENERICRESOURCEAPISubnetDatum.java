/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:ip-version",
        "GENERIC-RESOURCE-API:sdnc-subnet-id",
        "GENERIC-RESOURCE-API:subnet-id",
        "GENERIC-RESOURCE-API:subnet-role",
        "GENERIC-RESOURCE-API:subnet-name",
        "GENERIC-RESOURCE-API:dhcp-enabled",
        "GENERIC-RESOURCE-API:network-start-address",
        "GENERIC-RESOURCE-API:gateway-address",
        "GENERIC-RESOURCE-API:cidr-mask"
})
public class GENERICRESOURCEAPISubnetDatum {

    @JsonProperty("GENERIC-RESOURCE-API:ip-version")
    private String gENERICRESOURCEAPIIpVersion;
    @JsonProperty("GENERIC-RESOURCE-API:sdnc-subnet-id")
    private String gENERICRESOURCEAPISdncSubnetId;
    @JsonProperty("GENERIC-RESOURCE-API:subnet-id")
    private String gENERICRESOURCEAPISubnetId;
    @JsonProperty("GENERIC-RESOURCE-API:subnet-role")
    private String gENERICRESOURCEAPISubnetRole;
    @JsonProperty("GENERIC-RESOURCE-API:subnet-name")
    private String gENERICRESOURCEAPISubnetName;
    @JsonProperty("GENERIC-RESOURCE-API:dhcp-enabled")
    private String gENERICRESOURCEAPIDhcpEnabled;
    @JsonProperty("GENERIC-RESOURCE-API:network-start-address")
    private String gENERICRESOURCEAPINetworkStartAddress;
    @JsonProperty("GENERIC-RESOURCE-API:gateway-address")
    private String gENERICRESOURCEAPIGatewayAddress;
    @JsonProperty("GENERIC-RESOURCE-API:cidr-mask")
    private String gENERICRESOURCEAPICidrMask;

    @JsonProperty("GENERIC-RESOURCE-API:ip-version")
    public String getGENERICRESOURCEAPIIpVersion() {
        return gENERICRESOURCEAPIIpVersion;
    }

    @JsonProperty("GENERIC-RESOURCE-API:ip-version")
    public void setGENERICRESOURCEAPIIpVersion(String gENERICRESOURCEAPIIpVersion) {
        this.gENERICRESOURCEAPIIpVersion = gENERICRESOURCEAPIIpVersion;
    }

    @JsonProperty("GENERIC-RESOURCE-API:sdnc-subnet-id")
    public String getGENERICRESOURCEAPISdncSubnetId() {
        return gENERICRESOURCEAPISdncSubnetId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:sdnc-subnet-id")
    public void setGENERICRESOURCEAPISdncSubnetId(String gENERICRESOURCEAPISdncSubnetId) {
        this.gENERICRESOURCEAPISdncSubnetId = gENERICRESOURCEAPISdncSubnetId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subnet-id")
    public String getGENERICRESOURCEAPISubnetId() {
        return gENERICRESOURCEAPISubnetId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subnet-id")
    public void setGENERICRESOURCEAPISubnetId(String gENERICRESOURCEAPISubnetId) {
        this.gENERICRESOURCEAPISubnetId = gENERICRESOURCEAPISubnetId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subnet-role")
    public String getGENERICRESOURCEAPISubnetRole() {
        return gENERICRESOURCEAPISubnetRole;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subnet-role")
    public void setGENERICRESOURCEAPISubnetRole(String gENERICRESOURCEAPISubnetRole) {
        this.gENERICRESOURCEAPISubnetRole = gENERICRESOURCEAPISubnetRole;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subnet-name")
    public String getGENERICRESOURCEAPISubnetName() {
        return gENERICRESOURCEAPISubnetName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subnet-name")
    public void setGENERICRESOURCEAPISubnetName(String gENERICRESOURCEAPISubnetName) {
        this.gENERICRESOURCEAPISubnetName = gENERICRESOURCEAPISubnetName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:dhcp-enabled")
    public String getGENERICRESOURCEAPIDhcpEnabled() {
        return gENERICRESOURCEAPIDhcpEnabled;
    }

    @JsonProperty("GENERIC-RESOURCE-API:dhcp-enabled")
    public void setGENERICRESOURCEAPIDhcpEnabled(String gENERICRESOURCEAPIDhcpEnabled) {
        this.gENERICRESOURCEAPIDhcpEnabled = gENERICRESOURCEAPIDhcpEnabled;
    }

    @JsonProperty("GENERIC-RESOURCE-API:network-start-address")
    public String getGENERICRESOURCEAPINetworkStartAddress() {
        return gENERICRESOURCEAPINetworkStartAddress;
    }

    @JsonProperty("GENERIC-RESOURCE-API:network-start-address")
    public void setGENERICRESOURCEAPINetworkStartAddress(String gENERICRESOURCEAPINetworkStartAddress) {
        this.gENERICRESOURCEAPINetworkStartAddress = gENERICRESOURCEAPINetworkStartAddress;
    }

    @JsonProperty("GENERIC-RESOURCE-API:gateway-address")
    public String getGENERICRESOURCEAPIGatewayAddress() {
        return gENERICRESOURCEAPIGatewayAddress;
    }

    @JsonProperty("GENERIC-RESOURCE-API:gateway-address")
    public void setGENERICRESOURCEAPIGatewayAddress(String gENERICRESOURCEAPIGatewayAddress) {
        this.gENERICRESOURCEAPIGatewayAddress = gENERICRESOURCEAPIGatewayAddress;
    }

    @JsonProperty("GENERIC-RESOURCE-API:cidr-mask")
    public String getGENERICRESOURCEAPICidrMask() {
        return gENERICRESOURCEAPICidrMask;
    }

    @JsonProperty("GENERIC-RESOURCE-API:cidr-mask")
    public void setGENERICRESOURCEAPICidrMask(String gENERICRESOURCEAPICidrMask) {
        this.gENERICRESOURCEAPICidrMask = gENERICRESOURCEAPICidrMask;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPIIpVersion", gENERICRESOURCEAPIIpVersion).append("gENERICRESOURCEAPISdncSubnetId", gENERICRESOURCEAPISdncSubnetId).append("gENERICRESOURCEAPISubnetId", gENERICRESOURCEAPISubnetId).append("gENERICRESOURCEAPISubnetRole", gENERICRESOURCEAPISubnetRole).append("gENERICRESOURCEAPISubnetName", gENERICRESOURCEAPISubnetName).append("gENERICRESOURCEAPIDhcpEnabled", gENERICRESOURCEAPIDhcpEnabled).append("gENERICRESOURCEAPINetworkStartAddress", gENERICRESOURCEAPINetworkStartAddress).append("gENERICRESOURCEAPIGatewayAddress", gENERICRESOURCEAPIGatewayAddress).append("gENERICRESOURCEAPICidrMask", gENERICRESOURCEAPICidrMask).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPIDhcpEnabled).append(gENERICRESOURCEAPINetworkStartAddress).append(gENERICRESOURCEAPISubnetName).append(gENERICRESOURCEAPISdncSubnetId).append(gENERICRESOURCEAPISubnetId).append(gENERICRESOURCEAPICidrMask).append(gENERICRESOURCEAPIGatewayAddress).append(gENERICRESOURCEAPISubnetRole).append(gENERICRESOURCEAPIIpVersion).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPISubnetDatum) == false) {
            return false;
        }
        GENERICRESOURCEAPISubnetDatum rhs = ((GENERICRESOURCEAPISubnetDatum) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPIDhcpEnabled, rhs.gENERICRESOURCEAPIDhcpEnabled).append(gENERICRESOURCEAPINetworkStartAddress, rhs.gENERICRESOURCEAPINetworkStartAddress).append(gENERICRESOURCEAPISubnetName, rhs.gENERICRESOURCEAPISubnetName).append(gENERICRESOURCEAPISdncSubnetId, rhs.gENERICRESOURCEAPISdncSubnetId).append(gENERICRESOURCEAPISubnetId, rhs.gENERICRESOURCEAPISubnetId).append(gENERICRESOURCEAPICidrMask, rhs.gENERICRESOURCEAPICidrMask).append(gENERICRESOURCEAPIGatewayAddress, rhs.gENERICRESOURCEAPIGatewayAddress).append(gENERICRESOURCEAPISubnetRole, rhs.gENERICRESOURCEAPISubnetRole).append(gENERICRESOURCEAPIIpVersion, rhs.gENERICRESOURCEAPIIpVersion).isEquals();
    }

}