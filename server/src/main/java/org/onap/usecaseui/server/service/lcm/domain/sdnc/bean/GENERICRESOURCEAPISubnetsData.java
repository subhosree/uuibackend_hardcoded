/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:subnet-data"
})
public class GENERICRESOURCEAPISubnetsData {

    @JsonProperty("GENERIC-RESOURCE-API:subnet-data")
    private List<GENERICRESOURCEAPISubnetDatum> gENERICRESOURCEAPISubnetData = null;

    @JsonProperty("GENERIC-RESOURCE-API:subnet-data")
    public List<GENERICRESOURCEAPISubnetDatum> getGENERICRESOURCEAPISubnetData() {
        return gENERICRESOURCEAPISubnetData;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subnet-data")
    public void setGENERICRESOURCEAPISubnetData(List<GENERICRESOURCEAPISubnetDatum> gENERICRESOURCEAPISubnetData) {
        this.gENERICRESOURCEAPISubnetData = gENERICRESOURCEAPISubnetData;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPISubnetData", gENERICRESOURCEAPISubnetData).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPISubnetData).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPISubnetsData) == false) {
            return false;
        }
        GENERICRESOURCEAPISubnetsData rhs = ((GENERICRESOURCEAPISubnetsData) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPISubnetData, rhs.gENERICRESOURCEAPISubnetData).isEquals();
    }

}
