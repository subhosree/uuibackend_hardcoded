/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:vnf-type",
        "GENERIC-RESOURCE-API:onap-model-information"
})
public class GENERICRESOURCEAPIVnfInformation {

    @JsonProperty("GENERIC-RESOURCE-API:vnf-type")
    private String gENERICRESOURCEAPIVnfType;
    @JsonProperty("GENERIC-RESOURCE-API:onap-model-information")
    private GENERICRESOURCEAPIOnapModelInformation_ gENERICRESOURCEAPIOnapModelInformation;

    @JsonProperty("GENERIC-RESOURCE-API:vnf-type")
    public String getGENERICRESOURCEAPIVnfType() {
        return gENERICRESOURCEAPIVnfType;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-type")
    public void setGENERICRESOURCEAPIVnfType(String gENERICRESOURCEAPIVnfType) {
        this.gENERICRESOURCEAPIVnfType = gENERICRESOURCEAPIVnfType;
    }

    @JsonProperty("GENERIC-RESOURCE-API:onap-model-information")
    public GENERICRESOURCEAPIOnapModelInformation_ getGENERICRESOURCEAPIOnapModelInformation() {
        return gENERICRESOURCEAPIOnapModelInformation;
    }

    @JsonProperty("GENERIC-RESOURCE-API:onap-model-information")
    public void setGENERICRESOURCEAPIOnapModelInformation(GENERICRESOURCEAPIOnapModelInformation_ gENERICRESOURCEAPIOnapModelInformation) {
        this.gENERICRESOURCEAPIOnapModelInformation = gENERICRESOURCEAPIOnapModelInformation;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPIVnfType", gENERICRESOURCEAPIVnfType).append("gENERICRESOURCEAPIOnapModelInformation", gENERICRESOURCEAPIOnapModelInformation).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPIOnapModelInformation).append(gENERICRESOURCEAPIVnfType).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPIVnfInformation) == false) {
            return false;
        }
        GENERICRESOURCEAPIVnfInformation rhs = ((GENERICRESOURCEAPIVnfInformation) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPIOnapModelInformation, rhs.gENERICRESOURCEAPIOnapModelInformation).append(gENERICRESOURCEAPIVnfType, rhs.gENERICRESOURCEAPIVnfType).isEquals();
    }

}
