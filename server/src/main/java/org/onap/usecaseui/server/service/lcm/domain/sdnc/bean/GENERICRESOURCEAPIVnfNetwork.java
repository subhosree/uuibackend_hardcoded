/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:network-role",
        "GENERIC-RESOURCE-API:network-name",
        "GENERIC-RESOURCE-API:neutron-id",
        "GENERIC-RESOURCE-API:network-id",
        "GENERIC-RESOURCE-API:contrail-network-fqdn",
        "GENERIC-RESOURCE-API:subnets-data"
})
public class GENERICRESOURCEAPIVnfNetwork {

    @JsonProperty("GENERIC-RESOURCE-API:network-role")
    private String gENERICRESOURCEAPINetworkRole;
    @JsonProperty("GENERIC-RESOURCE-API:network-name")
    private String gENERICRESOURCEAPINetworkName;
    @JsonProperty("GENERIC-RESOURCE-API:neutron-id")
    private String gENERICRESOURCEAPINeutronId;
    @JsonProperty("GENERIC-RESOURCE-API:network-id")
    private String gENERICRESOURCEAPINetworkId;
    @JsonProperty("GENERIC-RESOURCE-API:contrail-network-fqdn")
    private String gENERICRESOURCEAPIContrailNetworkFqdn;
    @JsonProperty("GENERIC-RESOURCE-API:subnets-data")
    private GENERICRESOURCEAPISubnetsData gENERICRESOURCEAPISubnetsData;

    @JsonProperty("GENERIC-RESOURCE-API:network-role")
    public String getGENERICRESOURCEAPINetworkRole() {
        return gENERICRESOURCEAPINetworkRole;
    }

    @JsonProperty("GENERIC-RESOURCE-API:network-role")
    public void setGENERICRESOURCEAPINetworkRole(String gENERICRESOURCEAPINetworkRole) {
        this.gENERICRESOURCEAPINetworkRole = gENERICRESOURCEAPINetworkRole;
    }

    @JsonProperty("GENERIC-RESOURCE-API:network-name")
    public String getGENERICRESOURCEAPINetworkName() {
        return gENERICRESOURCEAPINetworkName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:network-name")
    public void setGENERICRESOURCEAPINetworkName(String gENERICRESOURCEAPINetworkName) {
        this.gENERICRESOURCEAPINetworkName = gENERICRESOURCEAPINetworkName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:neutron-id")
    public String getGENERICRESOURCEAPINeutronId() {
        return gENERICRESOURCEAPINeutronId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:neutron-id")
    public void setGENERICRESOURCEAPINeutronId(String gENERICRESOURCEAPINeutronId) {
        this.gENERICRESOURCEAPINeutronId = gENERICRESOURCEAPINeutronId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:network-id")
    public String getGENERICRESOURCEAPINetworkId() {
        return gENERICRESOURCEAPINetworkId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:network-id")
    public void setGENERICRESOURCEAPINetworkId(String gENERICRESOURCEAPINetworkId) {
        this.gENERICRESOURCEAPINetworkId = gENERICRESOURCEAPINetworkId;
    }

    @JsonProperty("GENERIC-RESOURCE-API:contrail-network-fqdn")
    public String getGENERICRESOURCEAPIContrailNetworkFqdn() {
        return gENERICRESOURCEAPIContrailNetworkFqdn;
    }

    @JsonProperty("GENERIC-RESOURCE-API:contrail-network-fqdn")
    public void setGENERICRESOURCEAPIContrailNetworkFqdn(String gENERICRESOURCEAPIContrailNetworkFqdn) {
        this.gENERICRESOURCEAPIContrailNetworkFqdn = gENERICRESOURCEAPIContrailNetworkFqdn;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subnets-data")
    public GENERICRESOURCEAPISubnetsData getGENERICRESOURCEAPISubnetsData() {
        return gENERICRESOURCEAPISubnetsData;
    }

    @JsonProperty("GENERIC-RESOURCE-API:subnets-data")
    public void setGENERICRESOURCEAPISubnetsData(GENERICRESOURCEAPISubnetsData gENERICRESOURCEAPISubnetsData) {
        this.gENERICRESOURCEAPISubnetsData = gENERICRESOURCEAPISubnetsData;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPINetworkRole", gENERICRESOURCEAPINetworkRole).append("gENERICRESOURCEAPINetworkName", gENERICRESOURCEAPINetworkName).append("gENERICRESOURCEAPINeutronId", gENERICRESOURCEAPINeutronId).append("gENERICRESOURCEAPINetworkId", gENERICRESOURCEAPINetworkId).append("gENERICRESOURCEAPIContrailNetworkFqdn", gENERICRESOURCEAPIContrailNetworkFqdn).append("gENERICRESOURCEAPISubnetsData", gENERICRESOURCEAPISubnetsData).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPINetworkId).append(gENERICRESOURCEAPIContrailNetworkFqdn).append(gENERICRESOURCEAPISubnetsData).append(gENERICRESOURCEAPINetworkName).append(gENERICRESOURCEAPINeutronId).append(gENERICRESOURCEAPINetworkRole).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPIVnfNetwork) == false) {
            return false;
        }
        GENERICRESOURCEAPIVnfNetwork rhs = ((GENERICRESOURCEAPIVnfNetwork) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPINetworkId, rhs.gENERICRESOURCEAPINetworkId).append(gENERICRESOURCEAPIContrailNetworkFqdn, rhs.gENERICRESOURCEAPIContrailNetworkFqdn).append(gENERICRESOURCEAPISubnetsData, rhs.gENERICRESOURCEAPISubnetsData).append(gENERICRESOURCEAPINetworkName, rhs.gENERICRESOURCEAPINetworkName).append(gENERICRESOURCEAPINeutronId, rhs.gENERICRESOURCEAPINeutronId).append(gENERICRESOURCEAPINetworkRole, rhs.gENERICRESOURCEAPINetworkRole).isEquals();
    }

}