/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GENERIC-RESOURCE-API:request-version",
        "GENERIC-RESOURCE-API:vnf-name",
        "GENERIC-RESOURCE-API:vnf-networks",
        "GENERIC-RESOURCE-API:vnf-input-parameters"
})
public class GENERICRESOURCEAPIVnfRequestInput {

    @JsonProperty("GENERIC-RESOURCE-API:request-version")
    private String gENERICRESOURCEAPIRequestVersion;
    @JsonProperty("GENERIC-RESOURCE-API:vnf-name")
    private String gENERICRESOURCEAPIVnfName;
    @JsonProperty("GENERIC-RESOURCE-API:vnf-networks")
    private GENERICRESOURCEAPIVnfNetworks gENERICRESOURCEAPIVnfNetworks;
    @JsonProperty("GENERIC-RESOURCE-API:vnf-input-parameters")
    private GENERICRESOURCEAPIVnfInputParameters gENERICRESOURCEAPIVnfInputParameters;

    @JsonProperty("GENERIC-RESOURCE-API:request-version")
    public String getGENERICRESOURCEAPIRequestVersion() {
        return gENERICRESOURCEAPIRequestVersion;
    }

    @JsonProperty("GENERIC-RESOURCE-API:request-version")
    public void setGENERICRESOURCEAPIRequestVersion(String gENERICRESOURCEAPIRequestVersion) {
        this.gENERICRESOURCEAPIRequestVersion = gENERICRESOURCEAPIRequestVersion;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-name")
    public String getGENERICRESOURCEAPIVnfName() {
        return gENERICRESOURCEAPIVnfName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-name")
    public void setGENERICRESOURCEAPIVnfName(String gENERICRESOURCEAPIVnfName) {
        this.gENERICRESOURCEAPIVnfName = gENERICRESOURCEAPIVnfName;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-networks")
    public GENERICRESOURCEAPIVnfNetworks getGENERICRESOURCEAPIVnfNetworks() {
        return gENERICRESOURCEAPIVnfNetworks;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-networks")
    public void setGENERICRESOURCEAPIVnfNetworks(GENERICRESOURCEAPIVnfNetworks gENERICRESOURCEAPIVnfNetworks) {
        this.gENERICRESOURCEAPIVnfNetworks = gENERICRESOURCEAPIVnfNetworks;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-input-parameters")
    public GENERICRESOURCEAPIVnfInputParameters getGENERICRESOURCEAPIVnfInputParameters() {
        return gENERICRESOURCEAPIVnfInputParameters;
    }

    @JsonProperty("GENERIC-RESOURCE-API:vnf-input-parameters")
    public void setGENERICRESOURCEAPIVnfInputParameters(GENERICRESOURCEAPIVnfInputParameters gENERICRESOURCEAPIVnfInputParameters) {
        this.gENERICRESOURCEAPIVnfInputParameters = gENERICRESOURCEAPIVnfInputParameters;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPIRequestVersion", gENERICRESOURCEAPIRequestVersion).append("gENERICRESOURCEAPIVnfName", gENERICRESOURCEAPIVnfName).append("gENERICRESOURCEAPIVnfNetworks", gENERICRESOURCEAPIVnfNetworks).append("gENERICRESOURCEAPIVnfInputParameters", gENERICRESOURCEAPIVnfInputParameters).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPIRequestVersion).append(gENERICRESOURCEAPIVnfInputParameters).append(gENERICRESOURCEAPIVnfName).append(gENERICRESOURCEAPIVnfNetworks).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GENERICRESOURCEAPIVnfRequestInput) == false) {
            return false;
        }
        GENERICRESOURCEAPIVnfRequestInput rhs = ((GENERICRESOURCEAPIVnfRequestInput) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPIRequestVersion, rhs.gENERICRESOURCEAPIRequestVersion).append(gENERICRESOURCEAPIVnfInputParameters, rhs.gENERICRESOURCEAPIVnfInputParameters).append(gENERICRESOURCEAPIVnfName, rhs.gENERICRESOURCEAPIVnfName).append(gENERICRESOURCEAPIVnfNetworks, rhs.gENERICRESOURCEAPIVnfNetworks).isEquals();
    }

}
