/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "sdnc-request-header",
        "request-information",
        "service-information",
        "vnf-information",
        "vnf-request-input"
})
public class Input {

    @JsonProperty("sdnc-request-header")
    private SdncRequestHeader sdncRequestHeader;
    @JsonProperty("request-information")
    private RequestInformation requestInformation;
    @JsonProperty("service-information")
    private ServiceInformation serviceInformation;
    @JsonProperty("vnf-information")
    private VnfInformation vnfInformation;
    @JsonProperty("vnf-request-input")
    private VnfRequestInput vnfRequestInput;

    public Input(SdncRequestHeader sdncRequestHeader, RequestInformation requestInformation, ServiceInformation serviceInformation,
          VnfInformation vnfInformation, VnfRequestInput vnfRequestInput)
    {
        this.sdncRequestHeader=sdncRequestHeader;
        this.requestInformation=requestInformation;
        this.serviceInformation=serviceInformation;
        this.vnfInformation=vnfInformation;
        this.vnfRequestInput=vnfRequestInput;
    }

    @JsonProperty("sdnc-request-header")
    public SdncRequestHeader getSdncRequestHeader() {
        return sdncRequestHeader;
    }

    @JsonProperty("sdnc-request-header")
    public void setSdncRequestHeader(SdncRequestHeader sdncRequestHeader) {
        this.sdncRequestHeader = sdncRequestHeader;
    }

    @JsonProperty("request-information")
    public RequestInformation getRequestInformation() {
        return requestInformation;
    }

    @JsonProperty("request-information")
    public void setRequestInformation(RequestInformation requestInformation) {
        this.requestInformation = requestInformation;
    }

    @JsonProperty("service-information")
    public ServiceInformation getServiceInformation() {
        return serviceInformation;
    }

    @JsonProperty("service-information")
    public void setServiceInformation(ServiceInformation serviceInformation) {
        this.serviceInformation = serviceInformation;
    }

    @JsonProperty("vnf-information")
    public VnfInformation getVnfInformation() {
        return vnfInformation;
    }

    @JsonProperty("vnf-information")
    public void setVnfInformation(VnfInformation vnfInformation) {
        this.vnfInformation = vnfInformation;
    }

    @JsonProperty("vnf-request-input")
    public VnfRequestInput getVnfRequestInput() {
        return vnfRequestInput;
    }

    @JsonProperty("vnf-request-input")
    public void setVnfRequestInput(VnfRequestInput vnfRequestInput) {
        this.vnfRequestInput = vnfRequestInput;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("sdncRequestHeader", sdncRequestHeader).append("requestInformation", requestInformation).append("serviceInformation", serviceInformation).append("vnfInformation", vnfInformation).append("vnfRequestInput", vnfRequestInput).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(requestInformation).append(vnfInformation).append(vnfRequestInput).append(sdncRequestHeader).append(serviceInformation).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Input) == false) {
            return false;
        }
        Input rhs = ((Input) other);
        return new EqualsBuilder().append(requestInformation, rhs.requestInformation).append(vnfInformation, rhs.vnfInformation).append(vnfRequestInput, rhs.vnfRequestInput).append(sdncRequestHeader, rhs.sdncRequestHeader).append(serviceInformation, rhs.serviceInformation).isEquals();
    }

}
