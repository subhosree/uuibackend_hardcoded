/**
 * Copyright 2016-2017 ZTE Corporation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class SDNCBean {
    @JsonProperty("GENERIC-RESOURCE-API:input")
    private GENERICRESOURCEAPIInput gENERICRESOURCEAPIInput;

    @JsonProperty("GENERIC-RESOURCE-API:input")
    public GENERICRESOURCEAPIInput getGENERICRESOURCEAPIInput() {
        return gENERICRESOURCEAPIInput;
    }

    @JsonProperty("GENERIC-RESOURCE-API:input")
    public void setGENERICRESOURCEAPIInput(GENERICRESOURCEAPIInput gENERICRESOURCEAPIInput) {
        this.gENERICRESOURCEAPIInput = gENERICRESOURCEAPIInput;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gENERICRESOURCEAPIInput", gENERICRESOURCEAPIInput).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gENERICRESOURCEAPIInput).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SDNCBean) == false) {
            return false;
        }
        SDNCBean rhs = ((SDNCBean) other);
        return new EqualsBuilder().append(gENERICRESOURCEAPIInput, rhs.gENERICRESOURCEAPIInput).isEquals();
    }
}
