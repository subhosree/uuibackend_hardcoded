/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "svc-request-id",
        "svc-action"
})
public class SdncRequestHeader {

    @JsonProperty("svc-request-id")
    private String svcRequestId;
    @JsonProperty("svc-action")
    private String svcAction;

    public SdncRequestHeader(String svcRequestId, String svcAction) {
        this.svcRequestId = svcRequestId;
        this.svcAction = svcAction;
    }

    @JsonProperty("svc-request-id")
    public String getSvcRequestId() {
        return svcRequestId;
    }

    @JsonProperty("svc-request-id")
    public void setSvcRequestId(String svcRequestId) {
        this.svcRequestId = svcRequestId;
    }

    @JsonProperty("svc-action")
    public String getSvcAction() {
        return svcAction;
    }

    @JsonProperty("svc-action")
    public void setSvcAction(String svcAction) {
        this.svcAction = svcAction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("svcRequestId", svcRequestId).append("svcAction", svcAction).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(svcAction).append(svcRequestId).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SdncRequestHeader) == false) {
            return false;
        }
        SdncRequestHeader rhs = ((SdncRequestHeader) other);
        return new EqualsBuilder().append(svcAction, rhs.svcAction).append(svcRequestId, rhs.svcRequestId).isEquals();
    }

}
