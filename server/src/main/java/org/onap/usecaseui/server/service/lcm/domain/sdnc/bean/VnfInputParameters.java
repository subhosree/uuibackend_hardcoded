/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "param"
})
public class VnfInputParameters {

    @JsonProperty("param")
    private List<Param> param = null;

    public VnfInputParameters(List<Param> param) {
        this.param = param;
    }

    @JsonProperty("param")
    public List<Param> getParam() {
        return param;
    }

    @JsonProperty("param")
    public void setParam(List<Param> param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("param", param).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(param).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof VnfInputParameters) == false) {
            return false;
        }
        VnfInputParameters rhs = ((VnfInputParameters) other);
        return new EqualsBuilder().append(param, rhs.param).isEquals();
    }

}
