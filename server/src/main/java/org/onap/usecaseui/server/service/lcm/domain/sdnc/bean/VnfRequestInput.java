/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.lcm.domain.sdnc.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "vnf-input-parameters"
})
public class VnfRequestInput {

    @JsonProperty("vnf-input-parameters")
    private VnfInputParameters vnfInputParameters;

    public VnfRequestInput(VnfInputParameters vnfInputParameters) {
        this.vnfInputParameters = vnfInputParameters;
    }

    @JsonProperty("vnf-input-parameters")
    public VnfInputParameters getVnfInputParameters() {
        return vnfInputParameters;
    }

    @JsonProperty("vnf-input-parameters")
    public void setVnfInputParameters(VnfInputParameters vnfInputParameters) {
        this.vnfInputParameters = vnfInputParameters;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("vnfInputParameters", vnfInputParameters).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(vnfInputParameters).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof VnfRequestInput) == false) {
            return false;
        }
        VnfRequestInput rhs = ((VnfRequestInput) other);
        return new EqualsBuilder().append(vnfInputParameters, rhs.vnfInputParameters).isEquals();
    }

}
