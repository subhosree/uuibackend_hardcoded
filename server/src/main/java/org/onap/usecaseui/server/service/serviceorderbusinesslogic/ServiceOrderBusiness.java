/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.onap.usecaseui.server.service.serviceorderbusinesslogic;

import org.onap.usecaseui.server.bean.orderservice.Site;
import org.onap.usecaseui.server.bean.orderservice.VpnInformation;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServiceOrderBusiness {

    public String calculateSiteCost(List<Site> sites)
    {
        int sitecount=sites.size();
        String sitecost="";
        if(sitecount == 0)
            return "0";
        else
        {
            sitecost = String.valueOf(sites.stream().filter(i -> Integer.parseInt(i.getPrice()) > 0).mapToInt(i -> Integer.parseInt(i.getPrice())).sum());

            return sitecost;
        }
    }

    public String calculateVpnCost(List<VpnInformation> vpnInformations)
    {
        return String.valueOf(vpnInformations.size()*getVpnCost());
    }

    public String calculateBandwidthCost(String bandWidth)
    {

        return String.valueOf(getBandWidthCost());
    }

    public String calculateCameraCost()
    {
        return "";
    }

    public int getVpnCost()
    {
        return 100;
    }

    public int getBandWidthCost()
    {
        return 100;
    }

    public int getSiteCost()
    {
        return 100;
    }
}
