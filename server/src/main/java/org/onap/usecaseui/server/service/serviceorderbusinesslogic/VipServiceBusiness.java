/*
 * Copyright (C) 2017 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.service.serviceorderbusinesslogic;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.onap.usecaseui.server.bean.*;
import org.onap.usecaseui.server.bean.vipregister.VipInfoBean;
import org.onap.usecaseui.server.bean.vipregister.VipInfoEntity;
import org.onap.usecaseui.server.constant.Constant;
import org.onap.usecaseui.server.controller.VipServiceController;
import org.onap.usecaseui.server.service.lcm.domain.aai.AAIService;
import org.onap.usecaseui.server.service.lcm.domain.sdnc.SDNCService;
import org.onap.usecaseui.server.util.RestfulServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.UrlResource;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.sql.rowset.serial.SerialBlob;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.net.MalformedURLException;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import retrofit2.Response;

@Component
@Transactional
@org.springframework.context.annotation.Configuration
@EnableAspectJAutoProxy
public class VipServiceBusiness {

    private static final Logger logger = LoggerFactory.getLogger(VipServiceBusiness.class);

    private AAIService aaiService;

    private SDNCService sdncService;

    public VipServiceBusiness() { this(RestfulServices.create(AAIService.class),RestfulServices.create("http://119.3.84.205:30202/restconf/operations/GENERIC-RESOURCE-API:vnf-topology-operation/",SDNCService.class)); }

    public VipServiceBusiness(AAIService aaiService, SDNCService sdncService) {
        this.aaiService = aaiService;
        this.sdncService = sdncService;}


   /* public VipServiceBusiness() {
        this(RestfulServices.create("http://119.3.84.205:30202/restconf/operations/GENERIC-RESOURCE-API:vnf-topology-operation/",SDNCService.class));
    }

   public VipServiceBusiness(SDNCService sdncService) {
        this.sdncService = sdncService;
    }*/


    VipBean2Sdnc vipBean2Sdnc = new VipBean2Sdnc();

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.openSession();
    }

    private final String ext = ".png";
    private final Path rootLocation = Paths.get("/home/root1/uploadfile/");



    public void saveVipToDB(MultipartFile file, String vipName, String vipRole, String vipModel) {
        VipInfoEntity vipInfoEntity= null;


        try {
            vipInfoEntity = new VipInfoEntity(String.valueOf(Math.random() * 49 + 1), vipName, vipRole, vipModel, new SerialBlob(file.getBytes()));

        }catch (IOException| SQLException e)
        {
            logger.error("exception occurred while image to Blob. Details:" + e.getMessage());
            System.out.println("Exception Occured : "+e.getMessage());
        }

        try(Session session = getSession()){
            if (null == vipInfoEntity) {
                logger.error("Vip Information is null!");
            }
            Transaction tx = session.beginTransaction();
            session.save(vipInfoEntity);
            tx.commit();
            session.flush();

            vipBean2Sdnc.setVipName(vipName);
            vipBean2Sdnc.setCompany(vipRole);
            vipBean2Sdnc.setUrl(vipModel);
            //added by Sindhuri
            queryAai();
           // pushToSdnc();

        } catch (Exception e) {
            logger.error("exception occurred while saving VIP information to DB. Details:" + e.getMessage());
            System.out.println("Exception Occured : "+e.getMessage());
        }
    }

    private void queryAai() {
        String result="";
        String customerId = "";
        ObjectMapper mapper = new ObjectMapper();

        try {
            logger.info("aai getCustomers is starting!");

            Response<ResponseBody> response = this.aaiService.getCustomers().execute();
            logger.info("aai getCustomers has finished!");
            if (response.isSuccessful()) {
                result=new String(response.body().bytes());

                Customers customers = mapper.readValue(result, new TypeReference<Customers>() {});
                customerId = customers.getCustomers().get(0).getCustomerId();



              //  customerId = "customerId";
                vipBean2Sdnc.setCustomerId(customerId);

                queryServiceSubscription(customerId);

                getSiteResources();

            }
          /*  if(true) {
                customerId = "customerId";
                vipBean2Sdnc.setCustomerId(customerId);

                queryServiceSubscription(customerId);

                getSiteResources();
            }*/
            else {
                logger.info(String.format("Failed to get data from AAI[code=%s, message=%s]", response.code(), response.message()));
                result=Constant.CONSTANT_FAILED;
                throw new NullPointerException("Failed to get data from AAI");
            }

        } catch (Exception ex) {

            logger.error("getCustomers exception occured:"+ex);
            result=Constant.CONSTANT_FAILED;
        }

    }

    private void queryServiceSubscription(String customerId) {
        String result="";
        String serviceType = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            logger.info("aai getServiceSubscription is starting!");

           Response<ResponseBody> response = this.aaiService.getServiceSubscription(customerId).execute();
            logger.info("aai getServiceSubscription has finished!");
           if (response.isSuccessful()) {
                result=new String(response.body().bytes());
                ServiceSubscriptions subscriptions = mapper.readValue(result, new TypeReference<ServiceSubscriptions>() {});
                serviceType = subscriptions.getServiceSubscriptions().get(0).getServiceType();
                vipBean2Sdnc.setServiceType(serviceType);

            }
            /*if(true)
            {
                serviceType = "ccvpn";
                vipBean2Sdnc.setServiceType(serviceType);
            }*/
            else {
                logger.info(String.format("Failed to get data from AAI[code=%s, message=%s]", response.code(), response.message()));
                result=Constant.CONSTANT_FAILED;
                throw new NullPointerException("Failed to get data from AAI");
            }

        } catch (Exception ex) {

            logger.error("getServiceSubscription exception occured:"+ex);
            result=Constant.CONSTANT_FAILED;
        }
    }

    private void getSiteResources() {
        String result="";
        String siteResourceId = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            logger.info("aai getSiteResources is starting!");

            Response<ResponseBody> response = this.aaiService.getSiteResources().execute();
            logger.info("aai getSiteResources has finished!");
            if (response.isSuccessful()) {
                result=new String(response.body().bytes());
                SiteResources siteResources = mapper.readValue(result, new TypeReference<SiteResources>() {});
                siteResourceId = siteResources.getSiteResources().get(0).getSiteResourceId();

                getService(siteResourceId);

            }
            /*if(true)
            {
                siteResourceId = "siteResourceId";

                getService(siteResourceId);

            }*/
            else {
                logger.info(String.format("Failed to get data from AAI[code=%s, message=%s]", response.code(), response.message()));
                result=Constant.CONSTANT_FAILED;
                throw new NullPointerException("Failed to get data from AAI");
            }

        } catch (Exception ex) {

            logger.error("getSiteResources exception occured:"+ex);
            result=Constant.CONSTANT_FAILED;
        }
    }

    private void getService(String siteResourceId) {
        String result="";
        String serviceInstanceId = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            logger.info("aai getService is starting!");

            Response<ResponseBody> response = this.aaiService.getServiceSubscription(siteResourceId).execute();
            logger.info("aai getService has finished!");
           if (response.isSuccessful()) {
                result=new String(response.body().bytes());
                ServiceInfo serviceInfo = mapper.readValue(result, new TypeReference<ServiceInfo>() {});

               // serviceInstanceId = serviceInfo.getRelationshipList().getRelationship().get(0).getRelationshipData().get(2).getRelationshipValue();
                List<RelationshipDatum> relationships = serviceInfo.getRelationshipList().getRelationship()
                        .stream()
                        .filter(relationship->relationship.getRelatedTo().equalsIgnoreCase("service-instance"))
                        .map(relationship->relationship.getRelationshipData()).collect(Collectors.toList()).get(0);
                      //  .stream().filter(relationshipData -> relationshipData.ge);

                serviceInstanceId = relationships.stream().filter(relationship->relationship.getRelationshipKey().equalsIgnoreCase("service-instance.service-instance-id"))
                            .map(relationship->relationship.getRelationshipValue()).toString();

                vipBean2Sdnc.setServiceInstanceId(serviceInstanceId);
                pushToSdnc(vipBean2Sdnc);

            }
      /* if(true)
       {
           serviceInstanceId ="098dad9941";
           vipBean2Sdnc.setServiceInstanceId(serviceInstanceId);
           pushToSdnc(vipBean2Sdnc);

       }*/
       else {
                logger.info(String.format("Failed to get data from AAI[code=%s, message=%s]", response.code(), response.message()));
                result=Constant.CONSTANT_FAILED;
                throw new NullPointerException("Failed to get data from AAI");
            }

        } catch (Exception ex) {

            logger.error("getService exception occured:"+ex);
            result=Constant.CONSTANT_FAILED;
        }
    }


    public void pushToSdnc(VipBean2Sdnc vipBean2Sdnc) throws IOException{
        String result=null;
        System.out.println("Received....");

        String customerId = vipBean2Sdnc.getCustomerId();
        String serviceType = vipBean2Sdnc.getServiceType();
        String serviceInstanceId = vipBean2Sdnc.getServiceInstanceId();
        String vipName = vipBean2Sdnc.getVipName();
        String company = vipBean2Sdnc.getCompany();
        String url = vipBean2Sdnc.getUrl();

        String str="{\n" +
                "   \"input\":{\n" +
                "      \"sdnc-request-header\":{\n" +
                "         \"svc-request-id\":\"ab9c110c-f1f1-43d4-a5aa-95e16abd10d6\",\n" +
                "         \"svc-action\":\"update\"\n" +
                "      },\n" +
                "      \"request-information\":{\n" +
                "         \"request-id\": \"ab9c110c-f1f1-43d4-a5aa-95e16abd10d6\",\n" +
                "         \"request-action\":\"SetScreenSourceCameraInstance\"\n" +
                "      },\n" +
                "      \"service-information\":{\n" +
                "         \"service-instance-id\":\""+serviceInstanceId+"\"\n" +
                "      },\n" +
                "      \"vnf-information\":{\n" +
                "\n" +
                "      },\n" +
                "      \"vnf-request-input\":{\n" +
                "         \"vnf-input-parameters\":{\n" +
                "            \"param\":[\n" +
                "               {\n" +
                "                  \"name\":\"VIP.name\",\n" +
                "                  \"value\":\""+vipName+"\"\n" +
                "               },\n" +
                "               {\n" +
                "                  \"name\":\"VIP.company\",\n" +
                "                  \"value\":\""+company+"\"\n" +
                "               },\n" +
                "               {\n" +
                "                  \"name\":\"VIP.model\",\n" +
                "                  \"value\":\""+url+"\"\n" +
                "               }\n" +
                "            ]\n" +
                "         }\n" +
                "      }\n" +
                "   }\n" +
                "}\n";
        RequestBody requestBody =  RequestBody.create(MediaType.parse("application/json"), str);
        System.out.println("Sending...........");
        try {
            Response<ResponseBody> response =sdncService.addVip(requestBody).execute();
            if (response.isSuccessful()) {
                result=new String(response.body().bytes());
            } else {
                logger.info(String.format("Add Model Information failed : [code=%s, message=%s]", response.code(), response.message()));
                result= Constant.CONSTANT_FAILED;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

       // return result;

    }


    public List<VipInfoBean> getAllVipInfoFromDb()
    {
        String sql="select vipInfoEntity from VipInfoEntity vipInfoEntity where 1=1";
        Query query= null;
        List<VipInfoEntity> vipList = new ArrayList<>();
        List<VipInfoBean> vipFinalList = new ArrayList<>();
        try(Session session = getSession()){
            query = session.createQuery(sql);
            vipList =  query.list();
            session.flush();
        }catch (Exception e) {
            logger.error("exception occurred. Details:" + e.getMessage());
        }


        for(VipInfoEntity vipInfoEntiry : vipList)
        {
            System.out.println("Data Extracted: "+vipInfoEntiry.toString());
            VipInfoBean vipInfoBean = new VipInfoBean();
            String url = vipInfoEntiry.getId().toString();
            vipInfoBean.setImgUrl(MvcUriComponentsBuilder
                    .fromMethodName(VipServiceController.class, "getVipImage", url).build().toString());
            vipInfoBean.setVipRole(vipInfoEntiry.getVipRole());
            vipInfoBean.setVipModel(vipInfoEntiry.getVipModel());
            vipInfoBean.setVipName(vipInfoEntiry.getVipName());
            vipFinalList.add(vipInfoBean);

        }
        return vipFinalList;
    }



    public byte[] getVipImageFromDb(String vipid)
    {
        String sql="select vipInfoEntity from VipInfoEntity vipInfoEntity where id='"+vipid+"'";
        Query query= null;
        byte[] image = null;


        try(Session session = getSession()){
            query = session.createQuery(sql);
            //query.setParameter("id",vipid);
            VipInfoEntity vipInfoEntity = (VipInfoEntity) query.uniqueResult();
            session.flush();
            Blob blob = vipInfoEntity.getVipImage();
            image=blob.getBytes(1,(int) blob.length());
            blob.free();
        }catch (Exception e) {
            logger.error("exception occurred while performing AlarmsHeaderServiceImpl queryAlarmsHeader. Details:" + e.getMessage());
            System.out.println("Exception Occured : "+e.getMessage());
        }
        return image;
    }

    /*

    public File[] getAllVipfiles() {


        File file = Paths.get("/home/root1/uploadfile").toFile();

        File[] filenames = file.listFiles((d, s) -> {
            return s.toLowerCase().endsWith(ext);
        });

        //Object[] filenames= Stream.of(file.list((pFile, pString) -> pString.endsWith(".png"))).toArray();

        System.out.println("All files : "+filenames.toString());

        return filenames;

    }





    public Resource loadFile(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }



        public void saveFile(MultipartFile file, String vipName, String vipRole) throws IOException{

            String filePath="/home/root1/uploadfile/"+vipName+ext;
            InputStream inputStream = file.getInputStream();
            FileOutputStream out = new FileOutputStream(new File(filePath));
            int read = 0;
            byte[] bytes = new byte[1024];
            out = new FileOutputStream(new File(filePath));
            while ((read = inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
    }


    public List<VipInfoBean> getAllVipfileList() {


        File file = Paths.get("/home/root1/uploadfile").toFile();


        List<String> filenames= Stream.of(file.list((pFile, pString) -> pString.endsWith(".png"))).collect(Collectors.toList());

        filenames = filenames
                .stream().map(fileName -> MvcUriComponentsBuilder
                        .fromMethodName(VipServiceController.class, "getVipImage", fileName).build().toString())
                .collect(Collectors.toList());

        List<VipInfoBean> vipinfoList =
                filenames.stream()
                        .map(changeList)
                        .collect(Collectors.toList());

        System.out.println("All files : " + filenames.toString());

        return vipinfoList;
    }

        Function<String, VipInfoBean> changeList
            = new Function<String, VipInfoBean>() {

        public VipInfoBean apply(String imgUrl) {
            VipInfoBean vipInfoBean = new VipInfoBean();
            vipInfoBean.setImgUrl(imgUrl);
            vipInfoBean.setVipName("vipname");
            vipInfoBean.setVipRole("viprole");
            return vipInfoBean;
        }
    };

    */


}
