/*
 * Copyright (C) 2018 CMCC, Inc. and others. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.onap.usecaseui.server.util;

import java.io.IOException;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JSONMapper {
    public HashMap<String, Object> JSONMapperCreator(HashMap<String, Object> supMap, HashMap<String, Object> subMap) {
        return hashMapper(supMap, subMap);
    }

    private HashMap<String, Object> hashMapper(HashMap<String, Object> supMap, HashMap<String, Object> subMap) {
        for (HashMap.Entry<String, Object> entry : supMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof String) {
                if (subMap.containsKey(key)) {
                    supMap.put(key, subMap.get(key));
                    break;
                }
            } else if (value instanceof HashMap) {
                HashMap<String, Object> genMap = (HashMap<String, Object>) value;
                hashMapper(genMap, subMap);
            } else if (value instanceof List) {
                if (subMap.containsKey(key)) {
                    supMap.put(key, subMap.get(key));
                    break;
                } else {
                    List<HashMap<String, Object>> rxData =
                            (List<HashMap<String, Object>>) (List<?>) value;
                    for (HashMap<String, Object> ls : rxData) {
                        hashMapper(ls, subMap);
                    }
                }
            } else {
                throw new IllegalArgumentException(String.valueOf(value));
            }
        }
        return supMap;
    }
}
