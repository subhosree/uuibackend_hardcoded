<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright 2016-2017 ZTE Corporation.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <groupId>org.onap.usecase-ui.server</groupId>
        <artifactId>usecase-ui-server-parent</artifactId>
        <version>1.3.0-SNAPSHOT</version>
    </parent>

    <modelVersion>4.0.0</modelVersion>
    <artifactId>standalone</artifactId>
    <version>1.3.0-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>usecase-ui-server-standalone</name>

    <properties>
        <packagename>usecase-ui-server</packagename>
        <linux64id>linux64</linux64id>
        <win64id>win64</win64id>
        <linux64outputdir>target/assembly/${linux64}/</linux64outputdir>
        <win64outputdir>target/assembly/${win64id}/</win64outputdir>
        <version.output>target/version/</version.output>
        <usecaseui.version>1.3.0</usecaseui.version>
        <maven.build.timestamp.format>yyyyMMdd'T'HHmmss'Z'</maven.build.timestamp.format>
    </properties>

    <build>
        <finalName>usecse-ui-server</finalName>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-resources-${linux64id}</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${basedir}/${linux64outputdir}</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>src/main/assembly/</directory>
                                    <filtering>false</filtering>
                                    <includes>
                                        <include>**/*</include>
                                    </includes>
                                    <excludes>
                                        <exclude>**/*.bat</exclude>
                                        <exclude>Dockerfile</exclude>
                                    </excludes>
                                </resource>
                            </resources>
                            <overwrite>true</overwrite>
                        </configuration>
                    </execution>
                    <execution>
                        <id>copy-resources-${win64id}</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${basedir}/${win64outputdir}</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>src/main/assembly/</directory>
                                    <filtering>false</filtering>
                                    <includes>
                                        <include>**/*</include>
                                    </includes>
                                    <excludes>
                                        <exclude>**/*.sh</exclude>
                                        <exclude>Dockerfile</exclude>
                                    </excludes>
                                </resource>
                            </resources>
                            <overwrite>true</overwrite>
                        </configuration>
                    </execution>
                    <execution>
                        <id>copy-dockerfile</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${version.output}</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>src/main/assembly/</directory>
                                    <filtering>false</filtering>
                                    <includes>
                                        <include>Dockerfile</include>
                                    </includes>
                                </resource>
                            </resources>
                            <overwrite>true</overwrite>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>2.8</version>
                <executions>
                    <execution>
                        <id>copy-jar-${linux64id}</id>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <phase>prepare-package</phase>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>org.onap.usecase-ui.server</groupId>
                                    <artifactId>usecase-ui-server</artifactId>
                                    <type>jar</type>
                                    <overWrite>true</overWrite>
                                    <outputDirectory>${linux64outputdir}</outputDirectory>
                                    <destFileName>usecase-ui-server.jar</destFileName>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                    <execution>
                        <id>copy-jar-${win64id}</id>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <phase>prepare-package</phase>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>org.onap.usecase-ui.server</groupId>
                                    <artifactId>usecase-ui-server</artifactId>
                                    <type>jar</type>
                                    <overWrite>true</overWrite>
                                    <outputDirectory>${win64outputdir}</outputDirectory>
                                    <destFileName>usecase-ui-server.jar</destFileName>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>1.8</version>
                <executions>
                    <execution>
                        <id>distribution</id>
                        <phase>package</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <tasks name="${project.artifactId}">
                                <tar destfile="${version.output}/${packagename}-${project.version}-linux64.tar.gz" longfile="posix" compression="gzip">
                                    <tarfileset dir="${linux64outputdir}" filemode="0644" dirmode="0755">
                                        <exclude name="**/*.sh"/>
                                    </tarfileset>
                                    <tarfileset dir="${linux64outputdir}" filemode="0755" dirmode="0755">
                                        <include name="**/*.sh"/>
                                    </tarfileset>
                                </tar>
                                <!--attachartifact file="${version.output}/${packagename}-${project.version}-linux64.tar.gz" classifier="linux64" type="tar.gz"/-->
                                <zip destfile="${version.output}/${packagename}-${project.version}-win64.zip" update="true">
                                    <zipfileset dir="${win64outputdir}" includes="**"/>
                                </zip>
                                <!--attachartifact file="${version.output}/${packagename}-${project.version}-win64.zip" classifier="win64" type="zip"/-->
                            </tasks>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-artifacts-linux64</id>
                        <phase>package</phase>
                        <goals>
                            <goal>attach-artifact</goal>
                        </goals>
                        <configuration>
                            <artifacts>
                                <artifact>
                                    <file>${version.output}/${packagename}-${project.version}-linux64.tar.gz</file>
                                    <type>tar.gz</type>
                                </artifact>
                            </artifacts>
                        </configuration>
                    </execution>
                    <execution>
                        <id>attach-artifacts-win64</id>
                        <phase>package</phase>
                        <goals>
                            <goal>attach-artifact</goal>
                        </goals>
                        <configuration>
                            <artifacts>
                                <artifact>
                                    <file>${version.output}/${packagename}-${project.version}-win64.zip</file>
                                    <type>zip</type>
                                </artifact>
                            </artifacts>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>docker</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>io.fabric8</groupId>
                        <artifactId>docker-maven-plugin</artifactId>
                        <version>0.16.5</version>
                        <inherited>false</inherited>
                        <configuration>
                            <images>
                                <image>
                                    <name>onap/usecase-ui-server</name>
                                    <build>
                                        <cleanup>try</cleanup>
                                        <dockerFileDir>${basedir}/${version.output}</dockerFileDir>
                                        <dockerFile>${basedir}/${version.output}/Dockerfile</dockerFile>
                                        <tags>
                                            <tag>${usecaseui.version}-SNAPSHOT-latest</tag>
                                            <tag>${usecaseui.version}-STAGING-latest</tag>
                                            <tag>${usecaseui.version}-STAGING-${maven.build.timestamp}</tag>
                                        </tags>
                                    </build>
                                </image>
                            </images>
                        </configuration>
                        <executions>
                            <execution>
                                <id>generate-images</id>
                                <phase>package</phase>
                                <goals>
                                    <goal>build</goal>
                                </goals>
                            </execution>

                            <execution>
                                <id>push-images</id>
                                <phase>deploy</phase>
                                <goals>
                                    <goal>build</goal>
                                    <goal>push</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <dependencyManagement>
        <dependencies>
            <!--dependency>
                <groupId>ant-contrib</groupId>
                <artifactId>ant-contrib</artifactId>
            </dependency-->
            <dependency>
                <groupId>org.onap.usecase-ui.server</groupId>
                <artifactId>usecase-ui-server</artifactId>
                <version>${project.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>
</project>